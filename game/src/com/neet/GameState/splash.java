package com.neet.GameState;

import com.neet.Main.GamePanel;

import java.awt.*;

public class splash extends GameState {

    private static Font font;
    private static int splashTiming = 0;
    private static int splashCount = 2;
    private static int splashCurrent = 1;
    private static int splashMax = GamePanel.GAME_HERTZ*2;

    private static String text = " ";


    public splash(GameStateManager gsm) {
        super(gsm);
        newSplash("unhidden", 1);
        font = new Font("Arial", Font.BOLD, 32);
    }

    public void init() {

    }

    public void update() {
        splashTiming++;
        if(splashTiming>splashMax){

            if(splashCurrent == 1)newSplash("Realm Fighters", 1);
            if(splashCurrent == 2)gsm.setState(GameStateManager.GAME);

            splashCurrent++;
        }
    }

    public void draw(Graphics2D g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);

        FontMetrics fmetric = g.getFontMetrics(font);

        g.setFont(font);

        g.setColor(Color.WHITE);
        g.drawString(text, GamePanel.WIDTH/2-fmetric.stringWidth(text)/2, GamePanel.HEIGHT/2+5);

    }

    public void handleInput() {

    }

    public void newSplash(String text, int sec) {
        splashTiming = 0;
        this.text = text;
        splashMax = GamePanel.GAME_HERTZ*sec;
    }
}
