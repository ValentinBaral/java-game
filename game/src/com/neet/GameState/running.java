package com.neet.GameState;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import com.neet.Audio.JukeBox;
import com.neet.Entity.*;
import com.neet.Handlers.Keys;
import com.neet.Main.GamePanel;
import com.neet.Main.config;
import com.neet.events.events;
import com.neet.events.utl;
import com.neet.world.map;

public class running extends GameState {
	
	public static Player player;
	private map m;
	
	
	protected BufferedImage image_other = utl.image_other;
	protected BufferedImage cutImage;
	protected int sprite_dimension = 60;
	
	public double cameraDelay = 1.1;
	private double camCountX = 1;
	private double camCountY = 1;
	private double oldX = 0;
	private double oldY = 0;

	private double cameraMod_X = 0;
	private double cameraMod_Y = 0;
	private double cameraMod_Max = 4;
	private double cameraMod_Step = 0.1;

	private boolean cameraMod_dX = true;
	private boolean cameraMod_dY = true;
	
	private int activeRoom = 0;
	
	private int pulseBG = 0;
	private int pulseDelay = 5;
	private boolean pulseBack = false;

	private static boolean showBreak = false;
	private static String breakText = "";

    public static int counter = 0;


	public running(GameStateManager gsm) {
		super(gsm);

		m = new map(125);

		//worldGenerator world = new worldGenerator();
		worldGenerator.create();

		player = new Player(0);
		player.setPosition(
				worldGenerator.getLevel(0).getX()+300,
				worldGenerator.getLevel(0).getY()+worldGenerator.getLevel(0).getHeight()-200-player.getheight()
		);

		/*for(int i = 0; i<worldGenerator.getRooms().size();i++){
			worldGenerator.getRoom(i).createEnemys();
		}*/

		

	}

	public void init() {
	}

	public void update() {

        counter++;

        //some Fix
        for(int i=0; i<worldGenerator.getActiveLevel().getList().size();i++){
            worldGenerator.getActiveLevel().getList().get(i).clearNext();
        }

        handleInput();

        Keys.update();

        //pool.update();

        handlePulseBG();
		
		if(gsm.currentState == gsm.GAME){

			background.update();

			// update events
			events.update();
			
			gfx.update();

			showNumbers.update();

			overlay.update();

            for(int i=0; i<worldGenerator.getActiveLevel().getList().size();i++){
                MapObject object = worldGenerator.getActiveLevel().getList().get(i);

                if (object.getActive() == true) {

                    object.update();

                }
            }

            // update map position
			updateMapPosition();
		
		}

	}
	
	public void setRoom(int newRoom) {activeRoom = newRoom;}
	public int getRoom() {return (int)activeRoom;}
	
	private void handlePulseBG(){
		
		if(pulseBack){
			if(pulseBG/pulseDelay<100){
				pulseBG = pulseBG+1;
			}else{
				pulseBack = false;
			}
		}
		if(!pulseBack){
			if(pulseBG/pulseDelay>0){
				pulseBG = pulseBG-1;
			}else{
				pulseBack = true;
			}
		}
	}
	
	public void updateMapPosition(){
		double x = GamePanel.WIDTH/2-player.getx()-player.getwidth()/2;
		double y = GamePanel.HEIGHT/2-player.gety()-player.getheight()/2;

		if(config.camera_Shake){
			if(player.getx() != player.getOldX()){
			//if(player.getMLeft()||player.getMRight()) {

				/*if (cameraMod_dX == true) {
					if (cameraMod_dY == true) {
						cameraMod_X = cameraMod_X + cameraMod_Step;
						if (cameraMod_X >= cameraMod_Max) {
							cameraMod_dX = false;
							cameraMod_dY = true;
						}
					}
				} else {
					if (cameraMod_dY == false) {
						cameraMod_X = cameraMod_X - cameraMod_Step;
						if (cameraMod_X <= 0) {
							cameraMod_dX = true;
							cameraMod_dY = false;
						}
					}
				}

				if (cameraMod_dY == true) {
					if (cameraMod_dX == false) {
						cameraMod_Y = cameraMod_Y + cameraMod_Step;
						if (cameraMod_Y >= cameraMod_Max) {
							cameraMod_dX = false;
							cameraMod_dY = false;
						}
					}
				} else {
					if (cameraMod_dX == true) {
						cameraMod_Y = cameraMod_Y - cameraMod_Step;
						if (cameraMod_Y <= 0) {
							cameraMod_dX = true;
							cameraMod_dY = true;
						}
					}
				}*/


				if (cameraMod_dY == true) {
					cameraMod_Y = cameraMod_Y + cameraMod_Step;
					if (cameraMod_Y >= cameraMod_Max) {
						cameraMod_dY = false;
					}
				} else {
					cameraMod_Y = cameraMod_Y - cameraMod_Step;
					if (cameraMod_Y <= 0) {
						cameraMod_dY = true;
					}
				}


			}else{
				if(cameraMod_Y < cameraMod_Max/2) {
					cameraMod_Y += cameraMod_Step;
					if (cameraMod_Y > cameraMod_Max / 2) {
						cameraMod_Y = cameraMod_Max / 2;
					}
				}
				if(cameraMod_Y > cameraMod_Max/2){
					cameraMod_Y -= cameraMod_Step;
					if(cameraMod_Y < cameraMod_Max/2){
						cameraMod_Y = cameraMod_Max/2;
					}
				}
			}

			x = x - cameraMod_Max / 2 + cameraMod_X;
			y = y - cameraMod_Max / 2 + cameraMod_Y;

		}


		/*if(x != oldX){
			System.out.println("in1");
			if(camCountX*cameraDelay<(Math.abs(x-oldX))){
				camCountX=camCountX*cameraDelay;
			}else{
				camCountX=Math.abs(x-oldX);
			}
			
			if(x>oldX){
				x = oldX + camCountX;
			}
			if(x<oldX){
				x = oldX - camCountX;
			}
		}else{
			camCountX = 1;
		}
			
		if(y != oldY){
			System.out.println("in2");
			if(camCountY*cameraDelay<(Math.abs(y-oldY))){
				camCountY=camCountY*cameraDelay;
			}else{
				camCountY=Math.abs(y-oldY);
			}
			
			if(y>oldY){
				y = oldY + camCountY;
			}
			if(y<oldY){
				y = oldY - camCountY;
			}
		}else{
			camCountY = 1;
		}
		
		oldX = x;
		oldY = y;*/
		
		m.setPosition(x,y);
	}

	public void draw(Graphics2D g) {

		g.setColor(Color.BLACK);
		g.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);

		gfx.draw(g, 3);
		
		background.draw(g);

        gfx.draw(g, 1);
		
		for(int i=0; i<worldGenerator.getActiveLevel().getList().size();i++) {
			MapObject object = worldGenerator.getActiveLevel().getList().get(i);
            if (object.getActive() == true) {
                if (object.render) {
				    if (object.visible) {
                        object.draw(g);
                    }
				}
			}
		}

		// draw bg maybe
		m.draw(g);
		
		// draw events
		events.draw(g);
		
		gfx.draw(g, 0);

		showNumbers.draw(g);

		overlay.draw(g);
		
		if(showBreak){
			//FontMetrics f24 = g.getFontMetrics(utl.Monospace24);
			
			/*g.setColor(Color.BLACK);
			g.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);
			
			g.setFont(utl.MonospaceBold24);

			g.setColor(Color.WHITE);
			g.drawString(breakText, GamePanel.WIDTH/2-f24.stringWidth(breakText)/2, GamePanel.HEIGHT/2);
			*/
		}
		
	}
	
	public static void showBreak(String text){
		breakText = text;
		showBreak = true;
	}
	
	public static void hideBreak(){
		showBreak = false;
	}

	public void handleInput() {

		if(!overlay.layer_INVENTORY) {

			if (Keys.keyState[Keys.UP]) {

			}

			if (Keys.isPressed(Keys.UP)) {
				//player.MoveUp();
			}

			if (Keys.isPressed(Keys.BUTTON1)) {
				player.MoveUp();
                System.out.println("... a");
                System.out.println("Jump!");
			}

			if (Keys.keyState[Keys.BUTTON1]) {
                /*if(!player.getJumping()) {
                    player.MoveUp();
                    System.out.println("... a");
                    System.out.println("Jump!");
                }*/


			} else {
				//if (player.getWallrunning()) {
					//player.jumpKeyUp();
				//}
			}

			if (Keys.isPressed(Keys.BUTTON2)) {
				player.dash();
			}

			if (Keys.isPressed(Keys.BUTTON3)) {
				player.interact();
			}

			if (Keys.isPressed(Keys.BUTTON4)) {
				player.handleOverlay();
			}

			if (Keys.isPressed(Keys.LEFT)) {
				player.setMLeft(true);
				player.setMRight(false);
			}

			if (Keys.isPressed(Keys.RIGHT)) {
				player.setMLeft(false);
				player.setMRight(true);
			}
		
		/*if(Keys.keyState[Keys.DOWN]) {
			if(Keys.isPressed(Keys.DOWN)) {
				player.block(true);
			}
		}else{
			player.block(false);
		}*/

			if (Keys.isPressed(Keys.ESCAPE)) {
				//collision.getList().clear();

				//worldGenerator.getLevels().clear();
				//worldGenerator.getMainBranch().clear();

				worldGenerator.returnToBase();


				//gsm.setState(GameStateManager.MENUSTATE);
			}

			if (Keys.keyState[Keys.LEFT]) {
				if (player.getMLeft())
					player.MoveLeft();
			} else {
				player.setMLeft(false);
				if (Keys.keyState[Keys.RIGHT]) {
					player.setMRight(true);
				}
			}
			if (Keys.keyState[Keys.RIGHT]) {
				if (player.getMRight())
					player.MoveRight();
			} else {
				player.setMRight(false);
				if (Keys.keyState[Keys.LEFT]) {
					player.setMLeft(true);
				}
			}

		}else{

			overlay.handleInput();

		}
		
	}
	
}










