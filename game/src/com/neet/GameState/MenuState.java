package com.neet.GameState;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;

import javax.imageio.ImageIO;

import com.neet.Audio.JukeBox;
import com.neet.Entity.npc;
import com.neet.Entity.tombstones;
import com.neet.Handlers.Keys;
import com.neet.Main.GamePanel;
import com.neet.Main.config;
import com.neet.events.events;
import com.neet.events.titelscreen;
import com.neet.events.utl;

public class MenuState extends GameState {
	
	private BufferedImage head;
	
	private int ACTIVE; 
	
	private int[] OPTIONS_default = {
		1,
		5
	};

	
	private int[] OPTIONS_default_save;
		
	private int[] START_default = {
			
	};
	
	private int[] MENU_default;
	

	private String[] START_data = {
		"Start",
		"Options",
		"Quit"
	};
	
	private String[] OPTIONS_data = {
		"difficulty",
		"volume",
		"Back"
	};

	private ArrayList[] body_sprites = {
			utl.head,
			utl.top,
			utl.weapon,
			utl.bottom
	};

	private int[] BS_default = {
			0,0,0,0
	};
	
	private String[] OPTIONS_volume_data = {
		"0",
		"1",
		"2",
		"3",
		"4",
		"5",
		"6",
		"7",
		"8",
		"9",
		"10"
	};
	
	private String[] OPTIONS_difficulty_data = {
		"easy",
		"normal",
		"hard"
	};

	
	ArrayList<String> MENU = new ArrayList<String>();
	ArrayList<String> START = new ArrayList<String>();
	ArrayList<String> OPTIONS = new ArrayList<String>();
	ArrayList<ArrayList<String>> MENU_L2 = new ArrayList<ArrayList<String>>();
	ArrayList<ArrayList<String>> OPTIONS_L2 = new ArrayList<ArrayList<String>>();
	ArrayList<ArrayList<String>> START_L2 = new ArrayList<ArrayList<String>>();
	ArrayList<String> OPTIONS_difficulty = new ArrayList<String>();
	ArrayList<String> OPTIONS_volume = new ArrayList<String>();
	
	private int currentChoice = 0;
	
	//font
	public static Color titleColor;
	public static Font titleFont;
	public static Font titleFont2;
	public static Font titleFont3;
	
	public static Font font;
	public static Font font2;
	public static Font font3;
	public static Font font4;
	public static Font font5;
	
	
	public int M_ACTIVE;
	
	public final int M_START = 0;
	public final int M_OPTIONS = 1;
	public final int M_SELECT = 2;
	public final int M_PROFILE = 3;
	
	
	
	//menu postition
	private int lineOffset = 30;
	private int lineWidth = 2;
	
	private int baseX = lineOffset+lineWidth+10;
	private int baseXline2 = GamePanel.WIDTH/2;
	private int baseY = lineOffset+lineWidth+20;
	private int line = 18;
	private int offsetMenu = 20;
	
	private int ActivePoint = 0;
	private int RealActivePoint = 0;
	private int ActivePointMax = 10;
	private int ActivePointMod = 2;
	private boolean ActivePointDir = true;
	
	private int activeTS = 0;
	private int animTS = 0;
	private int speedTS = 10;
	private boolean switchingTS = false;
	private boolean switchingTSdir = true;
	private boolean enterTS = true;
	
	private String titel = "!#$FDP";
	private String titel2 = "TIME";
	
	public static Color main_C = Color.WHITE;
	public static Color alt_C = Color.GRAY;
	
	public static Color shadow = Color.BLACK;
	
	public titelscreen TitelScreen = new titelscreen();
	
	private BufferedImage zombie;
	
	private int zombieAnim = 0;
	private int zombieAnimGoal = 120;
	private int zombieAnimSpeed = 3;
	
	private int zombieDelay = 0;
	private int zombieDelayGoal = 100;
	private boolean start = false;
	private boolean loading = false;
	private boolean loading_start = false;
	
	public MenuState(GameStateManager gsm) {
		
		super(gsm);

		M_ACTIVE = M_PROFILE;

					
		// Fill Menu
		fill_ARRAYLIST(START_data, START);
		fill_ARRAYLIST(OPTIONS_data, OPTIONS);
		fill_ARRAYLIST(OPTIONS_difficulty_data, OPTIONS_difficulty);
		fill_ARRAYLIST(OPTIONS_volume_data, OPTIONS_volume);
			
		OPTIONS_L2.add(0, OPTIONS_difficulty);
		OPTIONS_L2.add(1, OPTIONS_volume);
					
		//MENU_default = START_default.clone();
		//MENU_default_save = START_default.clone();
		
		OPTIONS_default_save = OPTIONS_default.clone();
					
		MENU = START;
		MENU_L2 = START_L2;
					
		//System.out.println(MENU_L2);
		
		// titles and fonts
		titleColor = Color.WHITE;
		titleFont = new Font("Arial", Font.BOLD, 34);
		titleFont2 = new Font("Arial", Font.BOLD, 34);
		titleFont3 = new Font("Arial", Font.BOLD, 34);
				
		//titleFont = new Font("Times New Roman", Font.PLAIN, 28);
		font = new Font("Arial", Font.PLAIN, 16);
		font2 = new Font("Arial", Font.PLAIN, 10);
		font3 = new Font("Times New Roman", Font.PLAIN, 16);
		font4 = new Font("Times New Roman", Font.PLAIN, 40);
		font5 = new Font("Arial", Font.PLAIN, 30);
					
		try{

			zombie = ImageIO.read(getClass().getResourceAsStream("/Sprites/Other/zombie_2.png"));
		}catch(Exception e) {

		}

	}
	
	public void init() {}
	
	public void update() {
		
		if(loading_start){
			gsm.setState(GameStateManager.GAME);
		}
		
		handleActivePoint();
		handleTSanim();
		handleStart();
		
		// check keys
		handleInput();
		
		TitelScreen.update();
		
		if(Arrays.equals(OPTIONS_default, OPTIONS_default_save) != true){
			if(!OPTIONS.contains("Save")){
				OPTIONS.add("Save");
			}
		}else{
			if(OPTIONS.contains("Save")){
				OPTIONS.remove("Save");
			}
		}
		
	}
	
	public void draw(Graphics2D g) {
		
		FontMetrics fmetric = g.getFontMetrics(font);
		FontMetrics fmetricTF = g.getFontMetrics(titleFont);
		FontMetrics fmetricTFs = g.getFontMetrics(titleFont2);
		FontMetrics fmetricF4 = g.getFontMetrics(font4);
		FontMetrics fmetricF3 = g.getFontMetrics(font3);
		FontMetrics fmetricF2 = g.getFontMetrics(font2);
		
		// draw bg
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);


		if(M_ACTIVE == M_PROFILE) {
			for (int i = 0; i < body_sprites.length; i++) {
				if (currentChoice == i)
					drawFont(String.valueOf(BS_default[i]), baseX, baseY + offsetMenu + i * line, Color.getHSBColor(0f, 1f, 1f), font, 0, 1, g);
				else drawFont(String.valueOf(BS_default[i]), baseX, baseY + offsetMenu + i * line, Color.WHITE, font, 0, 1, g);

				/*if (currentChoice == i)
					drawFont(String.valueOf(body_sprites[i].get(BS_default[i])), baseX, baseY + offsetMenu + i * line, Color.getHSBColor(0f, 1f, 1f), font, 0, 1, g);
				else drawFont(String.valueOf(body_sprites[i].get(BS_default[i])), baseX, baseY + offsetMenu + i * line, Color.WHITE, font, 0, 1, g);*/
			}
		}



		
		// titelscreen
		//TitelScreen.draw(g);
		
		// draw box
		/*g.setColor(Color.WHITE);
		
		g.fillRect(lineOffset, lineOffset, GamePanel.WIDTH/2-fmetricTF.stringWidth(titel)/2-lineOffset-10, lineWidth);
		g.fillRect(GamePanel.WIDTH/2+fmetricTF.stringWidth(titel)/2+10, lineOffset, GamePanel.WIDTH/2-fmetricTF.stringWidth(titel)/2-lineOffset-10, lineWidth);
		g.fillRect(lineOffset, lineOffset, lineWidth, GamePanel.HEIGHT-lineOffset*2);
		
		g.fillRect(GamePanel.WIDTH-lineOffset-lineWidth, lineOffset, lineWidth, GamePanel.HEIGHT-lineOffset*2);
		g.fillRect(lineOffset, GamePanel.HEIGHT-lineOffset-lineWidth, GamePanel.WIDTH-lineOffset*2, lineWidth);*/
		
		
	
		// draw title
		
		//drawFont(titel, GamePanel.WIDTH/2-fmetricTF.stringWidth(titel)/2, lineOffset+10, Color.WHITE, titleFont, 0, 2, g);
		
		//g.drawImage(utility.Title,baseX, 35, null);

		
		
		if(M_ACTIVE == M_START){
			
			for(int i=0; i<START.size(); i++){

				//if(currentChoice == i)drawFont(START.get(i), baseX, baseY+offsetMenu+i*line, Color.getHSBColor(0f, ((float)(RealActivePoint*0.1)), 1f), font, 0, 1, g);
				if(currentChoice == i)drawFont(START.get(i), baseX, baseY+offsetMenu+i*line, Color.getHSBColor(0f, 1f, 1f), font, 0, 1, g);
				else drawFont(START.get(i), baseX, baseY+offsetMenu+i*line, Color.WHITE, font, 0, 1, g);



			}
			
		}
		
		if(M_ACTIVE == M_OPTIONS){
			
			// draw menu points
			for(int i=0; i<OPTIONS.size(); i++){
				//if(currentChoice == i)drawFont(OPTIONS.get(i), baseX, baseY+offsetMenu+i*line, Color.getHSBColor(0f, ((float)(RealActivePoint*0.1)), 1f), font, 0, 1, g);
				if(currentChoice == i)drawFont(OPTIONS.get(i), baseX, baseY+offsetMenu+i*line, Color.getHSBColor(0f, 1f, 1f), font, 0, 1, g);
				else drawFont(OPTIONS.get(i), baseX, baseY+offsetMenu+i*line, Color.WHITE, font, 0, 1, g);
				//g.drawString(MENU.get(i), baseX+ActivePoint/10, baseY+offsetMenu+i*line);
				
				
				
					try {
						if(OPTIONS_L2.get(i) != null){
							if(currentChoice == i){
								if(OPTIONS_default[i] > 0){
									drawFont("<", baseXline2-fmetric.stringWidth("<")-1, baseY+offsetMenu+i*line, Color.WHITE, font, 0, 1, g);
								}else{
									drawFont("<", baseXline2-fmetric.stringWidth("<")-1, baseY+offsetMenu+i*line, Color.GRAY, font, 0, 1, g);
								}
								if(OPTIONS_default[i] < MENU_L2.get(i).size() - 1){
									drawFont(">", baseXline2+fmetric.stringWidth(OPTIONS_L2.get(i).get(OPTIONS_default[i]))+1, baseY+offsetMenu+i*line, Color.WHITE, font, 0, 1, g);
								}else{
									drawFont(">", baseXline2+fmetric.stringWidth(OPTIONS_L2.get(i).get(OPTIONS_default[i]))+1, baseY+offsetMenu+i*line, Color.GRAY, font, 0, 1, g);
								}
							}
								
							drawFont(OPTIONS_L2.get(i).get(OPTIONS_default[i]), baseXline2, baseY+offsetMenu+i*line+1, Color.WHITE, font, 0, 1, g);

						}
					} catch (Exception e) {
					}
			}
			
		}
		
		if(M_ACTIVE == M_SELECT){
			
			int level = tombstones.getTombstones().get(activeTS).level;
			int ATK = tombstones.getTombstones().get(activeTS).ATK;
			int DEF = tombstones.getTombstones().get(activeTS).DEF;
			String name = tombstones.getTombstones().get(activeTS).name;
			
			g.setColor(Color.WHITE);
			g.fillRect(GamePanel.WIDTH/2-50-animTS, GamePanel.HEIGHT/2-50, 100, 100);
			g.fillOval(GamePanel.WIDTH/2-50-animTS, GamePanel.HEIGHT/2-100, 100, 100);
			
			g.drawImage(zombie,GamePanel.WIDTH/2-10, GamePanel.HEIGHT/2+80-zombieAnim, null);
			
			if(activeTS > 0){
				drawFont("<", 100, 150, Color.WHITE, font4, 0, 0, g);
			}else{
				drawFont("<", 100, 150, Color.GRAY, font4, 0, 0, g);
			}
			if(activeTS < tombstones.getTombstones().size() - 1){
				drawFont(">", GamePanel.WIDTH-100-fmetricF4.stringWidth(">"), 150, Color.WHITE, font4, 0, 0, g);
			}else{
				drawFont(">", GamePanel.WIDTH-100-fmetricF4.stringWidth(">"), 150, Color.GRAY, font4, 0, 0, g);
			}
			
			if(enterTS){
				drawFont("ENTER", GamePanel.WIDTH/2-50, GamePanel.HEIGHT/2+100, Color.WHITE, font5, 0, 0, g);
			}else{
				drawFont("ENTER", GamePanel.WIDTH/2-50, GamePanel.HEIGHT/2+100, Color.GRAY, font5, 0, 0, g);
			}
			
			drawFont(name, GamePanel.WIDTH/2-fmetricF3.stringWidth(name)/2-animTS, GamePanel.HEIGHT/2-40, Color.BLACK, font3, 0, 0, g);
			
			drawFont("LEVEL", GamePanel.WIDTH/2-40-animTS, GamePanel.HEIGHT/2-20, Color.BLACK, font2, 0, 0, g);
			drawFont(String.valueOf(level), GamePanel.WIDTH/2+50-fmetricF2.stringWidth(String.valueOf(level))-10-animTS, GamePanel.HEIGHT/2-20, Color.BLACK, font2, 0, 0, g);
			
			drawFont("ATK", GamePanel.WIDTH/2-40-animTS, GamePanel.HEIGHT/2-10, Color.BLACK, font2, 0, 0, g);
			drawFont(String.valueOf(ATK), GamePanel.WIDTH/2+50-fmetricF2.stringWidth(String.valueOf(ATK))-10-animTS, GamePanel.HEIGHT/2-10, Color.BLACK, font2, 0, 0, g);
			
			drawFont("DEF", GamePanel.WIDTH/2-40-animTS, GamePanel.HEIGHT/2, Color.BLACK, font2, 0, 0, g);
			drawFont(String.valueOf(DEF), GamePanel.WIDTH/2+50-fmetricF2.stringWidth(String.valueOf(DEF))-10-animTS, GamePanel.HEIGHT/2, Color.BLACK, font2, 0, 0, g);

		}
		
		if(loading){
			loading_start = true;
			load(g, fmetricF4);
		}
		
		
		
		// other
		drawFont("alpha v0.4", 3, 10, Color.GREEN, font2, 0, 0, g);

	}
	
	private void load(Graphics2D g, FontMetrics fmetricF4){
		g.setColor(Color.RED);
		g.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);
		
		drawFont("LOADING", GamePanel.WIDTH/2-fmetricF4.stringWidth("LOADING")/2, GamePanel.HEIGHT/2+10, Color.WHITE, font4, 0, 0, g);

	}
	
	private void handleStart(){
		if(start){
			if(zombieAnim<zombieAnimGoal){
				zombieAnim+=zombieAnimSpeed;
				if(zombieAnim>=zombieAnimGoal){
					JukeBox.play("jaw");
				}
			}else{
				if(zombieDelay<zombieDelayGoal){
					zombieDelay++;
				}else{
					loading = true;
				}
			}
		}
	}
	
	private void handleActivePoint(){
		if(ActivePointDir){
			if(ActivePoint/ActivePointMod<ActivePointMax){
				ActivePoint = ActivePoint+1;
			}else{
				//ActivePointDir = false;
			}
		}
		if(!ActivePointDir){
			if(ActivePoint/ActivePointMod>0){
				ActivePoint = ActivePoint-1;
			}else{
				ActivePointDir = true;
			}
		}
		RealActivePoint = ActivePoint/ActivePointMod;
	}
	
	private void handleTSanim(){
		
		if(switchingTSdir){
			if(switchingTS){
				if(animTS>0-GamePanel.WIDTH/2){
					animTS-=speedTS;
				}else{
					switchingTS = false;
					animTS = GamePanel.WIDTH/2;
						
					if(activeTS>0)activeTS--;
						
				}
			}else{
				if(animTS>0){
					animTS-=speedTS;
					if(animTS == 0)enterTS = true;
				}
			}
		}
		
		if(!switchingTSdir){
			if(switchingTS){
				if(animTS<GamePanel.WIDTH/2){
					animTS+=speedTS;
				}else{
					switchingTS = false;
					animTS = 0-GamePanel.WIDTH/2;
					if(activeTS<tombstones.getTombstones().size()-1)activeTS++;
						
				}
			}else{
				if(animTS<0){
					animTS+=speedTS;
					if(animTS == 0)enterTS = true;
				}
			}
		}
	}
	
	//selecting points
	private void select() {

		if(M_ACTIVE == M_SELECT && enterTS){
			start = true;
		}
		if(M_ACTIVE == M_START || M_ACTIVE == M_OPTIONS){
			if(MENU.get(currentChoice) == "Start") {
				//gsm.setState(GameStateManager.GAME);
				M_ACTIVE = M_SELECT;
				tombstones.getTombstones().clear();
				tombstones.createTombstones(10);
			}
			else if(MENU.get(currentChoice) == "Options") {
				MENU_L2 = OPTIONS_L2;
				MENU = OPTIONS;
				MENU_default = OPTIONS_default.clone();
				//MENU_default_save = OPTIONS_default.clone();
				currentChoice = 0;
				M_ACTIVE = M_OPTIONS;
			}
			else if(MENU.get(currentChoice) == "Quit") {
				System.exit(0);
			}
			else if(MENU.get(currentChoice) == "Back") {
				MENU = START;
				MENU_L2 = START_L2;
				MENU_default = START_default.clone();
				//MENU_default_save = START_default.clone();
				currentChoice = 0;
				M_ACTIVE = M_START;
				OPTIONS_default = OPTIONS_default_save.clone();
			}
			else if(MENU.get(currentChoice) == "Save") {
				/*if(MENU_default != MENU_default_save){
					MENU_default_save = MENU_default.clone();
				}*/
				//MENU_default_save = MENU_default.clone();
				//OPTIONS_default = MENU_default.clone();
				OPTIONS_default_save = OPTIONS_default.clone();
				currentChoice = currentChoice-1;
				
				for(int i=0; i<OPTIONS.size()-1; i++){
					if(OPTIONS.get(i) == "volume") config.volume = OPTIONS_default[i];
					if(OPTIONS.get(i) == "difficulty") config.difficulty = OPTIONS_default[i];
				}
			}
		}
	}
	
	private void drawFont(String string,int x,int y,Color color,Font font,int offsetX, int offsetY, Graphics2D g){
		g.setFont(font);
		
		g.setColor(shadow);
		g.drawString(string, x+offsetX, y+offsetY);
		
		g.setColor(color);
		g.drawString(string, x, y);
	}
	
	public void fill_ARRAYLIST(String[] FROM, ArrayList<String> TO) {
		TO.clear();
		for(int i=0; i<FROM.length;i++){
			TO.add(FROM[i]);
		}
	}
	
	/*public void fill_ARRAYLIST(ArrayList<String> FROM, ArrayList<ArrayList<String>> TO) {
		TO.clear();
		for(int i=0; i<FROM.size();i++){
			TO.add(FROM);
		}
	}*/
	
	//keys input
	public void handleInput() {
		if(M_ACTIVE == M_PROFILE) {


			if (Keys.isPressed(Keys.UP)) {
				if (currentChoice > 0) {
					JukeBox.play("menu", 0);
					currentChoice--;
				}
			}
			if (Keys.isPressed(Keys.DOWN)) {
				if (currentChoice < body_sprites.length - 1) {
					JukeBox.play("menu", 0);
					currentChoice++;
				}
			}
			if (Keys.isPressed(Keys.LEFT)) {
				if (BS_default[currentChoice]-1 >= 0) {
					BS_default[currentChoice] = BS_default[currentChoice] - 1;
				}
			}
			if (Keys.isPressed(Keys.RIGHT)) {
				if (BS_default[currentChoice]+1 < body_sprites[currentChoice].size()) {
					BS_default[currentChoice] = BS_default[currentChoice] + 1;
				}
			}



		}else{
			if (Keys.isPressed(Keys.ENTER)) {
				JukeBox.play("menuselect");
				select();
			}
			if (Keys.isPressed(Keys.UP)) {
				if (currentChoice > 0) {
					JukeBox.play("menu", 0);
					currentChoice--;
					//currentDepth = 0;
				}
				ActivePoint = 0;
				RealActivePoint = 0;
				ActivePointDir = true;
			}
			if (Keys.isPressed(Keys.DOWN)) {
				if (currentChoice < MENU.size() - 1) {
					JukeBox.play("menu", 0);
					currentChoice++;
					//currentDepth = 0;

				}
				ActivePoint = 0;
				RealActivePoint = 0;
				ActivePointDir = true;
			}
			if (Keys.isPressed(Keys.LEFT)) {
				if (M_ACTIVE == M_SELECT) {
					if (activeTS > 0) {
						if (!switchingTS) {
							switchingTS = true;
							switchingTSdir = true;
							enterTS = false;
						}
					}
				}
				try {
					if (OPTIONS_default[currentChoice] > 0) {
						JukeBox.play("menu", 0);
						OPTIONS_default[currentChoice] = OPTIONS_default[currentChoice] - 1;
					}

				} catch (Exception e) {
					//e.printStackTrace();
				}
			}
			if (Keys.isPressed(Keys.RIGHT)) {
				if (M_ACTIVE == M_SELECT) {
					if (activeTS < tombstones.getTombstones().size() - 1) {
						if (!switchingTS) {
							switchingTS = true;
							switchingTSdir = false;
							enterTS = false;
						}
					}
				}
				try {
					if (OPTIONS_default[currentChoice] < OPTIONS_L2.get(currentChoice).size() - 1) {
						JukeBox.play("menu", 0);
						OPTIONS_default[currentChoice] = OPTIONS_default[currentChoice] + 1;
					}
				} catch (Exception e) {
					//e.printStackTrace();
				}
			}
			if (Keys.isPressed(Keys.ESCAPE)) {
				MENU = START;
				MENU_L2 = START_L2;
				MENU_default = START_default.clone();
				//MENU_default_save = START_default.clone();
				currentChoice = 0;
				M_ACTIVE = M_START;
				OPTIONS_default = OPTIONS_default_save.clone();
			}

		}
	}
	
}

