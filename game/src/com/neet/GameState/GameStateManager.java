package com.neet.GameState;

import com.neet.Audio.JukeBox;
import com.neet.Entity.loader;
import com.neet.Main.GamePanel;
import com.neet.events.events;
import com.neet.events.utl;

public class GameStateManager {
	
	public GameState[] gameStates;
	public int currentState;
		
	public static final int NUMGAMESTATES = 3;
	public static final int MENUSTATE = 0;
	public static final int GAME = 1;
	public static final int SPLASH = 2;
	
	public GameStateManager() {
		
		JukeBox.init();
		events.init();
		utl.init();
		
		gameStates = new GameState[NUMGAMESTATES];

		currentState = SPLASH;
		loadState(currentState);
		
	}
	
	private void loadState(int state) {
		if(state == SPLASH){
			gameStates[state] = new splash(this);
		}
		if(state == MENUSTATE){
			gameStates[state] = new MenuState(this);
		}
		else if(state == GAME){
			gameStates[state] = new running(this);
		}
	}
	
	private void unloadState(int state) {
		gameStates[state] = null;
	}
	
	public void setState(int state) {
		unloadState(currentState);
		currentState = state;
		loadState(currentState);
	}
		
	public void update() {
		if(gameStates[currentState] != null) gameStates[currentState].update();
	}
	
	public void draw(java.awt.Graphics2D g) {
		if(gameStates[currentState] != null) gameStates[currentState].draw(g);
		else {
			g.setColor(java.awt.Color.BLACK);
			g.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);
		}
	}
	
}