package com.neet.Main;

import javax.swing.JFrame;
import java.awt.*;

public class Game {

    private static String OS = System.getProperty("os.name").toLowerCase();
	
	public static void main(String[] args) {



		JFrame window = new JFrame("game");
		window.add(new GamePanel());
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setResizable(false);
		window.pack();
		window.setLocationRelativeTo(null);
		window.setVisible(true);

        if (isMac()) {
            System.out.println("This is Mac");
            com.apple.eawt.FullScreenUtilities.setWindowCanFullScreen(window,true);
            com.apple.eawt.Application.getApplication().requestToggleFullScreen(window);
        } else {
            System.out.println("This is Unix or Linux");
            GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
            gd.setFullScreenWindow(window);
        }



	}

    public static boolean isWindows() {

        return (OS.indexOf("win") >= 0);

    }

    public static boolean isMac() {

        return (OS.indexOf("mac") >= 0);

    }

    public static boolean isUnix() {

        return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );

    }
	
}
