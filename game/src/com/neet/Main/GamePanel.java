package com.neet.Main;


import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;






import javax.swing.JPanel;

import com.neet.GameState.GameStateManager;
import com.neet.Handlers.Keys;


@SuppressWarnings("serial")
public class GamePanel extends JPanel implements Runnable, KeyListener{

    GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
    int screenWidth = gd.getDisplayMode().getWidth();
    int screenHeight = gd.getDisplayMode().getHeight();
		
	// dimensions
	public static final int BASE_WIDTH = 300;
	public static final int BASE_HEIGHT = 200;

    public static int WIDTH = 300;
    public static int HEIGHT = 200;
	public static int SCALE; // float for precis scaling ni blure ye?
	
	public static int FPS = 0;
	public static int UPDATES = 0;
	
	//Canvas3D myCanvas3D = new Canvas3D(null);
	
	// game thread
	private Thread thread;
	private boolean running;
	private int targetFPS = 30;
	private long targetTime = 1000 / targetFPS;
	private int frameCount = 0;
	private int updateCount2 = 0;
	
	public static float interpolation = 0;
	//This value would probably be stored elsewhere.
	public static final int GAME_HERTZ = 120;
    //Calculate how many ns each frame should take for our target game hertz.
    final double TIME_BETWEEN_UPDATES = 1000000000 / GAME_HERTZ;
    //At the very most we will update the game this many times before a new render.
    //If you're worried about visual hitches more than perfect timing, set this to 1.
    final int MAX_UPDATES_BEFORE_RENDER = 5;
    //We will need the last update time.
    double lastUpdateTime = System.nanoTime();
    //Store the last time we rendered.
    double lastRenderTime = System.nanoTime();
    
    //If we are able to get as high as this FPS, don't render again.
    final double TARGET_FPS = 1000;
    final double TARGET_TIME_BETWEEN_RENDERS = 1000000000 / TARGET_FPS;
    
    //Simple way of finding FPS.
    int lastSecondTime = (int) (lastUpdateTime / 1000000000);
	
	// image
	private BufferedImage image;
	private Graphics2D g;
	
	// game state manager
	private GameStateManager gsm;
	
	// other
	private boolean recording = false;
	private int recordingCount = 0;
	private boolean screenshot;
	
	public GamePanel() {

		super();

        for(int i = 1;i<10;i++){
            if(WIDTH*i<screenWidth && HEIGHT*i<screenHeight){
                SCALE = i;
            }
        }

        WIDTH = (screenWidth/SCALE);
        HEIGHT = (screenHeight/SCALE) ;

		setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		setFocusable(true);
		requestFocus();
	}
	
	public void addNotify() {
		super.addNotify();
		if(thread == null) {
			thread = new Thread(this);
			addKeyListener(this);
			thread.start();
		}
	}
	
	private void init() {
		
		image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		g = (Graphics2D) image.getGraphics();
		/*g.setRenderingHint(
			RenderingHints.KEY_TEXT_ANTIALIASING,
			RenderingHints.VALUE_TEXT_ANTIALIAS_ON
		);*/
		
		
		running = true;
		
		gsm = new GameStateManager();
		
		
		
	}
	
	public void run() {
		init();
		
		long start2 = 0;
		
		long start;
		long elapsed;
		long wait;
		long finished;
		
		int theFPS = 0;
		
		while(running){
			gameLoop();
		}
		
		// game loop
		/*while(running) {
			
			theFPS++;
			
			start = System.nanoTime();
			
			update();
			draw();
			drawToScreen();
			
			elapsed = System.nanoTime() - start;
			
			wait = targetTime - elapsed/1000000;
			if(wait < 0) wait = 1;
			
			try {
				Thread.sleep(wait);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			
			if(start2 == 0){
				start2 = System.nanoTime();
			}
			
			if((start-start2)>1000000000){
				System.out.println("Target = "+targetTime);
				System.out.println("Elapsed = "+elapsed/1000000);
				System.out.println("FPS = "+theFPS);
				FPS = theFPS;
				theFPS = 0;
				start2 = 0;
			}
			
			
			
		}*/
		
	}
	
	private void gameLoop()
	   {

	         double now = System.nanoTime();
	         int updateCount = 0;
	         
	         if (true)
	         {
	             //Do as many game updates as we need to, potentially playing catchup.
	            while( now - lastUpdateTime > TIME_BETWEEN_UPDATES && updateCount < MAX_UPDATES_BEFORE_RENDER )
	            {
	            	update();
	            	lastUpdateTime += TIME_BETWEEN_UPDATES;
	            	updateCount++;
	            	updateCount2++;
	            	
	            	//System.out.println("updated");
	            }
	   
	            //If for some reason an update takes forever, we don't want to do an insane number of catchups.
	            //If you were doing some sort of game that needed to keep EXACT time, you would get rid of this.
	            if ( now - lastUpdateTime > TIME_BETWEEN_UPDATES)
	            {
	               lastUpdateTime = now - TIME_BETWEEN_UPDATES;
	            }
	         
	            //Render. To do so, we need to calculate interpolation for a smooth render.
	            interpolation = Math.min(1.0f, (float) ((now - lastUpdateTime) / TIME_BETWEEN_UPDATES) );
	            draw();
	            drawToScreen();
	            frameCount++;
	            
	            //System.out.println("drawn");
	            
	            lastRenderTime = now;
	         
	            //Update the frames we got.
	            int thisSecond = (int) (lastUpdateTime / 1000000000);
	            if (thisSecond > lastSecondTime)
	            {
	            	System.out.println(FPS+" fps");
	            	FPS = frameCount;
	            	UPDATES = updateCount2;
	            	updateCount2 = 0;
	            	frameCount = 0;
	            	lastSecondTime = thisSecond;
	            }
	         
	            //Yield until it has been at least the target time between renders. This saves the CPU from hogging.
	            while ( now - lastRenderTime < TARGET_TIME_BETWEEN_RENDERS && now - lastUpdateTime < TIME_BETWEEN_UPDATES)
	            {
	               //Thread.yield();
	            
	               //This stops the app from consuming all your CPU. It makes this slightly less accurate, but is worth it.
	               //You can remove this line and it will still work (better), your CPU just climbs on certain OSes.
	               //FYI on some OS's this can cause pretty bad stuttering. Scroll down and have a look at different peoples' solutions to this.
	               //try {Thread.sleep(1);} catch(Exception e) {} 
	            
	               now = System.nanoTime();
	            }
	         }
	}
	
	public void stop(){
		if(running){
			running = false;
		}
	}
	
	private void update() {
		gsm.update();
		//Keys.update(); // this neeeeeeeeds to get fixed!!!!
	}
	private void draw() {
		gsm.draw(g);
	}
	private void drawToScreen() {
		Graphics g2 = getGraphics();
		g2.drawImage(image, 0, 0, WIDTH * SCALE, HEIGHT * SCALE, null);
		g2.dispose();
		if(screenshot) {
			screenshot = false;
			try {
				java.io.File out = new java.io.File("screenshot " + System.nanoTime() + ".gif");
				javax.imageio.ImageIO.write(image, "gif", out);
			}
			catch(Exception e) {}
		}
		if(!recording) return;
		try {
			java.io.File out = new java.io.File("C:\\out\\frame" + recordingCount + ".gif");
			javax.imageio.ImageIO.write(image, "gif", out);
			recordingCount++;
		}
		catch(Exception e) {}
	}

	public void keyTyped(KeyEvent key) {}
	public void keyPressed(KeyEvent key) {
		if(key.isControlDown()) {
			if(key.getKeyCode() == KeyEvent.VK_R) {
				recording = !recording;
				return;
			}
			if(key.getKeyCode() == KeyEvent.VK_S) {
				screenshot = true;
				return;
			}
		}
		Keys.keySet(key.getKeyCode(), true);
	}
	public void keyReleased(KeyEvent key) {
		Keys.keySet(key.getKeyCode(), false);
	}

}