package com.neet.events;

import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;

import com.neet.Audio.JukeBox;

public final class utl {
	
	public static Font Impact36;
	public static Font Impact28;
    public static Font Impact18;
	public static Font Arial14;
	public static Font ArialBold36;
	public static Font ArialBold28;
	public static Font ArialBold24;
	public static Font Arial10;
	public static Font Arial36;
	public static Font Arial28;
	public static Font Times36;
	public static Font Times28;
	public static Font Times10;
	public static Font Monospace24;
	public static Font MonospaceBold24;
	public static Font Monospace10;
    public static Font Monospace9;

    public static ArrayList<BufferedImage> onhead = new ArrayList<BufferedImage>();
	public static ArrayList<BufferedImage> head = new ArrayList<BufferedImage>();
	public static ArrayList<BufferedImage> top = new ArrayList<BufferedImage>();
	public static ArrayList<BufferedImage> bottom = new ArrayList<BufferedImage>();
	public static ArrayList<BufferedImage> weapon = new ArrayList<BufferedImage>();
    public static ArrayList<BufferedImage> hat = new ArrayList<BufferedImage>();

	public static ArrayList<BufferedImage> head_enemy = new ArrayList<BufferedImage>();
	public static ArrayList<BufferedImage> top_enemy = new ArrayList<BufferedImage>();
	public static ArrayList<BufferedImage> bottom_enemy = new ArrayList<BufferedImage>();
	public static ArrayList<BufferedImage> weapon_enemy = new ArrayList<BufferedImage>();

	public static BufferedImage Title;
	public static BufferedImage image_other;
	public static BufferedImage Wall;
	
	public static Random random = new Random(); 
	
	
	public static void init(){
		
		
		Impact36 = new Font("Impact", Font.PLAIN, 36);
		Impact28 = new Font("Impact", Font.PLAIN, 28);
        Impact18 = new Font("Impact", Font.PLAIN, 18);
		Arial14 = new Font("Arial", Font.PLAIN, 14);
		Arial10 = new Font("Arial", Font.PLAIN, 10);
		ArialBold36 = new Font("Arial", Font.BOLD, 36);
		ArialBold28 = new Font("Arial", Font.BOLD, 28);
		ArialBold24 = new Font("Arial", Font.BOLD, 24);
		Arial36 = new Font("Arial", Font.PLAIN, 36);
		Arial28 = new Font("Arial", Font.PLAIN, 28);
		Times36 = new Font("Times New Roman", Font.PLAIN, 36);
		Times28 = new Font("Times New Roman", Font.PLAIN, 28);
		Times10 = new Font("Times New Roman", Font.PLAIN, 10);
		Monospace24 = new Font("Monospace", Font.PLAIN, 24);
		MonospaceBold24 = new Font("Monospace", Font.BOLD, 24);
		Monospace10 = new Font("Monospace", Font.PLAIN, 10);
        Monospace9 = new Font("Monospace", Font.PLAIN, 9);
		
		
		JukeBox.load("/SFX/player_pain.mp3", "player_pain");
		JukeBox.load("/SFX/playerjump.mp3", "playerjump");
		JukeBox.load("/SFX/playerlands.mp3", "playerlands");
		JukeBox.load("/SFX/sword2.mp3", "playerdash");
		JukeBox.load("/SFX/teleport.mp3", "magic");
        JukeBox.load("/SFX/playercharge.mp3", "wosch");
		JukeBox.load("/SFX/player_pain.mp3", "damage");
		JukeBox.load("/SFX/playerhit.mp3", "playerhit");
		JukeBox.load("/SFX/enemy1.mp3", "jaw");
		JukeBox.load("/SFX/reload.mp3", "reload");
		JukeBox.load("/SFX/machine_gun.mp3", "machine_gun");
		JukeBox.load("/SFX/steps.mp3", "steps");
        JukeBox.load("/SFX/victory.mp3", "victory");
        JukeBox.load("/SFX/newepos.mp3", "theme");
		
		
		
		
		try {


			head.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_head_6.png")));
			head.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_head_4.png")));
			head.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_head_9.png")));
            head.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_head_7.png")));
			head.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_head_8.png")));
            head.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_head_5.png")));


            top.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_top_7.png")));
			top.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_enemy_top_3.png")));
            top.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_top_new4.png")));


            bottom.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_bottom_5.png")));
			bottom.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_bottom_4.png")));
            bottom.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_bottom_new.png")));

			weapon.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_weapon_test_4.png")));
			weapon.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_weapon_test_5.png")));
			weapon.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_weapon_test_2.png")));
            weapon.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_weapon_test_6.png")));

            hat.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_hat_1.png")));
            hat.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_hat_2.png")));
            hat.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_hat_3.png")));
            hat.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_hat_4.png")));



			/*head_enemy.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_enemy_head_1.png")));
			head_enemy.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_enemy_head_2.png")));
			head_enemy.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_enemy_head_3.png")));

			top_enemy.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_enemy_top_2.png")));
            top_enemy.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_enemy_top_3.png")));
			top_enemy.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_enemy_top_4.png")));

            bottom_enemy.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_enemy_bottom_2.png")));
            bottom_enemy.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_enemy_bottom_3.png")));

            weapon_enemy.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_enemy_weapon_1.png")));
			weapon_enemy.add(ImageIO.read(utl.class.getResourceAsStream("/Sprites/Player/sprite_enemy_weapon_2.png")));*/

			image_other = ImageIO.read(utl.class.getResourceAsStream("/Sprites/Other/sprite_other.png"));
//			Title = ImageIO.read(utl.class.getResourceAsStream("/Sprites/Other/title2.png"));
			Wall = ImageIO.read(utl.class.getResourceAsStream("/Sprites/Other/wall.png"));
			
			
		
		} catch (IOException e) {
		}
	}
	
	
	
}
