package com.neet.events;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Random;

import com.neet.GameState.MenuState;
import com.neet.Main.GamePanel;

public class partikel {
	
	private int x;
	private int y;
	
	private int width;
	private int height;
	private Color color;
	
	private titelscreen TitelScreen;
	
	private Random random = new Random();
	
	public partikel(titelscreen TitelScreen){
		
		this.TitelScreen = TitelScreen;
		x = random.nextInt(GamePanel.WIDTH+50)-50;
		y = 0;
		width = random.nextInt(2);
		height = random.nextInt(4)+2;
		
		color = color.getHSBColor(0.6f+((float)random.nextInt(2)/10), 0.6f+((float)random.nextInt(3)/10), 0.3f+((float)random.nextInt(3)/10));
	}
	
	public void update(){
		x = x+random.nextInt(2);
		y = y+random.nextInt(6);
		
		if(x+width>GamePanel.WIDTH || y+height>GamePanel.HEIGHT){
			//TitelScreen.partikels.remove(this);
		}
		
	}
	public void draw(Graphics2D g){
		g.setColor(color);
		g.drawRect(x, y, width, height);
	}
}
