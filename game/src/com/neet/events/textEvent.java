package com.neet.events;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;

import com.neet.GameState.MenuState;

public class textEvent {
	
	private int x = 0;
	private int y = 0;
	
	private int width = 0;
	
	private int speed = 1;
	
	public int ID;
	
	private int deathTime = 0;
		
	private Color color;
	private Font font;
	private String text;
	
	public textEvent(Font font, Color color,String text, int ID, int speed, int deathTime){
		this.font = font;
		this.color = color;
		this.text = text;
		this.ID = ID;
		this.speed = speed;
		this.deathTime = deathTime;
		
		events.getTextEvents().add(this);
	}
	
	public void draw(Graphics2D g){
		if(width == 0){
			width = g.getFontMetrics(font).stringWidth(text);
		}
		
		g.setFont(font);
		
		// shadow 
		g.setColor(Color.BLACK);
		g.drawString(text, x, y+2);
		
		g.setColor(color);
		g.drawString(text, x, y);
	}
	
	public void setPosition(int x, int y){this.x = x; this.y = y;}
	public int getX(){return x;}
	public int getY(){return y;}
	public int getWidth(){return width;}
	
	public int getDT(){return deathTime;}
	public void setDT(int deathTime){this.deathTime = deathTime;}
	
	public int getSpeed(){return speed;}
	public void setSpeed(int speed){this.speed = speed;}
}
