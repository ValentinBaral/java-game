package com.neet.events;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

import com.neet.Main.GamePanel;

public class titelscreen {
	
	//public ArrayList<partikel> partikels = new ArrayList<partikel>();
	
	private Random random = new Random();
	
	private int count = 0;
	
	private Vector<Integer> x = new Vector<Integer>();
	private Vector<Integer> y = new Vector<Integer>();
	private Vector<Integer> width = new Vector<Integer>();
	private Vector<Integer> height = new Vector<Integer>();
	private Vector<Integer> effect = new Vector<Integer>();
	
	private Vector<Color> color = new Vector<Color>();
	
	private int ppf = 2;
	
	public void update(){
		
		ppf = random.nextInt(20);
		
		for(int i=0; i<ppf; i++){
			count++;
			
			effect.add(0);
			x.add(random.nextInt(GamePanel.WIDTH+50)-50);
			y.add(0);
			width.add(random.nextInt(2));
			height.add(random.nextInt(4)+2);
			
			color.add(Color.getHSBColor(0.6f+((float)random.nextInt(2)/10), 0.6f+((float)random.nextInt(3)/10), 0.3f+((float)random.nextInt(3)/10)));
		
		}
		
		for(int i=0; i<count; i++){
			updatePartikel(i);
		}
		
		/*for(int i=0; i<ppf; i++){
			partikel p = new partikel(this);
			partikels.add(p);
		}
		
		for(int i=0; i<partikels.size(); i++){
			partikels.get(i).update();
		}*/
		
	}
	
	public void draw(Graphics2D g){
		
		/*for(int i=0; i<partikels.size(); i++){
			partikels.get(i).draw(g);
		}*/
		
		for(int i=0; i<count; i++){
			drawPartikel(i, g);
		}
		
	}
	
	private void updatePartikel(int i){
		x.set(i, x.get(i)+random.nextInt(2));
		y.set(i, y.get(i)+random.nextInt(6));
		

		//x = x+random.nextInt(2);
		//y = y+random.nextInt(6);
		
		if(x.get(i)+width.get(i)>GamePanel.WIDTH || y.get(i)+height.get(i)>GamePanel.HEIGHT){
			//TitelScreen.partikels.remove(this);
			removeParticel(i);
		}
	}
	
	private void drawPartikel(int i, Graphics2D g){
		g.setColor(color.get(i));
		g.drawRect(x.get(i), y.get(i), width.get(i), height.get(i));
	}
	
	private void removeParticel(int i){
		x.remove(i);
		y.remove(i);
		width.remove(i);
		height.remove(i);
		color.remove(i);
		count--;
	}
	
}
