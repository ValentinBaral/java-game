package com.neet.events;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;

import com.neet.Audio.JukeBox;
import com.neet.Main.GamePanel;

public class events {
	
	private static boolean cleared = false;
	
	private static ArrayList<textEvent> textEvents = new ArrayList<textEvent>();
	private static ArrayList<textEvent> remove = new ArrayList<textEvent>();

	public static void init(){
		//JukeBox.load("/SFX/victory.mp3", "victory");
		//JukeBox.load("/SFX/explode.mp3", "text");
	}
	
	public static void cleared(int room){
		JukeBox.play("victory");
		cleared = true;
		
		textEvent t1 = new textEvent(utl.ArialBold28,Color.ORANGE,"ROOM "+(room+1), 0, 3, 150);
		t1.setPosition(0-t1.getWidth(), 100);
		
		textEvent t2 = new textEvent(utl.ArialBold36,Color.ORANGE,"CLEARED!", 1, 5, 90);
		t2.setPosition(0-400-t1.getWidth(), 135);
	}
	
	public static void update(){
		if(cleared){
			for(int i = 0; i < textEvents.size(); i++){
				if(textEvents.get(i).getX() < GamePanel.WIDTH/2-textEvents.get(i).getWidth()/2){
					textEvents.get(i).setPosition(textEvents.get(i).getX()+(1*textEvents.get(i).getSpeed()),textEvents.get(i).getY());
				}
				else{
					JukeBox.play("playerhit");
					remove(i);
				}
			}
		}
		for(int i = 0; i < remove.size(); i++){
			if(remove.get(i).getDT()>0){
				remove.get(i).setDT(remove.get(i).getDT()-1);
			}else{
				remove.remove(i);
			}
		}
	}
	
	public static void draw(Graphics2D g){
		for(int i = 0; i < textEvents.size(); i++){
			textEvents.get(i).draw(g);
		}
		for(int i = 0; i < remove.size(); i++){
			remove.get(i).draw(g);
		}
	}
	
	public static void remove(int i){
		remove.add(textEvents.get(i));
		textEvents.remove(i);
	}
	
	public static ArrayList<textEvent> getTextEvents(){return textEvents;}
}
