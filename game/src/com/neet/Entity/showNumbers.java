package com.neet.Entity;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

import com.neet.Main.GamePanel;
import com.neet.events.utl;
import com.neet.world.map;

public class showNumbers {
	
	private static Random random = new Random();
	
	private static int count = 0;
	
	private static Vector<Integer> x = new Vector<Integer>();
	private static Vector<Integer> y = new Vector<Integer>();
	private static Vector<Integer> direction = new Vector<Integer>();
	private static Vector<Integer> dropTime = new Vector<Integer>();
	
	private static Vector<Color> color = new Vector<Color>();
	private static Vector<String> text = new Vector<String>();
	private static Vector<Font> font = new Vector<Font>();
	
	public static void update(){
		
		for(int i=0; i<count; i++){
			updatePartikle(i);
		}

	}
	
	public static void draw(Graphics2D g){
		
		/*for(int i=0; i<partikels.size(); i++){
			partikels.get(i).draw(g);
		}*/
		
		for(int i=0; i<count; i++){
			drawNumber(i, g);
		}
		
	}
	
	private static void updatePartikle(int i){
		
		
		y.set(i, y.get(i)-random.nextInt(3));

		
		if(dropTime.get(i)==0){
			removeParticel(i);
			return;
		}
		
		if(dropTime.get(i)>0){
			dropTime.set(i, dropTime.get(i)-1);
		}
		
		if(x.get(i)+map.getx()>GamePanel.WIDTH || y.get(i)+map.gety()>GamePanel.HEIGHT){
			removeParticel(i);
		}
	}
	
	private static void drawNumber(int i, Graphics2D g){
		
		int newX = (int)(x.get(i)+map.getx());
		int newY = (int)(y.get(i)+map.gety());
		
		g.setColor(color.get(i));
		g.setFont(font.get(i));
		g.drawString(text.get(i),newX, newY);
	}
	
	private static void removeParticel(int i){
		x.remove(i);
		y.remove(i);
		color.remove(i);
		text.remove(i);
		font.remove(i);
		dropTime.remove(i);
		direction.remove(i);
		count--;
	}
	
	public static void addNumber(int targetX, int targetY, String num, int time, Color theColor, Font thefont ){
		
		
			
		count++;
		x.add(targetX+random.nextInt(5)-2);
		y.add(targetY+random.nextInt(5)-2);
		dropTime.add(time);
		direction.add(random.nextInt(2));
		color.add(theColor);
		text.add(num);
		font.add(thefont);
		
	}
	
}
