package com.neet.Entity;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Random;
import java.util.Vector;

import com.neet.Main.GamePanel;
import com.neet.world.map;

public class gfx {
	
	private static Random random = new Random();
	
	private static int count = 0;
	
	private static Vector<Integer> x = new Vector<Integer>();
	private static Vector<Integer> y = new Vector<Integer>();
	private static Vector<Integer> width = new Vector<Integer>();
	private static Vector<Integer> height = new Vector<Integer>();
	private static Vector<Integer> effect = new Vector<Integer>();
	private static Vector<Integer> direction = new Vector<Integer>();
	private static Vector<Integer> dropTime = new Vector<Integer>();
	private static Vector<Integer> layer = new Vector<Integer>();
	private static Vector<Boolean> staticobj = new Vector<Boolean>();
	
	private static Vector<Color> color = new Vector<Color>();
	
	public static void update(){

		for(int i=count-1; i >= 0; i--) {
			updatePartikle(i);
		}

		//showNumbers.update();

	}
	
	public static void draw(Graphics2D g, int draw_layer){

		for(int i=0; i<count; i++){
			if(layer.get(i) == draw_layer) {
				drawPartikle(i, g);

				//System.out.println("X "+x.get(i)+" Y "+y.get(i));
			}
		}
		
		//showNumbers.draw(g);
		
	}
	
	private static void updatePartikle(int i){

		if(dropTime.get(i)==0){
			removeParticel(i);
			return;
		}

		if(dropTime.get(i)>0){
			dropTime.set(i, dropTime.get(i)-1);
		}
		
		if(effect.get(i)==0){
			x.set(i, x.get(i)+random.nextInt(2));
			y.set(i, y.get(i)+random.nextInt(6));

		}
		
		if(effect.get(i)==1){
			y.set(i, y.get(i)+random.nextInt(3));
			if(direction.get(i)==1){
				x.set(i, x.get(i)+random.nextInt(2));
			}
			if(direction.get(i)==0){
				x.set(i, x.get(i) - random.nextInt(2));
			}

		}

		if(effect.get(i)==2){
			if(direction.get(i)==1){
				y.set(i, y.get(i)+random.nextInt(2));
			}
			if(direction.get(i)==0){
				y.set(i, y.get(i)-random.nextInt(2));
			}

			if(x.get(i)+width.get(i)+map.getx()>GamePanel.WIDTH || y.get(i)+height.get(i)+map.gety()>GamePanel.HEIGHT){
				removeParticel(i);
			}

		}

		if(effect.get(i)==3){
			x.set(i, x.get(i)+random.nextInt(3));
		}

		if(effect.get(i)==4){
			y.set(i, y.get(i)+random.nextInt(2));
		}

		if(dropTime.get(i)==0){
			removeParticel(i);
			return;
		}

		if(staticobj.get(i)) {
			if (x.get(i) > GamePanel.WIDTH || y.get(i) + height.get(i) > GamePanel.HEIGHT) {
				removeParticel(i);
			}
		}else {
			if (x.get(i) + width.get(i) + map.getx() > GamePanel.WIDTH || y.get(i) + height.get(i) + map.gety() > GamePanel.HEIGHT) {
				removeParticel(i);
			}
		}



	}

	private static void drawPartikle(int i, Graphics2D g){

		int newX;
		int newY;

		if(staticobj.get(i)){
			newX = (int)(x.get(i));
			newY = (int)(y.get(i));
		}else{
			newX = (int)(x.get(i)+map.getx());
			newY = (int)(y.get(i)+map.gety());
		}

		g.setColor(color.get(i));
		g.fillRect(newX, newY, width.get(i), height.get(i));
	}
	
	private static void removeParticel(int i){
		effect.remove(i);
		x.remove(i);
		y.remove(i);
		width.remove(i);
		height.remove(i);
		color.remove(i);
		dropTime.remove(i);
		direction.remove(i);
		layer.remove(i);
		staticobj.remove(i);
		count--;
	}

	public static void clean(){
		effect.clear();
		x.clear();
		y.clear();
		width.clear();
		height.clear();
		color.clear();
		dropTime.clear();
		direction.clear();
		layer.clear();
		staticobj.clear();
		count = 0;
	}
	
	public static void addParticle(int targetX, int targetY, int targetWidth, int targetHeight, int gfx, int amount, int time, Color theColor, int targetLayer, boolean isStatic){

		for(int i=0; i<amount; i++){

			count++;
			effect.add(gfx);

			if(gfx == 3) {
				x.add(targetX + random.nextInt(40) - 20);
				y.add(targetY + random.nextInt(40) - 20);
				//color.add(Color.getHSBColor(0.8f - ((float)random.nextInt(10)/50),0.5f + ((float)random.nextInt(20)/50),0.1f + ((float)random.nextInt(20)/50)));
				color.add(Color.getHSBColor(0f,0f,0.1f + ((float)random.nextInt(20)/50)));

			}else{
				x.add(targetX + random.nextInt(8) - 4);
				y.add(targetY + random.nextInt(8) - 4);
				color.add(theColor);
			}
			width.add(targetWidth+random.nextInt(3)-1);
			height.add(targetHeight+random.nextInt(5)-2);
			dropTime.add(time+random.nextInt(20)-10);
			direction.add(random.nextInt(2));
			layer.add(targetLayer);
			staticobj.add(isStatic);


		}
		
	}
	
}
