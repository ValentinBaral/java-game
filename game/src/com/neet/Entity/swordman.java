package com.neet.Entity;

public class swordman extends enemy {

	public swordman(int room) {
		super(room);
		
		IAM = IAM_ENEMY;
		
		width = 12;
		height = 12;
		
		ATK = 10;
		
		HP = 600;
		maxHP = HP;

        head = 0;
        top = 0;
        bottom = 0;
        //weapon = 0;
		
		jumpStrength = 15;
		
		weaponRadius = 10;
		
		//sound_atk = "machine_gun";


		animFrames = new int[]{4,4,2,3,3,1,4,4,4};
		animRepeat = new int[]{1,1,1,1,1,1,1,1,1};

		animScriptHead_X = new int[][]{{0,0,1,1},{0,0,1,1},{0},{0},{0},{0},{0},{0,0,0,0,0,0,0},{0}};
		animScriptHead_Y = new int[][]{{0,0,0,0},{0,0,0,0},{0},{0},{0},{0},{0},{-4,-8,-12,-16,-12,-4,10},{0}};
		animHeadFrames = new int[]{4,4,0,0,0,0,0,7,0};
		animHeadHasFrames = new int[]{0,0,0,0,0,0,0,0,0};

		animScriptTop_X = new int[][]{{0},{0,0,0,0},{0},{0},{0},{0},{0},{0,0},{0}};
		animScriptTop_Y = new int[][]{{0},{0,-1,-1,0},{0},{0},{0},{0},{0},{4,8},{0}};
		animTopFrames = new int[]{0,4,0,4,0,0,0,2,0};
		animTopHasFrames = new int[]{0,0,0,1,0,0,0,0,0};

		animScriptBottom_X = new int[][]{{0},{0,0,0,0},{0},{0},{0},{0},{0},{0},{0}};
		animScriptBottom_Y = new int[][]{{0},{0,0,0,0},{0},{0},{0},{0},{0},{0},{0}};
		animBottomFrames = new int[]{0,4,0,0,1,0,0,0,0};
		animBottomHasFrames = new int[]{0,1,0,0,1,0,0,0,0};

		animScriptWeapon_X = new int[][]{{0},{0,0,0,0},{4,4,4},{0,1,2,2},{0},{0},{0},{0,0},{0}};
		animScriptWeapon_Y = new int[][]{{0},{0,0,-1,-1},{0},{-1,-1,-2,-2},{0},{0},{0},{0,0},{0}};
		animWeaponFrames = new int[]{0,4,1,4,0,0,0,2,0};
		animWeaponHasFrames = new int[]{0,0,0,1,0,0,0,0,0};

	}

}
