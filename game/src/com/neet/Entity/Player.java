package com.neet.Entity;

import java.awt.Color;
import java.awt.Graphics2D;

import com.neet.Audio.JukeBox;
import com.neet.Main.config;
import com.neet.events.utl;

public class Player extends smthliving {
	
	private int delayCount = 0;
	private int delay = 18;
	
	public Player(int level) {
		super(level);
		
		worldGenerator.setActiveLevel(level); 
		
		IAM = IAM_PLAYER;
		
		width = 12;
		height = 12;
		
		if(config.difficulty == 0){
			HP = 2000;
		}
		if(config.difficulty == 1){
			HP = 1000;
		}
		if(config.difficulty == 2){
			HP = 500;
		}

        top = 2;
        head = 5;
        bottom = 2;

        maxHP = HP;

		ATK = 10;

        Inventory.add(itemDealer.request(0,LVL,-1));
        //Inventory.add(itemDealer.request(2,LVL,-1));
        Inventory.add(itemDealer.request(1,LVL,-1));




        weaponRadius = 15;
		
		animFrames = new int[]{4,4,2,3,3,1,4,4,4};
		animRepeat = new int[]{1,1,1,1,1,1,1,1,1};
		
		animScriptHead_X = new int[][]{{0,0,1,1},{0,0,1,1},{0},{0},{0},{0},{0},{0,0,0,0,0,0,0},{0}};
		animScriptHead_Y = new int[][]{{0,0,0,0},{0,0,0,0},{0},{0},{0},{0},{0},{-4,-8,-12,-16,-12,-4,4},{0}};
		animHeadFrames = new int[]{4,4,0,0,0,0,0,7,0};
		animHeadHasFrames = new int[]{0,0,0,0,0,0,0,0,0};
		animHeadRow = new int[]{0,0,0,0,0,0,0,0,0};


		animScriptTop_X = new int[][]{{0},{0,0,0,0},{0},{0},{0},{0},{0},{0,0},{0}};
		animScriptTop_Y = new int[][]{{0},{0,-1,-1,0},{0},{0},{0},{0},{0},{2,4},{0}};
		animTopFrames = new int[]{0,4,0,4,0,0,0,2,0};
		animTopHasFrames = new int[]{0,0,0,1,0,0,0,0,0};
		animTopRow = new int[]{0,0,0,3,0,0,0,0,0};
		
		animScriptBottom_X = new int[][]{{0},{0,0,0,0},{0},{0},{0},{0},{0},{0},{0}};
		animScriptBottom_Y = new int[][]{{0},{0,0,0,0},{0},{0},{0},{0},{0},{0},{0}};
		animBottomFrames = new int[]{0,4,0,0,1,0,0,0,0};
		animBottomHasFrames = new int[]{0,1,0,0,1,0,0,0,0};
		animBottomRow = new int[]{0,1,0,0,4,0,0,0,0};
		
		animScriptWeapon_X = new int[][]{{0},{0,0,0,0},{0,0,0},{4,6,8,8},{0},{0},{0},{0,0},{0}};
		animScriptWeapon_Y = new int[][]{{0},{0,0,-1,-1},{0},{-2,-2,0,2},{0},{0},{0},{0,0},{0}};
		animWeaponFrames = new int[]{0,4,1,3,0,0,0,2,0};
		animWeaponHasFrames = new int[]{0,0,0,1,0,0,0,0,0};
		animWeaponRow = new int[]{0,0,0,1,0,0,0,0,0};
		
		preferredTarget = IAM_ENEMY;
		
		/*try{
			//image = ImageIO.read(getClass().getResourceAsStream("/Sprites/Player/sprite_jet.png"));
			
			head = ImageIO.read(getClass().getResourceAsStream("/Sprites/Player/sprite_head.png"));
			top = ImageIO.read(getClass().getResourceAsStream("/Sprites/Player/sprite_body_1.png"));
			bottom = ImageIO.read(getClass().getResourceAsStream("/Sprites/Player/sprite_bottom_1.png"));
			weapon = ImageIO.read(getClass().getResourceAsStream("/Sprites/Player/sprite_weapon_1.png"));
			
		}catch(Exception e) {
		}*/
		
	}
	
	public void update(){
		
		//System.out.println("activeRoom "+activeRoom);
		
		//System.out.println(activeLevel);
		
		collision.interactionRadius(this);
		
		if(walking){
			if(delayCount<delay){
				delayCount++;
			}else{
				delayCount = 0;
				footsteps();
			}
		}

        //System.out.println(activeLevel+" / "+x+" / "+y);
		
		super.update();
	}
	
	public void draw(Graphics2D g) {
		
		g.setColor(Color.MAGENTA);
		
		//HITBOX
		//g.fillRect((int)x+(int)map.getx(), (int)y+(int)map.gety(), width, height);

		super.draw(g);
	}
	
	public void getAttacked(smthliving theEnemy, int damage){
		if(HP>0){
			HP = HP-damage;
			JukeBox.play("player_pain");
		}
		if(HP<=0){
			//worldGenerator.getRoom(activeRoom).getList().remove(this);
			//living = false;
			
			die();

		}
		
		gfx.addParticle((int)x, (int)y, 3, 3, 1, 20, 20, Color.ORANGE, 0,false);
		showNumbers.addNumber((int) x, (int) y, String.valueOf(damage), 35, Color.WHITE, utl.Arial10);

		
		
		pushBack(theEnemy.getFLeft());
	}
	
	public void footsteps(){
		JukeBox.play("playerlands");
	}

}
