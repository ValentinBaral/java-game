package com.neet.Entity;

import com.neet.Audio.JukeBox;
import com.neet.GameState.running;
import com.neet.Handlers.Keys;
import com.neet.Main.GamePanel;
import com.neet.events.utl;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Valentin on 11/03/2015.
 */
public class overlay {

    private static boolean active = true;
    private static boolean closing = false;
    private static int currentChoice = 0;

    private static int line = 10;

    private static int slideAnim = 0;

    private static int slideAnimMax = 30;

    public static boolean layer_INVENTORY = false;
    public static boolean layer_INGAME = true;

    private static smthliving player = running.player;

    private static inventory Inventory = player.getInventory();

    private static int[] body_sprites = {
            Inventory.getNumItemsForType(0),
            Inventory.getNumItemsForType(1)
    };

    //private static item currentHead;
    private static item currentHat;
    private static item currentWeapon;
    private static item currentItem;

    private static item[] current_items = {
            currentHat,
            currentWeapon
    };

    private static int[] target_sprites = {
            0,
            0
    };

    public static void update(){

        if(active){

            if(layer_INGAME) {

            }

            if(layer_INVENTORY){

                if (!closing) {
                    if (slideAnim > 0) slideAnim--;
                } else {
                    if (slideAnim < slideAnimMax) {
                        slideAnim++;
                    } else {
                        layer_INVENTORY = false;
                        closing = false;
                    }
                }

                //int tempHat = Inventory.getNthOfType(target_sprites[0],0);


                int tempItem = Inventory.getNthOfType(target_sprites[currentChoice],currentChoice);

                if(tempItem!=-1) {
                    current_items[currentChoice] = Inventory.getItem(tempItem);
                }
               /* if(tempHat != -1) {
                    currentHat = Inventory.getItem(tempHat);
                }
                //currentTop = Inventory.getItem(Inventory.getNthOfType(target_sprites[1],1));
                if(tempWeapon != -1) {
                    currentWeapon = Inventory.getItem(tempWeapon);
                }*/


                //player.setHead(currentHead.getSPRITE());
                //player.setTop(currentTop.getSPRITE());
                //player.setBottom(target_sprites[2]);
                //player.setWeapon(currentWeapon.getSPRITE());

                currentItem = current_items[currentChoice];
                /*if(currentChoice == 0){
                    currentItem = currentHat;
                }
                if(currentChoice == 1){
                    currentItem = currentWeapon;
                }*/
               /* if(currentChoice == 2){
                    currentItem = currentWeapon;
                }*/



            }

        }

    }

    public static void draw(Graphics2D g){

        if(active){

            if(layer_INGAME) {

                // #INTERFACE#
                g.setFont(utl.Arial10);
                String SPEED = String.valueOf((float)player.getSpeed());
                String FPS = String.valueOf(GamePanel.FPS);
                String UPDATES = String.valueOf(GamePanel.UPDATES);
                String HEALTH = String.valueOf(player.getHealth());
                String LEVEL = String.valueOf(player.getLVL());
                String EXP = String.valueOf(player.getExp());
                String EXPneed = String.valueOf(player.getExpNeed());
                //String thehealth = String.valueOf(player.getHealth())+"HP";

                /*
                // UPDATES
                g.setColor(Color.BLACK);
                g.drawString("UPDATES: "+UPDATES, 10, 21);
                g.setColor(Color.WHITE);
                g.drawString("UPDATES: "+UPDATES, 10, 20);

                // FPS
                g.setColor(Color.BLACK);
                g.drawString("FPS: "+FPS, 10, 31);
                g.setColor(Color.WHITE);
                g.drawString("FPS: "+FPS, 10, 30);*/

                drawFont("UPDATES: "+UPDATES, GamePanel.WIDTH-80, 20, Color.WHITE, utl.Arial10, 0, 1, 3, g);
                drawFont("FPS: "+FPS, GamePanel.WIDTH-80, 30, Color.WHITE, utl.Arial10, 0, 1, 3, g);



                FontMetrics f24 = g.getFontMetrics(utl.MonospaceBold24);

                /*g.setFont(utl.Monospace10);
                g.setColor(Color.WHITE);
                g.drawString("LEVEL " + LEVEL, 10, GamePanel.HEIGHT - 55);

                g.drawString("EXP " + EXP +"/"+ EXPneed, 10, GamePanel.HEIGHT - 45);

                g.setFont(utl.MonospaceBold24);

                g.setColor(Color.WHITE);
                g.drawString(HEALTH+" health", 10, GamePanel.HEIGHT-15);*/

                drawFont("LVL " + LEVEL, 35, GamePanel.HEIGHT-30, Color.ORANGE, utl.Impact18, 0, 1, 3, g);
                drawFont("EXP " + EXP +"/"+ EXPneed, 35, GamePanel.HEIGHT-15, Color.WHITE, utl.Arial10, 0, 1, 3, g);


                if (Keys.keyState[Keys.ESCAPE]) {
                    drawButton("ESC", 15, 10, Color.DARK_GRAY, g);
                }else{
                    drawButton("ESC", 15, 10, Color.GRAY, g);
                }
                drawFont("return to base", 55, 20, Color.WHITE, utl.Arial10, 0, 1, 0, g);


                if (Keys.keyState[Keys.BUTTON4]) {
                    drawButton("f", 15, 30, Color.DARK_GRAY, g);
                }else{
                    drawButton("f", 15, 30, Color.GRAY, g);
                }
                drawFont("open inventory", 35, 40, Color.WHITE, utl.Arial10, 0, 1, 0, g);


                if (Keys.keyState[Keys.BUTTON1]) {
                    drawButton("A", 15, GamePanel.HEIGHT-45, Color.DARK_GRAY, g);
                }else{
                    drawButton("A", 15, GamePanel.HEIGHT-45, Color.BLUE, g);
                }
                drawFont("jump!", 35, GamePanel.HEIGHT-35, Color.WHITE, utl.Arial10, 0, 1, 0, g);

                if (Keys.keyState[Keys.BUTTON2]) {
                    drawButton("S", 15, GamePanel.HEIGHT-25, Color.DARK_GRAY, g);
                }else{
                    drawButton("S", 15, GamePanel.HEIGHT-25, Color.RED, g);
                }
                drawFont("attack for: "+String.valueOf(player.getDamage()), 35, GamePanel.HEIGHT-15, Color.WHITE, utl.Arial10, 0, 1, 0, g);

                g.setColor(Color.RED);
                g.fillRect(GamePanel.WIDTH/2-15,GamePanel.HEIGHT-40,30,30);

                g.setColor(Color.GRAY);
                g.fillRect(GamePanel.WIDTH/2-15,GamePanel.HEIGHT-40,30,30-((int)(((double)player.HP/(double)player.maxHP)*(double)30)));

                drawFont(String.valueOf(player.HP)+" / "+String.valueOf(player.maxHP), 0,GamePanel.HEIGHT-20, Color.WHITE, utl.Arial10, 0, 1,2, g);



                //g.setColor(Color.WHITE);
                //g.drawString("100"+" armor", GamePanel.WIDTH-f24.stringWidth("100"+" armor")-10, GamePanel.HEIGHT-15);

                //g.setColor(Color.YELLOW);


            }

            if(layer_INVENTORY) {

                int mod = slideAnim * 3;

                g.setColor(Color.black);

                int x[] = {-GamePanel.HEIGHT - mod, 0 - mod, GamePanel.HEIGHT - mod, 0 - mod};
                int y[] = {GamePanel.HEIGHT + mod, GamePanel.HEIGHT * 2 + mod, GamePanel.HEIGHT + mod, 0 + mod};
                g.fillPolygon(x, y, 4);

                int x2[] = {GamePanel.WIDTH - GamePanel.HEIGHT + mod, GamePanel.WIDTH + mod, GamePanel.WIDTH + GamePanel.HEIGHT + mod, GamePanel.WIDTH + mod};
                int y2[] = {GamePanel.HEIGHT + mod, GamePanel.HEIGHT * 2 + mod, GamePanel.HEIGHT + mod, 0 + mod};
                g.fillPolygon(x2, y2, 4);

                int x3[] = {-GamePanel.HEIGHT - mod, 0 - mod, GamePanel.HEIGHT - mod, 0 - mod};
                int y3[] = {0 - mod, GamePanel.HEIGHT - mod, 0 - mod, -GamePanel.HEIGHT - mod};
                g.fillPolygon(x3, y3, 4);

                int x4[] = {GamePanel.WIDTH - GamePanel.HEIGHT + mod, GamePanel.WIDTH + mod, GamePanel.WIDTH + GamePanel.HEIGHT + mod, GamePanel.WIDTH + mod};
                int y4[] = {0 - mod, GamePanel.HEIGHT - mod, 0 - mod, -GamePanel.HEIGHT - mod};
                g.fillPolygon(x4, y4, 4);

                if (slideAnim < 1) {

                    drawFont("Inventory", GamePanel.WIDTH / 2 -20, GamePanel.HEIGHT / 2 - 60, Color.WHITE, utl.Arial14, 0, 1, 0, g);

                    for (int i = 0; i < body_sprites.length; i++) {
                        if (currentChoice == i) {
                            drawFont(String.valueOf((target_sprites[i] + 1) + " of " + body_sprites[i]), GamePanel.WIDTH / 2 + 100, GamePanel.HEIGHT / 2 - 10 + i * line, Color.GRAY, utl.Monospace10, 0, 1, 0, g);
                            drawFont("->", GamePanel.WIDTH / 2 + 80, GamePanel.HEIGHT / 2 - 10 + i * line, Color.GRAY, utl.Monospace10, 0, 1, 0, g);

                        } else {
                            drawFont(String.valueOf((target_sprites[i] + 1) + " of " + body_sprites[i]), GamePanel.WIDTH / 2 + 100, GamePanel.HEIGHT / 2 - 10 + i * line, Color.WHITE, utl.Monospace10, 0, 1, 0, g);
                        }
                    }
                    if(currentItem != null) {
                        drawFont("ATK", 10, GamePanel.HEIGHT / 2 - 10, Color.WHITE, utl.Monospace10, 0, 1, 0, g);
                        drawFont("DEF", 10, (GamePanel.HEIGHT / 2 - 10) +line*1, Color.WHITE, utl.Monospace10, 0, 1, 0, g);
                        drawFont("WEIGHT", 10, (GamePanel.HEIGHT / 2 - 10) +line*2, Color.WHITE, utl.Monospace10, 0, 1, 0, g);
                        drawFont("MAGI", 10, (GamePanel.HEIGHT / 2 - 10) +line*3, Color.WHITE, utl.Monospace10, 0, 1, 0, g);

                        Color rarecolor = Color.RED;

                        if(currentItem.getRARITY() == 1){
                            rarecolor = Color.WHITE;
                        }
                        else if(currentItem.getRARITY() == 2){
                            rarecolor = Color.GREEN;
                        }
                        else if(currentItem.getRARITY() == 3){
                            rarecolor = Color.MAGENTA;
                        }
                        else if(currentItem.getRARITY() == 4){
                            rarecolor = Color.ORANGE;
                        }

                        drawFont(String.valueOf(currentItem.getNAME()), 60, (GamePanel.HEIGHT / 2 - 10) - line * 1, rarecolor, utl.Monospace10, 0, 1, 0, g);
                        drawFont(String.valueOf(currentItem.getATK()), 60, GamePanel.HEIGHT / 2 - 10, Color.WHITE, utl.Monospace10, 0, 1, 0, g);
                        drawFont(String.valueOf(currentItem.getDEF()), 60, (GamePanel.HEIGHT / 2 - 10) + line * 1, Color.WHITE, utl.Monospace10, 0, 1, 0, g);
                        drawFont(String.valueOf(currentItem.getWEIGHT()), 60, (GamePanel.HEIGHT / 2 - 10) + line * 2, Color.WHITE, utl.Monospace10, 0, 1, 0, g);
                        drawFont(String.valueOf(currentItem.getMAGI()), 60, (GamePanel.HEIGHT / 2 - 10) + line * 3, Color.WHITE, utl.Monospace10, 0, 1, 0, g);
                    }


                    drawFont("press [f] to close", 30, GamePanel.HEIGHT - 20, Color.WHITE, utl.Monospace10, 0, 1, 0, g);


                }

            }

        }

    }

    public static void drawButton(String onButton, int x, int y, Color color, Graphics2D g){
        FontMetrics f = g.getFontMetrics(utl.Arial10);

        g.setFont(utl.Arial10);


        //shadow
        g.setColor(Color.BLACK);
        g.drawRoundRect(x,y+1,f.stringWidth(onButton)+6,12,4,4);
        g.fillRoundRect(x,y+1,f.stringWidth(onButton)+6,12,4,4);

        g.setColor(color);
        g.drawRoundRect(x,y,f.stringWidth(onButton)+6,12,4,4);
        g.fillRoundRect(x,y,f.stringWidth(onButton)+6,12,4,4);

        g.setColor(Color.WHITE);

        g.drawString(onButton,x+3, y+9);

    }

    public static void showInventory(smthliving target){

        Inventory = target.getInventory();

        /*target_sprites[0] = target.getHead();
        target_sprites[1] = target.getTop();
        //target_sprites[2] = target.getBottom();
        target_sprites[2] = target.getWeapon();*/

        target_sprites[0] = Inventory.getNthEquippedForType(0);
        target_sprites[1] = Inventory.getNthEquippedForType(1);
        //target_sprites[2] = Inventory.getNthEquippedForType(2);

        body_sprites[0] = Inventory.getNumItemsForType(0);
        body_sprites[1] = Inventory.getNumItemsForType(1);
        //body_sprites[2] = Inventory.getNumItemsForType(2);

        player = target;

        slideAnim = slideAnimMax;

        layer_INVENTORY = true;

    }

    public static void hideInventory(){
        closing = true;
    }

    public static boolean getActive(){
        return active;
    }

    public static void handleInput(){

        /*public int getHead() {return head;}
        public void setHead(int head) {this.head = head;}

        public int getTop() {return top;}
        public void setTop(int top) {this.top = top;}

        public int getBottom() {return bottom;}
        public void setBottom(int bottom) {this.bottom = bottom;}

        public int getWeapon() {return weapon;}
        public void setWeapon(int weapon) {this.weapon = weapon;}*/

        if(!closing) {

            if (Keys.isPressed(Keys.LEFT)) {
                if (target_sprites[currentChoice] >= 0) {

                    target_sprites[currentChoice] = target_sprites[currentChoice] - 1;

                    if(target_sprites[currentChoice] < 0){
                        Inventory.unequip(currentChoice);
                        current_items[currentChoice] = null;
                    }else{
                        Inventory.equip(Inventory.getNthOfType(target_sprites[currentChoice],currentChoice));
                    }

                }
            }

            if (Keys.isPressed(Keys.RIGHT)) {
                if (target_sprites[currentChoice] + 1 < body_sprites[currentChoice]) {
                    System.out.println("11111" + target_sprites[currentChoice]);
                    System.out.println("22222" + body_sprites[currentChoice]);
                    target_sprites[currentChoice] = target_sprites[currentChoice] + 1;
                    Inventory.equip(Inventory.getNthOfType(target_sprites[currentChoice],currentChoice));

                }
            }

            if (Keys.isPressed(Keys.UP)) {
                if (currentChoice > 0) {
                    JukeBox.play("menu", 0);
                    currentChoice--;
                }
            }

            if (Keys.isPressed(Keys.DOWN)) {
                if (currentChoice < body_sprites.length - 1) {
                    JukeBox.play("menu", 0);
                    currentChoice++;
                    System.out.println("11111 - " + target_sprites[currentChoice]);
                    System.out.println("22222 - " + body_sprites[currentChoice]);

                }
            }

            if (Keys.isPressed(Keys.BUTTON4)) {
                player.handleOverlay();
            }

        }

    }

    private static void drawFont(String string,int x,int y,Color color,Font font,int offsetX, int offsetY, int xpos, Graphics2D g){
        g.setFont(font);

        int drawx = x;

        if(xpos == 1){

            drawx = 15;
        }

        if(xpos == 2){
            FontMetrics f = g.getFontMetrics(font);

            drawx = GamePanel.WIDTH/2-f.stringWidth(string)/2;
        }

        if(xpos == 3){
            FontMetrics f = g.getFontMetrics(font);

            drawx = (GamePanel.WIDTH-f.stringWidth(string))-15;
        }

        g.setColor(Color.black);
        g.drawString(string, drawx+offsetX, y+offsetY);

        g.setColor(color);
        g.drawString(string, drawx, y);
    }

}
