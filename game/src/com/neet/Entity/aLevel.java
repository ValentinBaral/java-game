package com.neet.Entity;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

public class aLevel {
	
	private int roomX = 1000;
	private int roomY = 1000;
	
	private int x;
	private int y;
	
	private int from = 0;
	private int to = 0;

	private int num;

	private Color color;

	
	private ArrayList<MapObject> objects = new ArrayList<MapObject>();
	
	public aLevel(int posX,int posY,int num, String type){

        pool.clear();
		background.clear();
        static_background.clear();
		
		System.out.println("speedtest in "+System.nanoTime()/1000000);
		
		this.num = num;

		//System.out.println((int)(((double)num/worldGenerator.getLength())*100)+"% Loaded");
		System.out.println("loading ..");
		
		Random magicNum = worldGenerator.getRandom();

		System.out.println("room: "+num);
		worldGenerator.getLevels().add(this);

        pool.fill(20, num);
		
		//int[][] grid = worldGenerator.getGrid();
		
		int otherNum = num+1;

		if(type == "create"){

		}
		
		if(type == "Base"){
			
			roomX = 700;

			decoration textl1 = new decoration(num);
			textl1.setPosition(x+230,y+roomY-280);
			textl1.setText("Controls");
			textl1.setDimension(200,200);

			decoration textl2 = new decoration(num);
			textl2.setPosition(x+230,y+roomY-260);
			textl2.setText("<- = left  -> = right");
			textl2.setDimension(200,200);

			decoration textl3 = new decoration(num);
			textl3.setPosition(x+230,y+roomY-240);
			textl3.setText("z = jump  x = dash  f = inventory");
			textl3.setDimension(200,200);
			
			level ground1 = new level(num);
			ground1.setPosition(x,y);
			ground1.setDimension(roomX,200);
			
			level ground2 = new level(num);
			ground2.setPosition(x,y+roomY-200);
			ground2.setDimension(roomX,200);
			
			level ground3 = new level(num);
			ground3.setPosition(x,y+200);
			ground3.setDimension(200,roomY-400);
			
			level ground4 = new level(num);
			ground4.setPosition(x+roomX-200,y+200);
			ground4.setDimension(200,roomY-400);
			
			/*decoration dec1 = new decoration(num);
			dec1.setPosition(x+roomX/2-30, y+roomY-200-43);
			dec1.setImg(0);
			dec1.setInteraction(true, "NextLevel");*/
			
			level door = new level(num);
			door.setPosition(x+roomX-225,y+roomY-200-50);
			door.setDimension(25,50);
			door.setColor(Color.ORANGE);
			door.setNoColl(true);
			door.setInteraction(true, "NextLevel");
			
			
			npc guy = new npc(num);
			//guy.setNoColl(true);
			guy.setPosition(x+400, y+roomY-200-guy.getheight());
			guy.setDescription("master ninja");
			guy.setName("Jin");
			guy.setInteraction(true, "Dialog");
			guy.addDialoge("Dear jung ninja.");
			guy.addDialoge("There is a danger in this world!");
			guy.addDialoge("You are our only hope!!");
			guy.addDialoge("Good luck!!!");
			
			/* TEST */
			
			
			//npc guy2 = new npc(num);
			//guy2.setPosition(x+460, y+roomY-200-guy.getheight());
			//guy2.setInteraction(true, "Dialog");
			//guy2.addDialoge("meh!");
			//guy2.addDialoge("mehhhh.");
			
			//enemy bady = new enemy(num);
			//bady.setPosition(x+630, y+roomY-200-bady.getheight());
			
			
			// find closest target
			
			
			
			
			
			
			//background.makeBG(2000,2000,roomX,(int)(roomX/130),0.5f);
			
		}


		if(type == "Limbo"){

			roomX = 1100;

			/*level ground1 = new level(num);
			ground1.setPosition(x,y);
			ground1.setDimension(roomX,200);*/

			level ground2 = new level(num);
			ground2.setPosition(x,y+roomY-200);
			ground2.setDimension(roomX,20);

			/*level ground3 = new level(num);
			ground3.setPosition(x,y+200);
			ground3.setDimension(200,roomY-400);

			level ground4 = new level(num);
			ground4.setPosition(x+roomX-200,y+200);
			ground4.setDimension(200,roomY-400);*/

			level door = new level(num);
			door.setPosition(x+600,y+roomY-200-50);
			door.setDimension(25,50);
			door.setColor(Color.ORANGE);
			door.setNoColl(true);
			door.setInteraction(true, "NextLevel");
			door.setImg(2);
            door.forRealm(1);

			level door2 = new level(num);
			door2.setPosition(x + 680, y + roomY - 200 - 50);
			door2.setDimension(25, 50);
			door2.setColor(Color.ORANGE);
			door2.setNoColl(true);
			door2.setInteraction(true, "NextLevel");
			door2.setImg(2);
            door2.forRealm(2);

			level door3 = new level(num);
			door3.setPosition(x + 760, y + roomY - 200 - 50);
			door3.setDimension(25, 50);
			door3.setColor(Color.ORANGE);
			door3.setNoColl(true);
			door3.setInteraction(true, "NextLevel");
			door3.setImg(2);
            door3.forRealm(3);

			level door4 = new level(num);
			door4.setPosition(x + 840, y + roomY - 200 - 50);
			door4.setDimension(25, 50);
			door4.setColor(Color.ORANGE);
			door4.setNoColl(true);
			door4.setInteraction(true, "NextLevel");
			door4.setImg(2);
            door4.forRealm(4);


			/*npc guy = new npc(num);
			guy.setHeight(24);
			guy.setFLeft(true);
			guy.setFRight(false);
			//guy.setNoColl(true);
			guy.setPosition(x + 400, y + roomY - 200 - guy.getheight());
			guy.setSprites(2, 2, 1);
			guy.setDescription("master ninja");
			guy.setName("Jin");
			guy.setInteraction(true, "Dialog");
			guy.addDialoge("AYEE .. adventurer");
			guy.addDialoge("I created these 4 REALMS");
			guy.addDialoge("just for you.");
			guy.addDialoge("You need to GRIND your way to ..");
			guy.addDialoge("MAX LEVEL.");
            guy.addDialoge("Good luck!");*/


			background.makeBG(0, 300, 2000, 0, 0, 0.5f, true, 2);

		}
		System.out.println("typeis:: "+type);
		
		if(type.equals("Level1")){
            System.out.println("typeis:: "+type);
			int size = 10+magicNum.nextInt(10);
			//int lastX = 3;
			int length;
			int totalLength = 0;
			for(int i=0; i<size; i++){
				length = (1+magicNum.nextInt(5))*50;
				int theheight = magicNum.nextInt(3);
				
				level ground = new level(num);
				
				/*if(magicNum.nextInt(10)==0){
					ground.setDeadly(true);
					theheight = theheight+5;
					length = 40;
					ground.setColor(Color.MAGENTA);
				}*/
				
				ground.setPosition(x+totalLength,y+roomY-200+theheight*15);
				ground.setDimension(length,400);



                if(magicNum.nextInt(3)==0){
                    swordman bady = new swordman(num);
                    bady.setPosition(x+totalLength+50-bady.getwidth()/2, y+roomY-200+theheight*15-bady.getheight());
                    bady.setLVL(4 * num - magicNum.nextInt(3 * num));
                    bady.setMAXHP(100*bady.getLVL());

                    bady.setATK(1 * bady.getLVL());

                }else{
                    if(magicNum.nextInt(3)==0){
                        wizard bady2 = new wizard(num);
                        bady2.setPosition(x+totalLength+50-bady2.getwidth()/2, y+ roomY - 200 + theheight * 15 - bady2.getheight());
                        bady2.setLVL(4 * num - magicNum.nextInt(3 * num));
                        bady2.setMAXHP(100 * bady2.getLVL());
                        bady2.setATK(1*bady2.getLVL());
                    }
                }
				
				if(magicNum.nextInt(4)==0){
					level ground2 = new level(num);
					ground2.setPosition(x+totalLength,y+roomY-380+theheight*15);
					ground2.setDimension(length,(1+magicNum.nextInt(3))*25);

                    pool.newDrop(x+totalLength+50,y+ roomY - 200 + theheight * 15 - 30,1,itemDealer.request(magicNum.nextInt(2),num,-1));

                }
				
				if(i+1>=size){
					level door = new level(num);
					door.setPosition(x+totalLength+length/2-12,y+roomY-200+theheight*15-50);
					door.setDimension(25,50);
					door.setColor(Color.ORANGE);
					door.setNoColl(true);
					door.setInteraction(true, "NextLevel");
				}
				
				totalLength = totalLength+length;
			}
			
			
			
			level ground1 = new level(num);
			ground1.setPosition(x-100,y);
			ground1.setDimension(200,roomY);
			
			level ground2 = new level(num);
			ground2.setPosition(x,y);
			ground2.setDimension(totalLength,200);
			
			level ground3 = new level(num);
			ground3.setPosition(totalLength,y);
			ground3.setDimension(200,roomY);

			//background.makeBG(0, 2000,2000,totalLength,(int)(totalLength/130),0.3f);
			
			//background.makeBG(1, 50,2000,totalLength,(int)(totalLength/100),0.5f);
			background.makeBG(0, 300,2000,totalLength,(int)(totalLength/100),0.7f, true, 2);


		}

        if(type.equals("Level2")){
			color = Color.getHSBColor(0.3f,0.25f,0.5f);

            System.out.println("typeis:: "+type);
            int size = 10+magicNum.nextInt(10);
            //int lastX = 3;
            int length;
            int totalLength = 0;
            for(int i=0; i<size; i++){
                length = (1+magicNum.nextInt(5))*50;
                int theheight = magicNum.nextInt(3);

                level ground = new level(num);

				/*if(magicNum.nextInt(10)==0){
					ground.setDeadly(true);
					theheight = theheight+5;
					length = 40;
					ground.setColor(Color.MAGENTA);
				}*/

                ground.setPosition(x+totalLength,y+roomY-200+theheight*15);
                ground.setDimension(length,400);
                ground.setColor(color);



                if(magicNum.nextInt(3)==0){
                    swordman bady = new swordman(num);
                    bady.setPosition(x+totalLength+50-bady.getwidth()/2, y+roomY-200+theheight*15-bady.getheight());
                    bady.setLVL(4 * num - magicNum.nextInt(3 * num));
                    bady.setMAXHP(100*bady.getLVL());

                    bady.setATK(1 * bady.getLVL());

                }else{
                    if(magicNum.nextInt(3)==0){
                        wizard bady2 = new wizard(num);
                        bady2.setPosition(x+totalLength+50-bady2.getwidth()/2, y+ roomY - 200 + theheight * 15 - bady2.getheight());
                        bady2.setLVL(4 * num - magicNum.nextInt(3 * num));
                        bady2.setMAXHP(100 * bady2.getLVL());
                        bady2.setATK(1*bady2.getLVL());
                    }
                }

                if(magicNum.nextInt(4)==0){
                    level ground2 = new level(num);
                    ground2.setPosition(x+totalLength,y+roomY-380+theheight*15);
                    ground2.setDimension(length,(1+magicNum.nextInt(3))*25);

                    pool.newDrop(x + totalLength + 50, y + roomY - 200 + theheight * 15 - 30, 1, itemDealer.request(magicNum.nextInt(2), num, -1));

                }

                if(i+1>=size){
                    level door = new level(num);
                    door.setPosition(x+totalLength+length/2-12,y+roomY-200+theheight*15-50);
                    door.setDimension(25,50);
                    door.setColor(Color.ORANGE);
                    door.setNoColl(true);
                    door.setInteraction(true, "NextLevel");
                }

                totalLength = totalLength+length;
            }



            level ground1 = new level(num);
            ground1.setPosition(x - 100, y);
            ground1.setDimension(200, roomY);
            ground1.setColor(color);

            level ground2 = new level(num);
            ground2.setPosition(x, y);
            ground2.setDimension(totalLength, 200);
            ground2.setColor(color);

            level ground3 = new level(num);
            ground3.setPosition(totalLength, y);
            ground3.setDimension(200, roomY);
            ground3.setColor(color);

            //background.makeBG(0, 2000,2000,totalLength,(int)(totalLength/130),0.3f);

            //background.makeBG(1, 50,2000,totalLength,(int)(totalLength/100),0.5f);
			background.makeBG(0, 2000,2000,totalLength,(int)(totalLength/100),0.3f, true, 1);


        }

        if(type.equals("Level3")){
			color = Color.getHSBColor(0.1f,0.25f,0.8f);

            System.out.println("typeis:: "+type);
            int size = 10+magicNum.nextInt(10);
            //int lastX = 3;
            int length;
            int totalLength = 0;
            for(int i=0; i<size; i++){
                length = (1+magicNum.nextInt(5))*50;
                int theheight = magicNum.nextInt(2);

                level ground = new level(num);

				/*if(magicNum.nextInt(10)==0){
					ground.setDeadly(true);
					theheight = theheight+5;
					length = 40;
					ground.setColor(Color.MAGENTA);
				}*/

                ground.setPosition(x+totalLength,y+roomY-200+theheight*15);
                ground.setDimension(length,400);
                ground.setColor(color);



                if(magicNum.nextInt(3)==0){
                    swordman bady = new swordman(num);
                    bady.setPosition(x+totalLength+50-bady.getwidth()/2, y+roomY-200+theheight*15-bady.getheight());
                    bady.setLVL(4 * num - magicNum.nextInt(3 * num));
                    bady.setMAXHP(100*bady.getLVL());

                    bady.setATK(1 * bady.getLVL());

                }else{
                    if(magicNum.nextInt(3)==0){
                        wizard bady2 = new wizard(num);
                        bady2.setPosition(x+totalLength+50-bady2.getwidth()/2, y+ roomY - 200 + theheight * 15 - bady2.getheight());
                        bady2.setLVL(4 * num - magicNum.nextInt(3 * num));
                        bady2.setMAXHP(100 * bady2.getLVL());
                        bady2.setATK(1*bady2.getLVL());
                    }
                }

                if(magicNum.nextInt(3)==0){

                    pool.newDrop(x+totalLength+length/2-30,y+ roomY - 200 + theheight * 15 - 50,1,itemDealer.request(magicNum.nextInt(2),num,-1));

                }

                if(i+1>=size){
                    level door = new level(num);
                    door.setPosition(x+totalLength+length/2-12,y+roomY-200+theheight*15-50);
                    door.setDimension(25,50);
                    door.setColor(Color.ORANGE);
                    door.setNoColl(true);
                    door.setInteraction(true, "NextLevel");
                }

                totalLength = totalLength+length;
            }



            level ground1 = new level(num);
            ground1.setPosition(x - 100, y);
            ground1.setDimension(200,roomY);
            ground1.setColor(color);

            level ground2 = new level(num);
            ground2.setPosition(x,y);
            ground2.setDimension(totalLength, 200);
            ground2.setColor(color);

            level ground3 = new level(num);
            ground3.setPosition(totalLength,y);
            ground3.setDimension(200, roomY);
            ground3.setColor(color);

            //background.makeBG(0, 2000,2000,totalLength,(int)(totalLength/130),0.3f);

            //background.makeBG(1, 50,2000,totalLength,(int)(totalLength/100),0.5f);
            background.makeBG(0, 2000,2000,totalLength,(int)(totalLength/200),0.1f, true, 3);



        }

        if(type.equals("Level4")){
            color = Color.getHSBColor(0.1f,0.25f,0.8f);

            System.out.println("typeis:: "+type);
            int size = 5+magicNum.nextInt(5);
            //int lastX = 3;
            int length;
            int totalLength = 0;

            int org_x = -400;
            int org_y = 850;

            int c_x = org_x;
            int c_y = org_y;

            for(int i=0; i<size; i++){
                levelPart thisP = worldGenerator.generateNextPart(c_x,c_y,num,color);

                c_x = thisP.to_x;
                c_y = thisP.to_y;
            }

            levelPart thisP = worldGenerator.generateEnd(c_x,c_y,num,color);

            c_x = thisP.to_x;
            c_y = thisP.to_y;


            //background.makeBG(0, 300,2000,totalLength,(int)(totalLength/100),0.7f, true, 2);

            background.makeBG(0, 800,2100,c_x-org_x,((c_x-org_x)/200),0.6f, true, 3);

            //background.makeBG(1, 50,2000,c_x-org_x,(int)((c_x-org_x)/200),0.6f,true,3);


        }
		
	}




	public int getX(){return x;}
	public int getY(){return y;}
	
	public int getWidth(){return roomX;}
	public int getHeight(){return roomY;}
	
	public ArrayList<MapObject> getList(){return objects;}
	
	public void kill(MapObject object){objects.remove(object);}
	
	public void push(MapObject object){objects.add(object);}

}
