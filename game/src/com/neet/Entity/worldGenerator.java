package com.neet.Entity;
import java.awt.*;
import java.util.Random;
import java.util.ArrayList;

import com.neet.Audio.JukeBox;
import com.neet.GameState.running;

public final class worldGenerator {
	
	private static int activeLevel = 0;
	
	public static Random magicNum = new Random();
	//private static ArrayList<Integer> MainBranch = new ArrayList<Integer>();
	private static ArrayList<aLevel> Levels = new ArrayList<aLevel>();
	//private static ArrayList<ArrayList<Object>> Levels = new ArrayList<ArrayList<Object>>();
	
	//private static int[][] grid = new int[100][100];
	
	private static int length;
	
	private static int posX = 0;
	private static int posY = 0;
	
	private static int trys = 0;
	private static int levels = 0;

    private static int realm = 1;


    private static int current_x = 0;
    private static int current_y = 0;

    private static int prev_x = 0;
    private static int prev_y = 0;


    public worldGenerator(){
		//create();
		Levels.clear();
		//MainBranch.clear();
	}
	
	public static void create(){
        //JukeBox.loop("theme");
        createBase();
		
	}
	
	public static void createBase(){
		length = 100;
        System.out.println("LEVEL SIZE = "+Levels.size());
		/*for(int i = 0; i<length; i++){
			Levels.add(new Object());
		}*/

        for(int i = 0; i<Levels.size(); i++){
            System.out.println("LEVEL = " + Levels.get(i));
        }

		new aLevel(0,0,levels,"Limbo");
		levels++;
	}
	
	public static void addLevel(){
		//running.showBreak("Level "+String.valueOf(levels));
        String type = "Level"+String.valueOf(realm);

		new aLevel(0,0,levels,type);

        //pool.fill(20, levels);

        levels++;


        //running.hideBreak();
	}
	
	public static ArrayList<aLevel> getLevels(){
		return Levels;
	}
	
	/*public static ArrayList<Integer> getMainBranch(){
		return MainBranch;
	}*/
	
	public static int getLength(){
		return length;
	}
	
	public static Random getRandom(){
		return magicNum;
	}
	
	public static aLevel getLevel(int i){
		return Levels.get(i);
	}

	public static int getLevelNum(){
		return levels;
	}
	
	/*public static int[][] getGrid(){
		return grid;
	}*/


	public static aLevel getActiveLevel() {
		return (aLevel) Levels.get(activeLevel);
	}
	
	public static void setActiveLevel(int level){
		activeLevel = level;
	}

    public static void setRealm(int nextRealm){
        realm = nextRealm;
    }

	public static void returnToBase(){

		Levels.clear();
		levels = 0;
		setRealm(1);
		createBase();

        running.player.switchToLevel(0);

	}

    public static levelPart generateNextPart(int from_x, int from_y, int level, Color color){
        levelPart part = new levelPart(from_x,from_y);

        //part.from_x = current_x;
        //part.from_y = current_y;

        current_x = from_x;
        current_y = from_y;
        prev_x = from_x;
        prev_y = from_y;

        int type = magicNum.nextInt(2);

        newPart(level,color,magicNum.nextInt(100)+100,200,current_x,current_y,true);

        if(type == 0){
            newPart(level,color,magicNum.nextInt(100)+100,200,current_x,current_y-20,true);
            newPart(level,color,magicNum.nextInt(200)+300,200,current_x,current_y+40,true);

            level stone1 = new level(level);
            stone1.setPosition(prev_x, current_y-20);
            stone1.setDimension(20, 20);
            stone1.setColor(Color.darkGray);
            stone1.setImg(4);

            level deco1 = new level(level);
            deco1.setPosition((current_x-((current_x-prev_x)/2))-90, current_y-60);
            deco1.setDimension(60, 60);
            deco1.setNoColl(true);
            deco1.setImg(3);

            level deco2 = new level(level);
            deco2.setPosition((current_x-((current_x-prev_x)/2))+30, current_y-60);
            deco2.setDimension(60, 60);
            deco2.setNoColl(true);
            deco2.setImg(3);

            if(magicNum.nextInt(1)==0){
                newEnemy(level,(current_x-((current_x-prev_x)/2))-90,current_y);
                newEnemy(level,(current_x-((current_x-prev_x)/2))+30,current_y);
            }

            if(magicNum.nextInt(2)== 0) {
                level stand = new level(level);
                stand.setPosition((current_x - ((current_x - prev_x) / 2)) - 10, current_y - 15);
                stand.setDimension(20, 15);
                stand.setNoColl(true);
                stand.setColor(Color.gray);

                pool.newDrop((current_x - ((current_x - prev_x) / 2))-15,current_y - 60,1,itemDealer.request(magicNum.nextInt(2),level,-1));

            }

        }

        if(type == 1){
            newPart(level,color,magicNum.nextInt(100)+100,200,current_x,current_y-20,true);
            newPart(level,color,magicNum.nextInt(100)+400,200,current_x,current_y-10,true);
        }

        newPart(level,color,magicNum.nextInt(100)+100,200,current_x,current_y-10,true);


        part.to_x = current_x;
        part.to_y = current_y;

        return part;
    }

    public static levelPart generateEnd(int from_x, int from_y, int level, Color color){
        levelPart part = new levelPart(from_x,from_y);

        //part.from_x = current_x;
        //part.from_y = current_y;

        current_x = from_x;
        current_y = from_y;
        prev_x = from_x;
        prev_y = from_y;

        //int type = magicNum.nextInt(2);

        newPart(level,color,magicNum.nextInt(100)+100,200,current_x,current_y+20,true);



        level door = new level(level);
        door.setPosition((current_x-((current_x-prev_x)/2))-12, current_y-50);
        door.setNoColl(true);
        door.setDimension(25, 50);
        door.setInteraction(true, "NextLevel");
        door.setImg(2);


        part.to_x = current_x;
        part.to_y = current_y;

        return part;
    }

    public static void newPart(int level, Color color, int width, int height,int x,int y, boolean forward){
        level ground = new level(level);

        ground.setPosition(x,y);

        ground.setDimension(width,height);

        if(forward) {
            prev_x = current_x;
            prev_y = current_y;

            current_x += width;
            current_y = y;

        }

        System.out.println(level+"plob"+current_x);
        System.out.println(""+current_y);

        ground.setColor(color);

    }

    public static void newEnemy(int level,int x,int y){
        System.out.println("newenemy");


        int type = magicNum.nextInt(2);

        if(type == 0) {
            wizard bady = new wizard(level);
            bady.setPosition(x, y - bady.getheight()*2);
            bady.setLVL(4 * level - magicNum.nextInt(3 * level));
            bady.setMAXHP(100 * bady.getLVL());
            bady.setATK(1 * bady.getLVL());
        }
        if(type == 1){
            swordman bady = new swordman(level);
            bady.setPosition(x, y - bady.getheight()*2);
            bady.setLVL(4 * level - magicNum.nextInt(3 * level));
            bady.setMAXHP(100 * bady.getLVL());
            bady.setATK(1 * bady.getLVL());
        }

    }
	
}
