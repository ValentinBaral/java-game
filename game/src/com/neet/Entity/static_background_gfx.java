package com.neet.Entity;

import com.neet.Main.GamePanel;

import java.awt.*;
import java.util.Random;
import java.util.Vector;

/**
 * Created by Valentin on 22/02/2015.
 */
public class static_background_gfx {

    private static Random random = new Random();

    private static int count = 0;

    private static Vector<Integer> x = new Vector<Integer>();
    private static Vector<Integer> y = new Vector<Integer>();
    private static Vector<Integer> width = new Vector<Integer>();
    private static Vector<Integer> height = new Vector<Integer>();
    private static Vector<Integer> effect = new Vector<Integer>();
    private static Vector<Integer> direction = new Vector<Integer>();
    private static Vector<Integer> dropTime = new Vector<Integer>();

    private static Vector<Color> color = new Vector<Color>();

    public static void update(){

        for(int i=count-1; i>=0; i--){
            updatePartikle(i);
        }

        //showNumbers.update();

    }

    public static void draw(Graphics2D g){

		/*for(int i=0; i<partikels.size(); i++){
			partikels.get(i).draw(g);
		}*/

        for(int i=0; i<count; i++){
            drawPartikle(i, g);
        }

        //showNumbers.draw(g);

    }

    private static void updatePartikle(int i){

        if(effect.get(i)==0){
            x.set(i, x.get(i)+random.nextInt(2));
            y.set(i, y.get(i)+random.nextInt(6));
        }

        if(effect.get(i)==1){
            y.set(i, y.get(i)+random.nextInt(3));
            if(direction.get(i)==1){
                x.set(i, x.get(i)+random.nextInt(2));
            }
            if(direction.get(i)==0){
                x.set(i, x.get(i)-random.nextInt(2));
            }
        }
        if(effect.get(i)==2){
            y.set(i, y.get(i)+random.nextInt(2));
            /*if(direction.get(i)==1){
                y.set(i, y.get(i)+random.nextInt(2));
            }
            if(direction.get(i)==0){
                y.set(i, y.get(i)-random.nextInt(2));
            }*/
        }

        if(effect.get(i)==3){
            x.set(i, x.get(i)+random.nextInt(3));
        }

        if(dropTime.get(i)==0){
            removeParticel(i);
            return;
        }

        if(dropTime.get(i)>0){
            dropTime.set(i, dropTime.get(i)-1);
        }

        if(x.get(i)> GamePanel.WIDTH || y.get(i)+height.get(i)>GamePanel.HEIGHT){
            removeParticel(i);
        }
    }

    private static void drawPartikle(int i, Graphics2D g){

        int newX = (int)(x.get(i));
        int newY = (int)(y.get(i));

        g.setColor(color.get(i));
        g.fillRect(newX, newY, width.get(i), height.get(i));
    }

    private static void removeParticel(int i){
        effect.remove(i);
        x.remove(i);
        y.remove(i);
        width.remove(i);
        height.remove(i);
        color.remove(i);
        dropTime.remove(i);
        direction.remove(i);
        count--;
    }

    public static void clean(){
        effect.clear();
        x.clear();
        y.clear();
        width.clear();
        height.clear();
        color.clear();
        dropTime.clear();
        direction.clear();
        count = 0;
    }

    public static void addParticle(int targetX, int targetY, int targetWidth, int targetHeight, int gfx, int amount, int time, Color theColor){

        for(int i=0; i<amount; i++){

            count++;
            effect.add(gfx);

            if(gfx == 3) {
                x.add(targetX + random.nextInt(40) - 20);
                y.add(targetY + random.nextInt(40) - 20);
                color.add(Color.getHSBColor(0.8f - ((float)random.nextInt(10)/50),0.5f + ((float)random.nextInt(20)/50),0.1f + ((float)random.nextInt(20)/50)));
            }else{
                x.add(targetX + random.nextInt(8) - 4);
                y.add(targetY + random.nextInt(8) - 4);
                color.add(theColor);
            }
            width.add(targetWidth+random.nextInt(3)-1);
            height.add(targetHeight+random.nextInt(5)-2);
            dropTime.add(time+random.nextInt(100)-50);
            direction.add(random.nextInt(2));


        }

    }

}
