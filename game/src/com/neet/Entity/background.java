package com.neet.Entity;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

import com.neet.Main.GamePanel;
import com.neet.world.map;

public class background {
	
	private static Random random = new Random();
	
	private static int count = 0;
	
	private static Vector<Integer> x = new Vector<Integer>();
	private static Vector<Integer> y = new Vector<Integer>();
	private static Vector<Integer> width = new Vector<Integer>();
	private static Vector<Integer> height = new Vector<Integer>();	
	private static Vector<Color> color = new Vector<Color>();
	private static Vector<Integer> style = new Vector<Integer>();
	
	public static void update(){

		static_background.update();

		for(int i=0; i<count; i++){
			//updatePartikle(i);
		}


	}
	
	public static void draw(Graphics2D g){

		static_background.draw(g);
		
		for(int i=0; i<count; i++){
			drawPartikle(i, g);
		}


	}
	
	private static void updatePartikle(int i){
		if(x.get(i)+width.get(i)+map.getx()>GamePanel.WIDTH || y.get(i)+height.get(i)+map.gety()>GamePanel.HEIGHT){
			removeParticel(i);
		}
	}
	
	private static void drawPartikle(int i, Graphics2D g){
		
		int newX = (int)(x.get(i)+map.getx()/2);
		int newY = (int)(y.get(i)+map.gety()*0.75);
		
		g.setColor(color.get(i));
		//g.fillRect(newX, newY, width.get(i), height.get(i));
		if(style.get(i)==0) {
			g.fillArc(newX, newY-440, width.get(i), height.get(i), 220, 90);
		}

		if(style.get(i)==1) {
			g.fillRect(newX, newY+650, width.get(i), height.get(i));
		}

        if(style.get(i)==2) {
            g.fillRect(newX, newY+585, width.get(i), height.get(i));
        }
	}
	
	private static void removeParticel(int i){
		x.remove(i);
		y.remove(i);
		width.remove(i);
		height.remove(i);
		color.remove(i);
		style.remove(i);
		count--;
	}
	
	public static void clear(){
		x.clear();
		y.clear();
		width.clear();
		height.clear();
		color.clear();
		style.clear();
		count=0;
	}
	
	public static void makeBG(int _style, int targetWidth, int targetHeight, int length, int amount, float col0r, boolean staticBG, int bgNUM){
		
		for(int i=0; i<amount; i++){
			
			
				count++;
				x.add(-1000+i*(length/amount));
				y.add(random.nextInt(50)-25);
				width.add(targetWidth+random.nextInt(100)-50);
				height.add(targetHeight+random.nextInt(100)-50);
				style.add(_style);
				
				color.add(Color.getHSBColor(col0r, 0.5f, (float)(0.2+(double)random.nextInt(15)/(double)100)));
				
		}

		if(staticBG){
			static_background.showBG(bgNUM);
		}
		
	}
	
}
