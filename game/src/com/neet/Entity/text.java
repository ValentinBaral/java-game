package com.neet.Entity;

import java.awt.Color;
import java.awt.Graphics2D;

import com.neet.GameState.MenuState;

public class text extends decoration {
	
	private String text = " ";
	
	public text(int room) {
		super(room);
		
		
	}
	
	public void draw(Graphics2D g){
		
		g.setFont(MenuState.titleFont);
		g.setColor(Color.BLACK);
		g.drawString(text, (int)x+(int)map.getx()+190, (int)y+(int)map.gety()+230);
		g.setColor(Color.WHITE);
		g.drawString(text, (int)x+(int)map.getx()+190, (int)y+(int)map.gety()+229);
		
		super.draw(g);
	}
	
	public void setText(String text){
		this.text = text;
	}

}
