package com.neet.Entity;

import java.awt.Color;
import java.awt.Graphics2D;

import com.neet.Audio.JukeBox;
import com.neet.events.utl;

public class box extends Solids{
	
	protected int lives;

	public box(int room) {
		super(room);
		
		lives = 2;
		
		IAM = IAM_DESTRUCTABLE;
		
		width = 25;
		height = 25;
		noGrav = false;
		move_static = false;
	}
	
	public void getAttacked(smthliving theEnemy, int damage){
		if(lives>0){
			lives = lives-1;
			JukeBox.play("playerhit");
		}
		if(lives<=0){
			/*worldGenerator.getRoom(activeRoom).getList().remove(this);
			living = false;*/
			
			die();

		}
		
		gfx.addParticle((int)x, (int)y, 3, 3, 1, 20, 15, Color.GRAY,0,false);
		
		showNumbers.addNumber((int) x + ((int) width / 2), (int) y, "1", 35, Color.RED, utl.Arial10);
		
		System.out.println("rekt");
	}
	
	
	public void die(){
		noCollision = true;
		worldGenerator.getLevel(activeLevel).getList().remove(this);
		

	}
	
	public void draw(Graphics2D g) {
		
		if(lives>1){
			g.setColor(Color.getHSBColor(0.1f, 0.7f, 1f));
		}else{
			g.setColor(Color.getHSBColor(0.1f, 0.6f, 0.8f));
		}
		g.fillRect((int)map.getx()+(int)x, (int)map.gety()+(int)y, width, height);
		
		super.draw(g);
	}

}
