package com.neet.Entity;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;

import com.neet.Audio.JukeBox;
import com.neet.events.utl;

public class npc extends smthliving{
	
	brain brain = new brain(this);

	public npc(int room) {
		super(room);
		
		Name = "Jack";
		
		IAM = IAM_NPC;
		
		width = 12;
		height = 12;
		
		ATK = 1;
		
		HP = 2;
		
		jumpStrength = 15;
		
		weaponRadius = 10;
		
		preferredTarget = IAM_ENEMY;
		
		animFrames = new int[]{4,4,2,3,3,3,3,4,4};
		animRepeat = new int[]{1,1,1,1,1,1,1,1,1};
		
		animScriptHead_X = new int[][]{{0,0,1,1},{0,0,1,1},{0},{0},{0},{0},{0},{0,0,0,0,0,0,0},{0}};
		animScriptHead_Y = new int[][]{{0,0,0,0},{0,0,0,0},{0},{0},{0},{0},{0},{-4,-8,-12,-16,-12,-4,4},{0}};
		animHeadFrames = new int[]{4,4,0,0,0,0,0,7,0};
		animHeadHasFrames = new int[]{0,0,0,0,0,0,0,0,0};
		
		animScriptTop_X = new int[][]{{0},{0,0,0,0},{0},{0},{0},{0},{0},{0,0},{0}};
		animScriptTop_Y = new int[][]{{0},{0,-1,-1,0},{0},{0},{0},{0},{0},{2,4},{0}};
		animTopFrames = new int[]{0,4,0,0,0,0,0,2,0};
		animTopHasFrames = new int[]{0,0,0,0,0,0,0,0,0};
		
		animScriptBottom_X = new int[][]{{0},{0,0,0,0},{0},{0},{0},{0},{0},{0},{0}};
		animScriptBottom_Y = new int[][]{{0},{0,0,0,0},{0},{0},{0},{0},{0},{0},{0}};
		animBottomFrames = new int[]{0,4,0,0,0,0,0,0,0};
		animBottomHasFrames = new int[]{0,1,0,0,0,0,0,0,0};
		
		animScriptWeapon_X = new int[][]{{0},{0,0,0,0},{4},{3,4},{0},{0},{0},{0,0},{0}};
		animScriptWeapon_Y = new int[][]{{0},{0,0,-1,-1},{0},{0,0},{0},{0},{0},{2,4},{0}};
		animWeaponFrames = new int[]{0,4,1,2,0,0,0,2,0};
		animWeaponHasFrames = new int[]{0,0,0,0,0,0,0,0,0};
		
		
	}
	
	public void getAttacked(smthliving theEnemy){
		if(HP>0){
			HP = HP-theEnemy.getATK();
			JukeBox.play("damage");
		}
		if(HP<=0){
			/*worldGenerator.getRoom(activeRoom).getList().remove(this);
			living = false;*/
			
			die();
			
			//worldGenerator.getRoom(running.player.getRoom()).checkCleared();
			
		}
		gfx.addParticle((int)x, (int)y, 3, 3, 1, 20, 15, Color.RED,0,false);
		
		pushBack(theEnemy.getFLeft());
		
	}
	
	public void update(){
		if(living){
			brain.think();
		}
		
		super.update();
	}
	
	public void draw(Graphics2D g){
		
		/*int row = 0;
		for(int i = 0; i<getHealth(); i++){
			if(i%3==0){
				row++;
			}
			cutImage2 = image2.getSubimage(0*sprite_dimension, 1*sprite_dimension, sprite_dimension, sprite_dimension);
			g.drawImage(cutImage2, (int)x+(int)map.getx()+i*6-13-18*row, (int)y+(int)map.gety()-30-row*6, null);
		}*/
		
		
		/*g.setColor(Color.RED);
		if(interaction){
			if(inRadius){
				
				FontMetrics pf = g.getFontMetrics(utl.Times10);

				g.setFont(utl.Times10);
				g.setColor(Color.WHITE);
				
				if(Name != null){
					g.drawString(Name,(int)x+(int)map.getx()+width/2-pf.stringWidth(Name)/2,(int)y+(int)map.gety()-10);
				}
				g.setColor(Color.YELLOW);
				if(Description != null){
					g.drawString(Description,(int)x+(int)map.getx()+width/2-pf.stringWidth(Description)/2,(int)y+(int)map.gety()-22);
				}

			}
		}*/
		
		super.draw(g);
	}
	
	public void jaw(){
		//JukeBox.play("jaw");
	}

}
