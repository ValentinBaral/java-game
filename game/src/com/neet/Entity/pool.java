package com.neet.Entity;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by valentinbaral on 15/04/15.
 */
public class pool {

    private static ArrayList<shot> shots = new ArrayList<shot>();
    private static ArrayList<drop> drops = new ArrayList<drop>();




    public static void update(){

    }

    public static void fill(int num, int activeLevel){

        for(int i=0;i<num;i++){
            shots.add(new shot(activeLevel));
            drops.add(new drop(activeLevel));
        }

    }

    public static void clear(){
        System.out.println("@pool clear");
        shots.clear();
        drops.clear();
    }

    public static void newShot(smthliving obj){
        shot Shot = findShot();

        System.out.println("@newShot request");

        if(Shot != null){

            System.out.println("@newShot found");


            if(obj.getFLeft()){
                Shot.face_left = true;
                Shot.face_right = false;

                Shot.setPosition((double) (((int) obj.getx() - Shot.getwidth())), obj.gety());

            }else{
                Shot.face_right = true;
                Shot.face_left = false;

                Shot.setPosition((double) (((int) obj.getx())),obj.gety());

            }

            //Shot.setATK(obj.getATK());

            Shot.setObj(obj);

            Shot.setActive(true);

            //Shot.update();
        }

    }

    public static void newDrop(double _x, double _y, int type, item Item){
        drop Drop = findDrop();

        System.out.println("@newDrop request");

        if(Drop != null){

            Drop.setPosition(_x, _y);
            Drop.setInteraction(true, "drop");
            Drop.setType(type);

            if(Item != null){
                Drop.setItem(Item);
            }

            Drop.setActive(true);

            if(type == 0){
                Drop.width = 10;
                Drop.height = 10;
            }
            if(type == 1){
                Drop.width = 30;
                Drop.height = 30;
            }

        }

    }

    private static shot findShot(){

        System.out.println("@findShot");


        for(int i=0;i<shots.size();i++){
            if(shots.get(i).getActive() == false){
                return shots.get(i);
            }
        }

        return null;

    }

    private static drop findDrop(){

        System.out.println("@findDrop");


        for(int i=0;i<drops.size();i++){
            if(drops.get(i).getActive() == false){
                System.out.println("@foundDrop");
                return drops.get(i);
            }
        }

        return null;

    }



}



