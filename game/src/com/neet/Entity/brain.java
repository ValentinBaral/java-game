package com.neet.Entity;

import java.util.Random;

import com.neet.GameState.running;

public class brain {

	public String type = "";
	
	public smthliving guy;
	
	private MapObject Opponent;
	
	private boolean bored = true;
	private boolean waiting = false;
	private boolean hasOpponent = false;
	
	private boolean preparingAttack = false;
	private boolean preparingReload = false;
	
	private int waitTime = 0;
	private int waitCount = 0;

	static Random magicNum = new Random();
	
	
	public brain(smthliving guy){
		this.guy = guy;
	}
	
	public void think(){
		
		if(magicNum.nextInt(1000) == 0){
			//guy.jaw();
		}
		
		if(guy.getMoveLock()){wait(1);}
		
		if(waiting){
			
			guy.setMLeft(false);
			guy.setMRight(false);
			
			waitCount++;
			if(waitCount>=waitTime){
				waitCount = 0;
				waitTime = 0;
				waiting = false;
				
				
				
			}
		}
		
		if(waiting&&preparingAttack){
			preparingAttack = false;
			if(Opponent.getx()<=guy.getx()+guy.getwidth()+10 && Opponent.getx()+Opponent.getwidth()>=guy.getx()-10){
				if(Opponent.gety()<=guy.gety()+guy.getheight() && Opponent.gety()+Opponent.getheight()>=guy.gety()){
					preparingAttack = true;
				}
			}
		}
		

		if(!waiting && preparingAttack){


			guy.dash();
			preparingAttack = false;
			wait(60);

		}
			
		if(!waiting && preparingReload){
			guy.reload();
			preparingReload = false;
			wait(60);
		}
			
		
		if(!waiting && !preparingAttack && !preparingReload){
			
			if(!hasOpponent)findOpponent();
			
			if(hasOpponent){
				if(Opponent.getLiving()){attack();}
			}
			
			if(bored){
				
				//patrole();
				
			}
			
		}
		
	}
	
	public void wait(int time){
		waiting = true;
		waitTime = waitTime+time;
	}
	
	public void prepareAttack(int time){
		wait(time);
		preparingAttack = true;
	}
	
	public void prepareReload(int time){
		wait(time);
		preparingReload = true;
	}
	
	public void findOpponent(){
		//System.out.println("guyLevel "+guy.getLevel());
		//System.out.println("playerLevel "+running.player.getLevel());
		for(int i = 0; i < worldGenerator.getLevel(guy.getLevel()).getList().size(); i++){
			MapObject that = worldGenerator.getLevel(guy.getLevel()).getList().get(i);
			
			if(that.IAM() == guy.preferredTarget){
				Opponent = that;
				bored = false;
				hasOpponent = true;
			}
			
		}
	}
	
	public void attack(){

		if (Opponent.getx() <= guy.getx() + guy.getwidth() + 100 && Opponent.getx() + Opponent.getwidth() >= guy.getx() - 100) {
			if (Opponent.gety() <= guy.gety() + guy.getheight() +50 && Opponent.gety() + Opponent.getheight() >= guy.gety() -50) {


				if (guy.getMLeft()) {
					guy.moveLeft(0.5);
				}
				if (guy.getMRight()) {
					guy.moveRight(0.5);
				}


				if (Opponent.getx() + Opponent.getwidth() < guy.getx()) {

					if (guy.getMRight()) {
						wait(10 + magicNum.nextInt(10));
					}

					guy.setFLeft(true);
					guy.setFRight(false);

					guy.setMLeft(true);
					guy.setMRight(false);

				}
				if (Opponent.getx() > guy.getx() + guy.getwidth()) {

					if (guy.getMLeft()) {
						wait(10 + magicNum.nextInt(10));
					}

					guy.setFLeft(false);
					guy.setFRight(true);

					guy.setMLeft(false);
					guy.setMRight(true);

				}

				if(type == "wizard") {

					if (Opponent.getx() <= guy.getx() + guy.getwidth() + 80 && Opponent.getx() + Opponent.getwidth() >= guy.getx() - 80) {
						if (Opponent.gety() <= guy.gety() + guy.getheight() + 10 && Opponent.gety() + Opponent.getheight() >= guy.gety() -10) {
							guy.shoot();
							prepareReload(120);

						}
					}

				}else {

					if (Opponent.getx() <= guy.getx() + guy.getwidth() + 10 && Opponent.getx() + Opponent.getwidth() >= guy.getx() - 10) {
						if (Opponent.gety() <= guy.gety() + guy.getheight() && Opponent.gety() + Opponent.getheight() >= guy.gety()) {
							prepareAttack(10);
						}
					}

					if (guy.getCollision_left() || guy.getCollision_right()) {
					/*guy.MoveUp();
					wait(60);*/
					}
				}

			}
		}else{
			waiting = true;
			preparingAttack = false;
		}
	}
	
	public void patrole(){
		if(guy.getFLeft()){
			guy.moveLeft(0.3);
			if(guy.getCollision_left()){
				guy.setFLeft(false);
				guy.setFRight(true);
				
				wait(30);
			}
		}
		if(guy.getFRight()){
			guy.moveRight(0.3);
			if(guy.getCollision_right()){
				guy.setFLeft(true);
				guy.setFRight(false);
				
				wait(30);
			}
		}
	}
	
}
