package com.neet.Entity;
import com.neet.GameState.running;
import com.neet.events.utl;
import com.neet.world.map;

import java.awt.*;

public class decoration extends Solids {

	protected String text = "";

	public decoration(int room) {
		super(room);
		noCollision = true;
		//hasImage = true;

	}
	
	public void handleWindowFrames(){
		
		if(animRealFrames > 0){
			if(running.player.getOldX() > running.player.getx()){animCount--;}
		}
		
		if(animRealFrames < animFrames[activeImg]-1){
			if(running.player.getOldX() < running.player.getx()){animCount++;}
		}
		
		animRealFrames = (int)(animCount/animDelay);
		/*if((int)(animCount/animDelay) >= animFrames[activeImg]){
			animCount = 0;
			animRealFrames = 0;
		}*/
	}
	
	public void update(){
		if(specialImg){
			if(activeImg == 1){
				if(running.player.getLevel() == activeLevel){
					handleWindowFrames();
				}
			}
		}
		
		super.update();
	}

	public void draw(Graphics2D g){

		if(text != ""){
			g.setColor(Color.GRAY);
			g.setFont(utl.Arial14);

			g.drawString(text, (int)x+(int)map.getx(), (int)y+(int)map.gety());
		}

		super.draw(g);
	}
	
	public void setImg(int img){activeImg = img;hasImage = true;}

	public void setText(String text){this.text = text;}

}
