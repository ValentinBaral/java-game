package com.neet.Entity;

/**
 * Created by valentinbaral on 19/04/15.
 */
public class item implements Cloneable{
    private String NAME;
    private int TYPE;
    private int SPRITE;
    private int ATK;
    private int DEF;
    private int MAGI;
    private int WEIGHT;
    private int RARITY;

    public item(){

    }

    public item(item other){
        this.NAME = other.getNAME();
        this.TYPE = other.getTYPE();
        this.SPRITE = other.getSPRITE();
        this.ATK = other.getATK();
        this.DEF = other.getDEF();
        this.MAGI = other.getMAGI();
        this.WEIGHT = other.getWEIGHT();
        this.RARITY = other.getRARITY();
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public int getTYPE() {
        return TYPE;
    }

    public void setTYPE(int TYPE) {
        this.TYPE = TYPE;
    }

    public int getSPRITE() {
        return SPRITE;
    }

    public void setSPRITE(int SPRITE) {
        this.SPRITE = SPRITE;
    }

    public int getATK() {
        return ATK;
    }

    public void setATK(int ATK) {
        this.ATK = ATK;
    }

    public int getDEF() {
        return DEF;
    }

    public void setDEF(int DEF) {
        this.DEF = DEF;
    }

    public int getMAGI() {
        return MAGI;
    }

    public void setMAGI(int MAGI) {
        this.MAGI = MAGI;
    }

    public int getWEIGHT() {
        return WEIGHT;
    }

    public void setWEIGHT(int WEIGHT) {
        this.WEIGHT = WEIGHT;
    }

    public int getRARITY() {
        return RARITY;
    }

    public void setRARITY(int RARITY) {
        this.RARITY = RARITY;
    }
}
