package com.neet.Entity;

import java.util.ArrayList;

/**
 * Created by valentinbaral on 18/04/15.
 */
public class inventory {

    //private ArrayList<ArrayList<Integer>> head = new ArrayList<ArrayList<Integer>>();
    //private ArrayList<ArrayList<Integer>> top = new ArrayList<ArrayList<Integer>>();
    //private ArrayList<ArrayList<Integer>> weapon = new ArrayList<ArrayList<Integer>>();

    //public

    private smthliving obj;

    public inventory(smthliving obj){
        this.obj = obj;
    }

    private ArrayList<item> items = new ArrayList<item>();
    private ArrayList<Boolean> equipped = new ArrayList<Boolean>();

    public void add(item Item){

        System.out.println("@inventory addItem");

        items.add(Item);
        equipped.add(false);

        if(getEquippedItemForType(Item.getTYPE())!=null){

        }else {
            equip(items.size()-1);
        }


    }

    public void remove(int num){
        items.remove(num);
        equipped.remove(num);
    }

    public void equip(int num){
        int type = items.get(num).getTYPE();
        int nth = 0;
        for(int i = 0; i<items.size(); i++){
            if(items.get(i).getTYPE() == type){
                equipped.set(i, false);
                nth = i;
            }
        }
        equipped.set(num, true);
    }

    public void unequip(int num){
        int type = num;
        int nth = 0;
        for(int i = 0; i<items.size(); i++){
            if(items.get(i).getTYPE() == type){
                equipped.set(i, false);
                nth = i;
            }
        }
    }

    public int getEquippedForType(int type){
        int equippedNum = 0;

        for(int i = 0; i<items.size(); i++){
            if(items.get(i).getTYPE() == type){
                if(equipped.get(i)){

                    equippedNum++;
                    return equippedNum;
                }
            }
        }
        return equippedNum;
    }

    public item getEquippedItemForType(int type){

        for(int i = 0; i<items.size(); i++){
            if(items.get(i).getTYPE() == type){
                if(equipped.get(i)){

                    return items.get(i);
                }
            }
        }
        return null;
    }

    public ArrayList<item> getEquipped(){
        //int equippedNum = 0;
        ArrayList<item> allEquipped = new ArrayList<item>();

        for(int i = 0; i<items.size(); i++){
            if(equipped.get(i)) {

                allEquipped.add(items.get(i));
            }
        }

        return allEquipped;
    }

    public int getNthEquippedForType(int type){
        int equippedNum = 0;
        int oftype = 0;

        for(int i = 0; i<items.size(); i++){
            if(items.get(i).getTYPE() == type){

                if(equipped.get(i)) {

                    equippedNum++;
                    return oftype;
                }
                oftype++;
            }
        }
        return -1;
    }

    public int getNumItemsForType(int type){
        int itemNum = 0;

        for(int i = 0; i<items.size(); i++){
            if(items.get(i).getTYPE() == type){
                itemNum++;
            }else{
            }
        }

        return itemNum;
    }

    public int getNthOfType(int num, int type){
        int itemNum = 0;

        for(int i = 0; i<items.size(); i++){
            if(items.get(i).getTYPE() == type){


                if(itemNum == num){
                    return i;
                }

                itemNum++;
            }
        }

        return -1;
    }

    public ArrayList<item> getItems(){
        return items;
    }

    public item getItem(int num){
        return items.get(num);
    }

}
