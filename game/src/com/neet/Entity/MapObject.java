package com.neet.Entity;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import com.neet.Audio.JukeBox;
import com.neet.Handlers.Keys;
import com.neet.Main.GamePanel;
import com.neet.events.utl;
import com.neet.world.map;

public abstract class MapObject {
	
	protected map map;



	
	// position
	
	protected double x;
	protected double y;
	
	protected double nextX;
	protected double nextY;
	
	protected double oldX;
	protected double oldY;
	
	// dimensions
	protected int width;
	protected int height;

    public boolean visible = true;
	
	// movement
	protected boolean move_up;
	protected boolean move_down;
	protected boolean move_falling;
	protected boolean move_crouching;
	protected boolean move_wj;
	//protected boolean move_OnGround;
	protected boolean move_static;
	protected boolean noCollision = false;
	
	protected boolean living;
	
	public boolean render = true;
	
	protected int IAM;
	public static int IAM_PLAYER = 0;
	public static int IAM_ENEMY = 1;
	public static int IAM_NPC = 2;
	
	public static int IAM_SOLID = 3;
	
	public static int IAM_DOOR = 4;
	public static int IAM_SHOT = 5;
	
	public static int IAM_DESTRUCTABLE = 6;

    protected int nextRealm = 0;
	//following different enemy IAMs
	
	protected boolean noGrav = false;
	
	protected boolean collision_left;
	protected boolean collision_top;
	protected boolean collision_right;
	protected boolean collision_bottom;
	
	protected BufferedImage image;
	protected BufferedImage cutImage;
	
	protected BufferedImage image2;
	protected BufferedImage cutImage2;
	
	protected int sprite_dimension = 60;
	
	protected int preferredTarget;
	
	protected int activeLevel = 0;

	protected int ATK;

    protected int HP = 100;
    protected int maxHP = 100;

	protected boolean face_left = false;
	protected boolean face_right = true;
	
	protected boolean deadly = false;
	
	protected ArrayList<String> dialogLines = new ArrayList<String>();
	protected String interactionType = "";
	protected boolean interaction = false;
	protected boolean inRadius = false;
	protected boolean showDialog = false;
	protected int dialogLevel = 0;

    protected boolean active = true;
	
	protected int dialogCooldown = 0;
	protected int dialogCooldownMax = 240;
	
	//modifiers
	protected double baseX = 2;
	protected double baseY = 1.5;
	protected double speed = 1;
	
	public MapObject() {
		ATK = 0;
		//collision.push(this);
		
		try{
			image2 = ImageIO.read(getClass().getResourceAsStream("/Sprites/Other/sprite_other.png"));
		}catch(Exception e) {
		}
	}

	public void setMapPosition() {
		//xmap = map.getx();
		//ymap = map.gety();
	}
	
	public void moveUp(double multiply){
		if(!collision_top || noCollision){
			nextY = nextY-baseY*speed*multiply;
		}
	}
	
	public void moveDown(double multiply){
		if(!collision_bottom || noCollision){
			nextY = nextY+baseY*speed*multiply;
			//speed = speed+speed*0.02;
		}
	}
	
	public void moveLeft(double multiply){
		if(!collision_left || noCollision){
			nextX = nextX-baseX*speed*multiply;
		}
	}

	public void moveRight(double multiply){
		if(!collision_right || noCollision){
			nextX = nextX+baseX*speed*multiply;
		}
	}
	
	public void gravity() {
		if(!collision_bottom && !move_static && !noGrav){
			moveDown(1);
		}
	}
	
	public void setPosition(double x, double y) {
		this.x = x;
		this.y = y;
		nextX = x;
		nextY = y;
		
		oldX = x;
		oldY = y;
	}
	
	public void setDimension(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	public void setCollisions(boolean collision_left, boolean collision_top, boolean collision_right, boolean collision_bottom) {
		this.collision_left = collision_left;
		this.collision_top = collision_top;
		this.collision_right = collision_right;
		this.collision_bottom = collision_bottom;
	}
	
	public void setStatic(boolean move_static) {
		this.move_static = move_static;
	}
	
	public void setColor(Graphics2D g) {
		//this.width = width;
		//this.height = height;
	}
	
	public void calcSpeed(){
		if(speed>1){
			speed = speed-speed*0.008;
		}else{
			speed = 1;
		}
		if(speed > 3){
			speed = 3;
		}
		/*if(nextX==x && nextY==y){
			speed = 1;
		}*/
	}
	
	public void playSound(String name){
		JukeBox.play(name); // + volume
	}
	
	public void update() {
		// render


		if(((int)x+width+(int)map.getx()>0 && (int)x+(int)map.getx()<GamePanel.WIDTH)&&((int)y+height+(int)map.gety()>0 && (int)y+(int)map.gety()<GamePanel.HEIGHT)){
			render = true;
		}else{
			render = false;
		}


		/* NEW SPEED
		
		input -> calcNextCoordinades -> collisionWithNewCoordinades -> draw (1frame)
		
		NEW SPEED */

		gravity();
		//calcSpeed();

		//if(IAM == IAM_PLAYER){collision.checkRoom(this);}
		if(!move_static && IAM != IAM_SHOT){collision.check(this);}
		if(IAM == IAM_SHOT){collision.check(this);}

		if(showDialog){
			if(dialogCooldown<dialogCooldownMax){
				dialogCooldown++;
			}else{
				dialogLevel = 0;
				showDialog = false;
			}
		}

		// update position
		oldX = x;
		oldY = y;

		x = nextX;
		y = nextY;
	}
	
	public void draw(Graphics2D g) {
		if(showDialog){
			FontMetrics pf = g.getFontMetrics(utl.Times10);

			g.setFont(utl.Times10);
			g.setColor(Color.WHITE);
			
			if(dialogLines != null){
				g.drawString(dialogLines.get(dialogLevel-1),(int)x+(int)map.getx()+width/2-pf.stringWidth(dialogLines.get(dialogLevel-1))/2,(int)y+(int)map.gety()-30);
			}
		}
		
		if(interaction){
			if(inRadius){
				if(interactionType != "drop"){
					FontMetrics pf = g.getFontMetrics(utl.Times10);
	
					g.setFont(utl.Times10);
					
					g.setColor(Color.WHITE);
					g.drawRoundRect((int)x+(int)map.getx()+width/2-pf.stringWidth(Keys.keys[Keys.BUTTON3])/2-3, (int)y+(int)map.gety()-19, pf.stringWidth(Keys.keys[Keys.BUTTON3])+6, 12, 6, 6);
					
					g.setColor(Color.WHITE);
					g.drawString(Keys.keys[Keys.BUTTON3],(int)x+(int)map.getx()+width/2-pf.stringWidth(Keys.keys[Keys.BUTTON3])/2,(int)y+(int)map.gety()-10);
				}else{
					
				}
			}
		}
		
	}
	
	public abstract void getAttacked(smthliving theEnemy, int damage);
	public abstract void jaw();
	
	public void clearNext(){
		nextX = x;
		nextY = y;
	}
	
	public double getx() {return x;}
	public double gety() {return y;}
	
	public void setx(double x) {this.x = x;}
	public void sety(double y) {this.y = y;}
	
	public double getNextX() {return nextX;}
	public double getNextY() {return nextY;}
	
	public void setNextX(double nextX) {this.nextX = nextX;}
	public void setNextY(double nextY) {this.nextY = nextY;}
	
	public double getOldX() {return oldX;}
	public double getOldY() {return oldY;}
	
	public int getwidth() {return width;}
	public int getheight() {return height;}
	
	public boolean getCollision_left() {return collision_left;}
	public boolean getCollision_top() {return collision_top;}
	public boolean getCollision_right() {return collision_right;}
	public boolean getCollision_bottom() {return collision_bottom;}


	
	public double getSpeed() {return speed;}
	
	public boolean getStatic() {return move_static;}
	public boolean getNoColl() {return noCollision;}
	public void setNoColl(boolean noCollision) {this.noCollision = noCollision;}
	
	public boolean getLiving(){return living;}
	
	public int IAM() {return IAM;}
	public void setIAM(int IAM) {this.IAM = IAM;}

	public int getPreferredTarget() {return preferredTarget;}

	public void setLevel(int i){activeLevel = i;}
	public int getLevel(){return activeLevel;}


    public boolean getFLeft() {return face_left;}
	public boolean getFRight() {return face_right;}
	
	public boolean getDeadly() {return deadly;}
	public void setDeadly(boolean deadly) {this.deadly = deadly;}

	public boolean getInteraction() {
		return interaction;
	}

    public boolean getActive() {return active;}
    public void setActive(boolean active) {this.active = active;}
	
	public void setInteraction(boolean interaction,String interactionType) {
		this.interaction = interaction;
		this.interactionType = interactionType;
	}
	
	public void setInRadius(boolean inRadius) {
		this.inRadius = inRadius;
	}
	
	public void addDialoge(String line){
		dialogLines.add(line);
	}
	
	public void interaction(smthliving obj){
		if(interactionType == "Dialog"){
			if(dialogLines.size()>0){
				dialogCooldown = 0;
				if(dialogLevel<dialogLines.size()){
					showDialog = true;
					dialogLevel++;
					
				}else{
					dialogLevel = 0;
					showDialog = false;
				}
			}
		}
		if(interactionType == "NextLevel"){
			if(obj.getLevel() < worldGenerator.getLevels().size()){
				//System.out.println(worldGenerator.getLevels());
				//obj.setLevel(obj.getLevel()+1);
                if(nextRealm !=0){
                    worldGenerator.setRealm(nextRealm);
                }
				obj.switchLevel(obj.getLevel()+1);

			}
		}
	}

	public String getInteractionType() {
		return interactionType;
	}

    public void forRealm(int nextRealm){
        this.nextRealm = nextRealm;
    }



	
	

}
