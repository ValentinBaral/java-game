package com.neet.Entity;

import com.neet.GameState.running;
import com.neet.events.utl;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;

import javax.imageio.ImageIO;


public class shot extends MapObject {

	private int activeAnim = 2;
    private int animCount = 0;
    private int animDelay = 15;
    private int animRealFrames = 0;
    private boolean animating = true;

    private int[] animFrames = {3};

    private smthliving obj;
	
	public shot(int activeLevel){
        active = false;

		/*this.activeLevel = activeRoom;
		//worldGenerator.getLevel(activeRoom).getList().add(this);
        worldGenerator.getLevel(activeLevel).getList().add(this);

		System.out.println("new shot"+activeRoom+" jmm "+Enemy.getx());

		
		width = 6;
		height = 4;
	
		ATK = 100;
		
		IAM = IAM_SHOT;
		
		noGrav = true;
        //noCollision = true;


		if(fleft){
			//x = (double)(((int)Enemy.getx()-width));
			face_left = true;
			face_right = false;

            setPosition((double)(((int)Enemy.getx()-width)),Enemy.gety());

		}else{
			//x = (double)((int)Enemy.getx()+Enemy.getwidth());
			face_right = true;
			face_left = false;

            setPosition((double)(((int)Enemy.getx()-width)),Enemy.gety());

        }*/
		
		//y = Enemy.gety();

        this.activeLevel = activeLevel;
        worldGenerator.getLevel(activeLevel).getList().add(this);

        width = 6;
        height = 4;

        ATK = 100;

        IAM = IAM_SHOT;

        noGrav = true;

        image = utl.image_other;

        render = true;


        /*try{
			image = ImageIO.read(getClass().getResourceAsStream("/Sprites/Other/sprite_other.png"));
        }catch(Exception e) {
		}*/
	}
	
	public void handleFrames(){
		if(animating){
			animCount++;
			animRealFrames = (int)(animCount/animDelay);
			
			if((int)(animCount/animDelay) >= animFrames[activeAnim]){
				animCount = 0;
				animRealFrames = 0;

			}
		}
	}
	
	public void update(){
		if(face_left){
			moveLeft(0.3);
		}
		if(face_right){
            moveRight(0.3);
        }

        System.out.println("new x"+((int)x+(int)map.getx()));
        System.out.println("new y"+((int)y+(int)map.gety()));
		
		super.update();
	}
	
	public void draw(Graphics2D g){
		if(face_right){
			cutImage = image.getSubimage(animRealFrames*sprite_dimension, activeAnim*sprite_dimension, sprite_dimension, sprite_dimension);

			AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
		    tx.translate(-cutImage.getWidth(null), 0);
		    AffineTransformOp op = new AffineTransformOp(tx,AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		    cutImage = op.filter(cutImage, null);
			
		    /*for(int i = 0; i<15; i=i+2){
		    	g.setColor(Color.getHSBColor(0, 0, (float)(1-(i*0.05))));

				g.fillRect((int)x-i+(int)map.getx(), (int)y+(int)map.gety(), width, height);
		    	
			}*/

			//g.drawImage(cutImage, (int)x-i+(int)map.getx()-24, (int)y+(int)map.gety()-21, null);

		}
		if(face_left){
			cutImage = image.getSubimage(animRealFrames*sprite_dimension, activeAnim*sprite_dimension, sprite_dimension, sprite_dimension);

			/*for(int i = 0; i<15; i++){
				
				g.setColor(Color.getHSBColor(0, 0, (float)(1-(i*0.05))));

				g.fillRect((int)x+i+(int)map.getx(), (int)y+(int)map.gety(), width, height);
				

			}*/
		}
		
		
		g.drawImage(cutImage, ((int)x+(int)map.getx())-24, ((int)y+(int)map.gety())-23, null);
		
		super.draw(g);
	}

	public void getAttacked(smthliving theEnemy, int damage) {
		
	}

    public smthliving getObj(){
        return obj;
    }

    public void setObj(smthliving obj){
        this.obj = obj;
    }

	@Override
	public void jaw() {
		// TODO Auto-generated method stub
		
	}
}
