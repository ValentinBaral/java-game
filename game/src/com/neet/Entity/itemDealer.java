package com.neet.Entity;

import com.neet.events.utl;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by valentinbaral on 18/04/15.
 */
public class itemDealer {

    private static Random rand = new Random();

    public static String[] name1 = {"Mighty", "Vengenceful", "Merciless", "Brutal", "Minor", "Big","Small","Infinity","Rusty","Evil","Godlike"};
    public static String[] namew2 = {"Sword", "Crusher", "Punisher", "Knife"};

    public static String[] nameh2 = {"Hat", "Mask", "Helmet", "Hair"};


    public static item request(int type, int level, int sprite){
        //int[] item = new int[10];

        item Item = new item();

        int rarity_gen = rand.nextInt(100);

        if(rarity_gen<5){
            Item.setRARITY(4);
        }
        else if(rarity_gen<15){
            Item.setRARITY(3);
        }
        else if(rarity_gen<50){
            Item.setRARITY(2);
        }
        else{
            Item.setRARITY(1);
        }

        if(type == 0){
            //head
            Item.setNAME(name1[rand.nextInt(name1.length)] + " " + nameh2[rand.nextInt(nameh2.length)]);
            Item.setTYPE(0);

            if(sprite>0){
                Item.setSPRITE(sprite);
            }else {
                Item.setSPRITE(rand.nextInt(utl.hat.size()));
            }
            Item.setATK(((rand.nextInt(10)*rand.nextInt(10))*level)*Item.getRARITY());
            Item.setDEF((50+rand.nextInt(10)*level)*Item.getRARITY());
            Item.setWEIGHT(5+rand.nextInt((Item.getATK()+Item.getDEF())/level));
            Item.setMAGI(rand.nextInt(level)*Item.getRARITY());
        }
       /* if(type == 1){
            //top
            Item.setNAME("some top");
            Item.setTYPE(1);

            if(sprite>0){
                Item.setSPRITE(sprite);
            }else {
                Item.setSPRITE(rand.nextInt(utl.top.size()));
            }
            Item.setATK(rand.nextInt(5)*rand.nextInt(5));
            Item.setDEF(50+rand.nextInt(10)*level);
            Item.setWEIGHT(5+rand.nextInt(Item.getATK()+Item.getDEF()));
            Item.setMAGI(rand.nextInt(level));
        }*/
        if(type == 1){
            //weapon
            Item.setNAME(name1[rand.nextInt(name1.length)] + " " + namew2[rand.nextInt(namew2.length)]);
            Item.setTYPE(1);

            if(sprite>0){
                Item.setSPRITE(sprite);
            }else {
                Item.setSPRITE(rand.nextInt(utl.weapon.size()));
            }
            Item.setATK((50+rand.nextInt(100)*level)*Item.getRARITY());
            Item.setDEF(rand.nextInt(10));
            Item.setWEIGHT(5+rand.nextInt((Item.getATK()+Item.getDEF())/level));
            Item.setMAGI(rand.nextInt(level)*Item.getRARITY());
        }

        System.out.println("LEVEL!!!!!!!!!======== "+level);

        return Item;
    }
}
