package com.neet.Entity;
import java.awt.Color;
import java.awt.Graphics2D;
import java.io.File;

import javax.imageio.ImageIO;

import com.neet.Main.GamePanel;
import com.neet.world.map;

public class Solids extends MapObject{
	
	protected boolean hasImage;
	
	protected int numImgs = 5;
	protected int activeImg;
	protected int[] animFrames = new int[numImgs];
	
	protected int animCount = 0;
	protected int animDelay = 15;
	protected int animRealFrames = 0;
	
	protected boolean specialImg = false;
	
	protected boolean transforming = false;
	protected int transformCount = 0;
	protected int transformReal;
	protected int transformDelay = 30;
	protected int transformDirection = 0;
	protected int transformNew;
	

	public Solids(int room) {
		worldGenerator.getLevel(room).getList().add(this);
		activeLevel = room;
		
		try{
			//path = "C:/Users/gaertneeeeeeeer/workspace/gamedevsuccess/gamedevsuccess/Resources/Sprites/Other/sprite_decoration.png";
	        //file = new File(path);
	        //image = ImageIO.read(file);
	        image = ImageIO.read(getClass().getResourceAsStream("/Sprites/Other/sprite_decoration.png"));
		}catch(Exception e) {
			//e.printStackTrace();
		}
		
		animFrames = new int[]{1, 20,1,1,1,1};
		
		move_static = true;
		
		living = false;
		
		IAM = IAM_SOLID;
	}
	
	public void getAttacked(smthliving theEnemy, int damage){
		
	}
	
	public void slide(int direction, int newNum){
		transforming = true;
		transformDirection = direction;
		transformNew = newNum;
	}
	
	public void handleFrames(){
		if(!specialImg){
			animCount++;
			animRealFrames = (int)(animCount/animDelay);
			if((int)(animCount/animDelay) >= animFrames[activeImg]){
				animCount = 0;
				animRealFrames = 0;
			}
		}
	}
	
	public void handleTransform(){

		if(transforming){
			transformCount++;
			transformReal = (int)(transformCount/transformDelay);
			if(transformDirection == 1){
				height = height-transformReal;
				if(height <= transformNew){
					transformCount = 0;
					transformReal = 0;
					transforming = false;
				}
			}
		}
	}
	
	public void update(){
		handleFrames();
		handleTransform();
		
		super.update();
	}
	
	public void draw(Graphics2D g) {
		double drawX = x;
		double drawY = y;
		
		//double drawX = (x-oldX)*GamePanel.interpolation+oldX;
		//double drawY = (y-oldY)*GamePanel.interpolation+oldY;
		
		if(hasImage){
			cutImage = image.getSubimage(animRealFrames*sprite_dimension, sprite_dimension*activeImg, sprite_dimension, sprite_dimension);
			g.drawImage(cutImage, (int)drawX+(int)map.getx()+width/2-30, (int)drawY+height/2-30+(int)map.gety(), null);
		}
		
		super.draw(g);
	}
	
	public void setSpecialAnim(boolean YES){specialImg = YES;}

	@Override
	public void jaw() {
		// TODO Auto-generated method stub
		
	}

}
