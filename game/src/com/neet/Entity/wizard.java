package com.neet.Entity;

public class wizard extends enemy {

    public wizard(int room) {
        super(room);

        super.brain.type = "wizard";

        IAM = IAM_ENEMY;

        width = 12;
        height = 12;

        ATK = 20;

        HP = 300;
        maxHP = HP;

        jumpStrength = 15;

        weaponRadius = 10;

        //sound_atk = "machine_gun";

        head = 1;
        top = 1;
        bottom = 1;
        //weapon = 0;


        animFrames = new int[]{4,4,2,3,3,1,4,4,4};
        animRepeat = new int[]{1,1,1,1,1,1,1,1,1};

        animScriptHead_X = new int[][]{{0,0,1,1},{0,0,1,1},{0},{0},{0},{0},{0},{0,0,0,0,0,0,0},{0}};
        animScriptHead_Y = new int[][]{{0,0,0,0},{0,0,0,0},{0},{0},{0},{0},{0},{-4,-8,-12,-16,-12,-4,10},{0}};
        animHeadFrames = new int[]{4,4,0,0,0,0,0,7,0};
        animHeadHasFrames = new int[]{0,0,0,0,0,0,0,0,0};

        animScriptTop_X = new int[][]{{0},{0,0,0,0},{0},{0},{0},{0},{0},{0,0},{0}};
        animScriptTop_Y = new int[][]{{0},{0,-1,-1,0},{0},{0},{0},{0},{0},{4,8},{0}};
        animTopFrames = new int[]{0,4,0,4,0,0,0,2,0};
        animTopHasFrames = new int[]{0,0,0,1,0,0,0,0,0};

        animScriptBottom_X = new int[][]{{0},{0,0,0,0},{0},{0},{0},{0},{0},{0},{0}};
        animScriptBottom_Y = new int[][]{{0},{0,0,0,0},{0},{0},{0},{0},{0},{0},{0}};
        animBottomFrames = new int[]{0,4,0,0,1,0,0,0,0};
        animBottomHasFrames = new int[]{0,1,0,0,1,0,0,0,0};

        animScriptWeapon_X = new int[][]{{0},{0,0,0,0},{4,4,4},{0,1,2,2},{0},{0},{0},{0,0},{0}};
        animScriptWeapon_Y = new int[][]{{0},{0,0,-1,-1},{0},{-1,-1,-2,-2},{0},{0},{0},{0,0},{0}};
        animWeaponFrames = new int[]{0,4,1,4,0,0,0,2,0};
        animWeaponHasFrames = new int[]{0,0,0,1,0,0,0,0,0};

    }

}
