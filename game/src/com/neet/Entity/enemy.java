package com.neet.Entity;

import java.awt.*;

import com.neet.Audio.JukeBox;
import com.neet.events.utl;

public class enemy extends smthliving{

	brain brain = new brain(this);

	public enemy(int room) {
		super(room);

		head = 0;
		top = 0;
		bottom = 0;
		//weapon = 0;

        Inventory.add(itemDealer.request(0,LVL,-1));
        Inventory.add(itemDealer.request(1,LVL,-1));

		animFrames = new int[]{4,4,2,3,3,1,4,4,4};
		animRepeat = new int[]{1,1,1,1,1,1,1,1,1};

		animScriptHead_X = new int[][]{{0,0,1,1},{0,0,1,1},{0},{0},{0},{0},{0},{0,0,0,0,0,0,0},{0}};
		animScriptHead_Y = new int[][]{{0,0,0,0},{0,0,0,0},{0},{0},{0},{0},{0},{-4,-8,-12,-16,-12,-4,4},{0}};
		animHeadFrames = new int[]{4,4,0,0,0,0,0,7,0};
		animHeadHasFrames = new int[]{0,0,0,0,0,0,0,0,0};
		animHeadRow = new int[]{0,0,0,0,0,0,0,0,0};


		animScriptTop_X = new int[][]{{0},{0,0,0,0},{0},{0},{0},{0},{0},{0,0},{0}};
		animScriptTop_Y = new int[][]{{0},{0,-1,-1,0},{0},{0},{0},{0},{0},{2,4},{0}};
		animTopFrames = new int[]{0,4,0,4,0,0,0,2,0};
		animTopHasFrames = new int[]{0,0,0,1,0,0,0,0,0};
		animTopRow = new int[]{0,0,0,3,0,0,0,0,0};


		animScriptBottom_X = new int[][]{{0},{0,0,0,0},{0},{0},{0},{0},{0},{0},{0}};
		animScriptBottom_Y = new int[][]{{0},{0,0,0,0},{0},{0},{0},{0},{0},{0},{0}};
		animBottomFrames = new int[]{0,4,0,0,1,0,0,0,0};
		animBottomHasFrames = new int[]{0,1,0,0,1,0,0,0,0};
		animBottomRow = new int[]{0,1,0,0,4,0,0,0,0};


		animScriptWeapon_X = new int[][]{{0},{0,0,0,0},{4,4,4},{0,1,2,2},{0},{0},{0},{0,0},{0}};
		animScriptWeapon_Y = new int[][]{{0},{0,0,-1,-1},{0},{-1,-1,-2,-2},{0},{0},{0},{0,0},{0}};
		animWeaponFrames = new int[]{0,4,1,4,0,0,0,2,0};
		animWeaponHasFrames = new int[]{0,0,0,1,0,0,0,0,0};
		animWeaponRow = new int[]{0,0,0,1,0,0,0,0,0};

		/*try{
			//path = "C:/Users/gaertneeeeeeeer/workspace/gamedevsuccess/gamedevsuccess/Resources/Sprites/Player/sprite_enemy.png";
	        //file = new File(path);
	        //image = ImageIO.read(file);
			//image = ImageIO.read(getClass().getResourceAsStream("/Sprites/Enemies/sprite_soldat.png"));
			
			head = ImageIO.read(getClass().getResourceAsStream("/Sprites/Player/sprite_head.png"));
			top = ImageIO.read(getClass().getResourceAsStream("/Sprites/Player/sprite_body_1.png"));
			bottom = ImageIO.read(getClass().getResourceAsStream("/Sprites/Player/sprite_bottom_1.png"));
			weapon = ImageIO.read(getClass().getResourceAsStream("/Sprites/Player/sprite_weapon_1.png"));
	        
		}catch(Exception e) {
		}*/

	}
	
	public void getAttacked(smthliving theEnemy, int damage){
		if(HP-damage>0){
			HP = HP-damage;
			JukeBox.play("damage");
		}else{
			/*worldGenerator.getRoom(activeRoom).getList().remove(this);
			living = false;*/

			theEnemy.gainEXP(LVL);
			
			die();

            pool.newDrop(x,y,0,null);
			
			/*drop Drop = new drop(activeLevel, 0);
			Drop.setPosition(x, y);
			Drop.setInteraction(true, "drop");*/
			
			
			//worldGenerator.getRoom(running.player.getRoom()).checkCleared();
			
		}
		gfx.addParticle((int)x, (int)y, 3, 3, 1, 20, 15, Color.RED,0,false);
		
		showNumbers.addNumber((int) x, (int) y, String.valueOf(damage), 35, Color.RED, utl.Arial10);
		
		pushBack(theEnemy.getFLeft());
		
	}

	public void update(){
		if(living){
			brain.think();
		}
		
		super.update();
	}
	
	public void draw(Graphics2D g){
		
		/*int row = 0;
		for(int i = 0; i<getHealth(); i++){
			if(i%3==0){
				row++;
			}
			cutImage2 = image2.getSubimage(0*sprite_dimension, 1*sprite_dimension, sprite_dimension, sprite_dimension);
			g.drawImage(cutImage2, (int)x+(int)map.getx()+i*6-13-18*row, (int)y+(int)map.gety()-30-row*6, null);
		}*/

		g.setColor(Color.GREEN);


		
		g.fillRect(((int) x + (int) map.getx() + (int) (width / 2)) - (int) ((HP / 50) / 2), (int) y + (int) map.gety() - 6, ((int) (float) HP / 50), 4);

        g.setColor(Color.BLUE);

        int xs[] = {((int)x+(int)(width/2)-6)+(int)map.getx(), ((int)x+(int)(width/2))+(int)map.getx(), ((int)x+(int)(width/2)+6)+(int)map.getx(), ((int)x+(int)(width/2))+(int)map.getx()};
        int ys[] = {((int)y+(int)(-14))+(int)map.gety(), ((int)y+(int)(-14)-6)+(int)map.gety(), ((int)y+(int)(-14))+(int)map.gety(), ((int)y+(int)(-14)+6)+(int)map.gety()};
        g.fillPolygon(xs, ys, 4);

		FontMetrics pf = g.getFontMetrics(utl.Monospace9);

		String theLEVEL = String.valueOf(LVL);
		g.setFont(utl.Monospace9);
		g.setColor(Color.WHITE);
		g.drawString(theLEVEL,(int)x+(int)map.getx()+width/2-pf.stringWidth(theLEVEL)/2,(int)y+(int)map.gety()-10);

		super.draw(g);
	}
	
	public void jaw(){
		JukeBox.play("jaw");
	}

}
