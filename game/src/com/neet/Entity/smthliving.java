package com.neet.Entity;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

import com.neet.Audio.JukeBox;
import com.neet.events.utl;

public class smthliving extends MapObject {

	protected String Name;
	protected String Description;

    // Stats
    protected int LVL = 1;
    protected int EXP = 0;
    protected int EXPNEED = 100;


    // Jumps
	protected int jumpStrength = 35;
	protected int nextJumpCount = 0;
	protected int jumpCount = 0;
	protected int jumpDuration = 0;
	protected int numberJumps = 0;
	protected boolean jumping;
	protected boolean jumpLock;
	protected boolean jumpUp = false;
	protected boolean jumpLeft = false;
	protected boolean jumpRight = false;
	
	// Dash
	protected int airDashPower = 20;
	protected int speedDashPower = 10;
	
	protected int dashPower = 20;
	protected int dashCount = 0;
	protected boolean dashLock = false;
	protected boolean dashing = false;
	protected boolean dashing2 = false;
	protected boolean dashing3 = false;
	protected int dash2Count = 0;
	protected int dash2Time = 10;

	
	protected boolean directionLock = false;
	protected boolean moveLock = false;
	
	protected boolean animLock = false; //onetime
	
	protected boolean move_left;
	protected boolean move_right;
	
	protected boolean walking = false;

	protected boolean overlayActive = false;
	
	protected boolean wallrunning = false;
	protected int wallrunCount = 0;
	protected int wallrunMax = 30;
	
	protected int numAnims = 9;
	
	protected int[] animFrames = new int[numAnims];
	protected int[] animRepeat = new int [numAnims];
	
	protected int[][] animScriptHead_X = new int[numAnims][20];
	protected int[][] animScriptHead_Y = new int[numAnims][20];
	protected int[] animHeadFrames = new int[numAnims];
	protected int[] animHeadHasFrames = new int[numAnims];
	protected int[] animHeadRow = new int[numAnims];
	
	protected int[][] animScriptTop_X = new int[numAnims][20];
	protected int[][] animScriptTop_Y = new int[numAnims][20];
	protected int[] animTopFrames = new int[numAnims];
	protected int[] animTopHasFrames = new int[numAnims];
	protected int[] animTopRow = new int[numAnims];
	
	protected int[][] animScriptBottom_X = new int[numAnims][20];
	protected int[][] animScriptBottom_Y = new int[numAnims][20];
	protected int[] animBottomFrames = new int[numAnims];
	protected int[] animBottomHasFrames = new int[numAnims];
	protected int[] animBottomRow = new int[numAnims];

	protected int[][] animScriptWeapon_X = new int[numAnims][20];
	protected int[][] animScriptWeapon_Y = new int[numAnims][20];
	protected int[] animWeaponFrames = new int[numAnims];
	protected int[] animWeaponHasFrames = new int[numAnims];
	protected int[] animWeaponRow = new int[numAnims];
	
	
	protected int animHeadCount = 0;
	protected int animTopCount = 0;
	protected int animBottomCount = 0;
	protected int animWeaponCount = 0;
	
	protected int animHeadRealCount = 0;
	protected int animTopRealCount = 0;
	protected int animBottomRealCount = 0;
	protected int animWeaponRealCount = 0;
	
	protected int activeHeadAnim = 0;
	protected int activeTopAnim = 0;
	protected int activeBottomAnim = 0;
	protected int activeWeaponAnim = 0;
	
	protected boolean animHeadLock = false;
	protected boolean animTopLock = false;
	protected boolean animBottomLock = false;
	protected boolean animWeaponLock = false;

	protected int activeAnim = 1;
	protected int animCount = 0;
	protected int animDelay = 15;
	protected int animRealFrames = 0;
	protected boolean animating = true;
	
	protected int anim_standing = 0;
	protected int anim_running = 1;
	protected int anim_dash = 2;
	protected int anim_attacking = 3;
	protected int anim_jump = 4;
	protected int anim_block = 5;
	protected int anim_die = 7;
	protected int anim_reload = 8;

	protected int anim_mod = 0;
	protected boolean anim_ignore_mod = false;
	
	protected boolean pushed = false;
	protected boolean pushLeft;
	protected int pushX = 12;
	protected int pushY = 4;
	protected int pushUnlock = 30;
	protected int pushCount = 0;

	protected int waitTime = 0;
	protected int waitCount = 0;
	
	protected int weaponRadius = 10;
	
	protected boolean blocking = false;
	
	protected boolean die = false;
	
	protected boolean inAction = false;
	
	protected boolean animFreeze = false;
	
	protected MapObject interactTarget = null;


	
	/*protected int head = utl.random.nextInt(utl.head.size());
	protected int top = utl.random.nextInt(utl.top.size());
	protected int bottom = utl.random.nextInt(utl.bottom.size());
	protected int weapon = utl.random.nextInt(utl.weapon.size());*/

	protected int head = 0;
	protected int top = 0;
	protected int bottom = 0;
	//protected int weapon = 0;

    protected item itemHat = null;
    protected item itemWeapon = null;
	
	/*protected BufferedImage cutHead;
	protected BufferedImage cutBody;
	protected BufferedImage cutBottom;
	protected BufferedImage cutWeapon;

	protected BufferedImage sHead;
	protected BufferedImage sTop;
	protected BufferedImage sBottom;
	protected BufferedImage sWeapon;  */
	
	public static Random magicNum = new Random();

    protected inventory Inventory;

	
	// sounds 
	protected String sound_atk = "playerdash";
	
	
	protected MapObject target;
	
	protected ArrayList<MapObject> targets = new ArrayList<MapObject>();

	public smthliving(int room) {
		worldGenerator.getLevel(room).getList().add(this);
		activeLevel = room;

        Inventory = new inventory(this);
		
		living = true;
		
		//collision.checkRoom(this);
	}
	
	public void MoveUp() {
		if(!moveLock){
			if(!blocking){
				jump();
			}
		}
	}
	
	public void MoveDown() {
		//moveDown();
	}
	
	public void MoveLeft() {
		if(!moveLock){
			double multiply = 1;

			if(jumpUp){
				multiply = 1.1;
			}

            multiply = multiply - ((double)getWeight()/1000);
			
			/*if(blocking){
				multiply = 0.5;
			}
			if(!collision_bottom){
				multiply = 0.8;
			}*/
			
			moveLeft(multiply);

		}
	}
	
	public void MoveRight() {
		if(!moveLock){
			double multiply = 1;

			if(jumpUp){
				multiply = 1.1;
			}

            multiply = multiply - ((double)getWeight()/1000);
			
			/*if(blocking){
				multiply = 0.5;
			}
			if(!collision_bottom){
				multiply = 0.8;
			}*/
			
			moveRight(multiply);
		}

	}
	
	public void dash() {
		if(!blocking){
			if(jumping){
				ATK("airDash");
			}
			if(!jumping&&collision_bottom){
				if(move_left){
					ATK("speedDash");
				}
				else if(move_right){
					ATK("speedDash");
						
				}else{
					ATK("stillDash");
				}
			}
		}
	}
	
	public void jump(){
		
		/*if(jumping || !collision_bottom){

			if(!collision_bottom&&collision_right){

					cancelJump();
					noGrav = true;
					dashLock = false;
					JukeBox.play("playerjump");
					jumping = true;
					jumpCount = 0;
					jumpDuration = 0;
					jumpLeft = true;
					jumpUp = true;
					face_left = true;
					face_right = false;
					numberJumps++;
					speed = speed/speed;
					dashLock = false;
					jumpLock = true;
		
					gfx.addParticle((int)x, (int)y+height/2, 2, 2, 1, 30, 15, Color.GRAY, 0,false);


			}
			if(!collision_bottom&&collision_left){


					cancelJump();
					noGrav = true;
					dashLock = false;
					JukeBox.play("playerjump");
					jumping = true;
					jumpCount = 0;
					jumpDuration = 0;
					jumpRight = true;
					jumpUp = true;
					face_left = false;
					face_right = true;
					numberJumps++;
					speed = speed/speed;
					dashLock = false;
					jumpLock = true;
		
					gfx.addParticle((int)x, (int)y+height/2, 2, 2, 1, 30, 15, Color.GRAY, 0,false);



			}

		}*/
		
		if(!jumpLock&&collision_bottom){
			JukeBox.play("playerjump");

			if(move_left){
				jumpLeft = true;
				
			}
			if(move_right){
				jumpRight = true;
				
			}
			jumping = true;
			jumpLock = true;
			noGrav = true;
			jumpUp = true;
			collision_bottom = false;
			
			gfx.addParticle((int)x, (int)y, 2, 2, 1, 30, 15, Color.GRAY, 0,false);
		}
		
		
		

		
	}
	
	public void cancelJump(){
		jumpUp = false;
		jumpCount = 0;
		jumpLeft = false;
		jumpRight = false;
	}
	
	public double horizontalFalloff(){
		double multiply = 1;
		if(jumpDuration<jumpStrength){
			multiply = 1;
		}else{
			multiply = 1-(jumpDuration*0.02+(numberJumps*0.2));
			if(multiply<0) multiply = 0;
		}
		return multiply;
	}
	
	public void jumpKeyUp(){
		System.out.println("keyup?");
		//jump();
	}
	
	public void handleWallrun(){
		if(wallrunning){
			wallrunCount++;
			moveUp(1.2);
			gfx.addParticle((int)x, (int)y, 2, 2, 1, 30, 15, Color.GREEN, 0,false);
			System.out.println("wallrun");
			if(wallrunCount>wallrunMax){
				System.out.println("CANCELwallrun");
				wallrunCount = 0;
				//wallrunning = false;
				noGrav = false;
				jump();
			}
		}
	}
	
	public void handleJump(){
		if(jumpLock){
			
			if(collision_bottom){
				jumping = false;
				jumpDuration = 0;
				
				nextJumpCount++;
				if(nextJumpCount == 1){
					JukeBox.play("playerlands");
				}
				if(nextJumpCount >= 3){
					jumpLock = false;
					nextJumpCount = 0;
					jumpLeft = false;
					jumpRight = false;
					numberJumps = 0;	
				}	
			}
		}		
		
		if(jumping){
			
			walking = false;
			
			jumpDuration++;
						
			if(jumpUp){
				if(jumpCount<jumpStrength-numberJumps*2){
					jumpCount++;
					if(collision_top){
						jumpCount=jumpStrength-numberJumps*2;
						JukeBox.play("playerlands");
					}
					moveUp((0.2+(jumpStrength/jumpCount)/5)-(double)getWeight()/2000);
				}
				if(jumpCount>=jumpStrength-numberJumps*2){
					noGrav = false;
					jumpUp = false;
					jumpCount = 0;
				}
			}
			
			/*if(jumpLeft){
				if(collision_left){
					jumpLeft = false;
					JukeBox.play("playerlands");
				}
				moveLeft(horizontalFalloff());
			}
			if(jumpRight){
				if(collision_right){
					jumpRight = false;
					JukeBox.play("playerlands");
				}
				moveRight(horizontalFalloff());
			}*/
		}
	}
	
	public void ATK(String type){
		if(!dashing2 && !dashing){
			if(type == "airDash"){
				if(!dashLock){
					cancelJump();
					
					animBottom(anim_dash,true,true);
					animWeapon(anim_dash,true,true);
					
					dashPower = airDashPower;
					dashCount = 0;
					
					noGrav = true;
					dashing = true;
					directionLock = true;
					moveLock = true;
					JukeBox.play("playerdash");
				}
			}
			
			if(type == "speedDash"){
				
				animTop(anim_attacking, true, true);
				animWeapon(anim_attacking, true, true);
				
				dashPower = speedDashPower;
				dashCount = 0;
				
				dashing = true;
				dashing2 = true;
				directionLock = true;
				moveLock = true;
					
				JukeBox.play(sound_atk);
				//damage();
			}
			
			if(type == "stillDash"){
				dashing2 = true;
					
				animTop(anim_attacking, true, true);
				animWeapon(anim_attacking, true, true);
					
					
				JukeBox.play(sound_atk);
			}
		}
	}
	
	public void block(boolean blocklocked){
		if(blocklocked){
			if(!blocking){
				blocking = true;
				directionLock = true;
				resetFrames();
			}
		}
		if(!blocklocked){
			blocking = false;
			directionLock = false;
		}
	}
	
	public void animHead(int animation, boolean reset, boolean lock){
		if(!animHeadLock){
			activeHeadAnim = animation;
			if(reset){animHeadCount = 0;}
			if(lock){animHeadLock = true;}
		}
	}
	public void animTop(int animation, boolean reset, boolean lock){
		if(!animTopLock){
			activeTopAnim = animation;
			if(reset){animTopCount = 0;}
			if(lock){animTopLock = true;}
		}
	}
	public void animBottom(int animation, boolean reset, boolean lock){
		if(!animBottomLock){
			activeBottomAnim = animation;
			if(reset){animBottomCount = 0;}
			if(lock){animBottomLock = true;}
		}
	}	
	public void animWeapon(int animation, boolean reset, boolean lock){
		if(!animWeaponLock){

			if((activeWeaponAnim == anim_attacking || animation == anim_attacking)&&(animation==anim_attacking)){
				anim_ignore_mod = false;
				if(anim_mod == 0){anim_mod = 1;}else {anim_mod = 0;}
				System.out.println("MODED ANIMATION CHANING TO :::::"+anim_mod);
			}
			if(animation != anim_attacking){
				anim_ignore_mod = true;
			}

			activeWeaponAnim = animation;


			if(reset){animWeaponCount = 0;}
			if(lock){animWeaponLock = true;}
		}
	}
	
	public void damage(){
		if(collision.check_special(this, -weaponRadius, 5, weaponRadius*2, -10)){
			for(int i = 0;i<getTargets().size();i++){
				int damage = 0;
				double multiply = magicNum.nextDouble();
				if(magicNum.nextInt(5)==0){
					damage = (int)(getDamage()+((getDamage())*multiply));
				}else{
					damage = (int)(getDamage()-((getDamage()/2)*multiply));
				}

				getTargets().get(i).getAttacked(this, damage);
				if(i==getTargets().size()-1){
					getTargets().clear();
				}
			}
		}
	}
	
	public void handleDash(){
		if(dashLock){
			if(collision_bottom){
				dashLock = false;
			}
		}
		if(dashing){
			dashCount++;
			gfx.addParticle((int)x+width/2, (int)y+height/2, 1, 2, 2, 10, 20, Color.WHITE, 0,false);
			if(dashCount<dashPower){
				if(dashCount%2 == 0)damage();
				
				if(face_left){
					moveLeft(2);
					if(collision_left){
						dashCount=dashPower;
					}
				}
				if(face_right){
					moveRight(2);
					if(collision_right){
						dashCount=dashPower;
					}
				}
			}
			if(dashCount>=dashPower){
				dashCount = 0;
				noGrav = false;
				dashing = false;
				dashLock = true;
				directionLock = false;
				moveLock = false;
			}
		}
	}
	
	public void lock(int time){
		moveLock = true;
		waitTime = waitTime+time;
	}
	
	public void getAttacked(smthliving theEnemy, int damage){
		if(!blocking){
			if(HP>0){
				HP = HP-damage;
				JukeBox.play("damage");
			}
			if(HP<=0){
				die();
			}
		}else{
			JukeBox.play("magic");
		}
		
		
	}

	
	public void pushBack(boolean left){
		pushLeft = left;
		pushed = true;
		moveLock = true;
		noGrav = true;
		
	}
	
	public void interact(){
		if(interactTarget != null){
			interactTarget.interaction(this);
		}
	}
	
	public void die() {

        if(IAM == IAM_PLAYER){
            //running.showBreak("game over");

            worldGenerator.returnToBase();

            HP = maxHP;
        }else{

            //worldGenerator.getRoom(activeRoom).getList().remove(this);
            walking = false;
            living = false;
            die = true;

            HP = 0;

            animHead(anim_die, true, true);
            animTop(anim_die, true, true);
            animBottom(anim_die, true, true);
            animWeapon(anim_die, true, true);

            noCollision = true;
            noGrav = true;

            gfx.addParticle((int) x - 5 + (int) (width / 2), (int) y, 5, 5, 1, 50, 30, Color.RED, 0, false);


            visible = false;

        }

	}
	
	public void switchLevel(int level){
		
		worldGenerator.addLevel();
		
		worldGenerator.getLevel(activeLevel).getList().remove(this);
		worldGenerator.getLevel(level).getList().add(this);
		
		setPosition(
			worldGenerator.getLevel(level).getX()+300, 
			worldGenerator.getLevel(level).getY()+worldGenerator.getLevel(level).getHeight()-200-getheight()
		);
		
		activeLevel = activeLevel+1;
		worldGenerator.setActiveLevel(activeLevel);
		
	}

    public void switchToLevel(int level){

        if(worldGenerator.getLevels().size() > activeLevel){
            worldGenerator.getLevel(activeLevel).getList().remove(this);
        }
        worldGenerator.getLevel(level).getList().add(this);

        setPosition(
                worldGenerator.getLevel(level).getX()+300,
                worldGenerator.getLevel(level).getY()+worldGenerator.getLevel(level).getHeight()-200-getheight()
        );

        activeLevel = level;
        worldGenerator.setActiveLevel(activeLevel);
    }
	
	public void reload(){
		//animWeapon(anim_reload, true, true);
		//animBottom(anim_reload, true, true);
		//JukeBox.play("reload");
	}
	
	public void handlePushBack(){
		if(pushed){
			pushCount++;
			if(pushCount<pushY){
				moveUp(1);
			}
			if(pushCount<pushX){
				if(pushLeft){
					moveLeft(1);
				}
				if(!pushLeft){
					moveRight(1);
				}
			}
			if(pushCount==pushX){
				noGrav = false;
			}
			if(pushCount>=pushUnlock){
				pushed = false;
				pushCount = 0;
				moveLock = false;
			}
		}
	}
	
	public void handleFrames(){
		if(animating){
			
			animCount++;
			
			if(animHeadRealCount < animHeadFrames[activeHeadAnim])animHeadCount++;
			if(animTopRealCount < animTopFrames[activeTopAnim])animTopCount++;
			if(animBottomRealCount < animBottomFrames[activeBottomAnim])animBottomCount++;
			if(animWeaponRealCount < animWeaponFrames[activeWeaponAnim])animWeaponCount++;
			
			
			animRealFrames = (int)(animCount/animDelay);
			
			animHeadRealCount = (int)(animHeadCount/animDelay);
			animTopRealCount = (int)(animTopCount/animDelay);
			animBottomRealCount = (int)(animBottomCount/animDelay);
			animWeaponRealCount = (int)(animWeaponCount/animDelay);

			
			if(!die){
			
				if(animRealFrames >= animFrames[activeAnim]){
					animCount = 0;
					animRealFrames = 0;
					animLock = false;
				}
				
				if(animHeadRealCount >= animHeadFrames[activeHeadAnim]){
					animHeadCount = 0;
					animHeadRealCount = 0;
					animHeadLock = false;
				}
				
				if(animTopRealCount >= animTopFrames[activeTopAnim]){
					animTopCount = 0;
					animTopRealCount = 0;
					animTopLock = false;
				}
				
				if(animBottomRealCount >= animBottomFrames[activeBottomAnim]){
					animBottomCount = 0;
					animBottomRealCount = 0;
					animBottomLock = false;
				}
				
				if(animWeaponRealCount >= animWeaponFrames[activeWeaponAnim]){
					animWeaponCount = 0;
					animWeaponRealCount = 0;
					animWeaponLock = false;
				}
			
			}
			
		}
	}
	
	public void resetFrames(){
		animCount = 0;
	}
	
	public void update(){
		
		if(living){

            /*ArrayList<item> items =  Inventory.getEquipped();

            for(int i = 0; i<items.size(); i++){
                if(items.get(i).getTYPE() == 0){

                }
            }*/
            itemHat = Inventory.getEquippedItemForType(0);
            itemWeapon = Inventory.getEquippedItemForType(1);
		
			if(dashing2){
                dash2Count++;
				directionLock = true;
				if(dash2Count>dash2Time){
					damage();
					dash2Count = 0;
					dashing2 = false;
					directionLock = false;
				}
			}
			
			
			
			
			if(move_left || move_right){
				
				walking = true;
				
				animHead(anim_running, false, false);
				animTop(anim_running, false, false);
				animBottom(anim_running, false, false);
				animWeapon(anim_running, false, false);
				
			}else{
				
				walking = false;
				
				animHead(anim_standing, false, false);
				animTop(anim_standing, false, false);
				animBottom(anim_standing, false, false);
				animWeapon(anim_standing, false, false);
				
			}
			if(dashing){
				//animBottom(anim_dash, false, true);
			}

			if(!collision_bottom){
				animBottom(anim_jump, false, false);
			}
			
			
			if(!directionLock){
				if(move_left){
					face_left = true;
					face_right = false;
				}
				if(move_right){
					face_right = true;
					face_left = false;
				}
			}


            handlePushBack();
			handleDash();
			handleJump();
			//handleWallrun();
		}
		if(!animFreeze){
			handleFrames();
		}
		super.update();
		
	}
	
	public void draw(Graphics2D g){
		
		//double drawX = (x-oldX)*GamePanel.interpolation+oldX;
		//double drawY = (y-oldY)*GamePanel.interpolation+oldY;
		
		double drawX = x;
		double drawY = y;
		
		int headAnimation = animHeadRealCount;
		int topAnimation = animTopRealCount;
		int bottomAnimation = animBottomRealCount;
		int weaponAnimation = animWeaponRealCount;
		
		int headFrame = activeHeadAnim;
		int topFrame = activeTopAnim;
		int bottomFrame = activeBottomAnim;
		int weaponFrame = activeWeaponAnim;
		
		if(animHeadHasFrames[activeHeadAnim] == 0){
			headFrame = 0;
			headAnimation = 0;
		}
		if(animTopHasFrames[activeTopAnim] == 0){
			topFrame = 0;
			topAnimation = 0;
		}
		if(animBottomHasFrames[activeBottomAnim] == 0){
			bottomFrame = 0;
			bottomAnimation = 0;
		}
		if(animWeaponHasFrames[activeWeaponAnim] == 0){
			weaponFrame = 0;
			weaponAnimation = 0;
		}
		
		int Head_X = 0;
		int Head_Y = 0;
		
		int Top_X = 0;
		int Top_Y = 0;
		
		int Bottom_X = 0;
		int Bottom_Y = 0;
		
		int Weapon_X = 0;
		int Weapon_Y = 0;
		
		try {
			Head_X = animScriptHead_X[activeHeadAnim][animHeadRealCount];
			Head_Y = animScriptHead_Y[activeHeadAnim][animHeadRealCount];
		} catch (Exception e) {}
			
		try {
			Top_X = animScriptTop_X[activeTopAnim][animTopRealCount];
			Top_Y = animScriptTop_Y[activeTopAnim][animTopRealCount];
		} catch (Exception e) {}
		
		try {
			Bottom_X = animScriptBottom_X[activeBottomAnim][animBottomRealCount];
			Bottom_Y = animScriptBottom_Y[activeBottomAnim][animBottomRealCount];
		} catch (Exception e) {}
		
		try {
			Weapon_X = animScriptWeapon_X[activeWeaponAnim][animWeaponRealCount];
			Weapon_Y = animScriptWeapon_Y[activeWeaponAnim][animWeaponRealCount];
		} catch (Exception e) {}

		int thisAnim_mod = 0;

		if(!anim_ignore_mod){
			thisAnim_mod = anim_mod;
		}

		/*if(IAM != IAM_PLAYER){
			sHead = utl.head_enemy.get(head);
			sTop = utl.top_enemy.get(top);
			sBottom = utl.bottom_enemy.get(bottom);
			sWeapon = utl.weapon_enemy.get(weapon);
		}else{
			sHead = utl.head.get(head);
			sTop = utl.top.get(top);
			sBottom = utl.bottom.get(bottom);
			sWeapon = utl.weapon.get(weapon);
		}*/

        drawSprite(face_left, utl.bottom.get(bottom),bottomAnimation,animBottomRow[activeBottomAnim],Bottom_X,Bottom_Y,g);
        if(itemWeapon!=null){
            drawSprite(face_left, utl.weapon.get(itemWeapon.getSPRITE()),weaponAnimation,(animWeaponRow[activeWeaponAnim] + thisAnim_mod),Weapon_X,Weapon_Y,g);
        }
        drawSprite(face_left, utl.top.get(top),topAnimation,animTopRow[activeTopAnim],Top_X,Top_Y,g);
        drawSprite(face_left, utl.head.get(head),headAnimation,animHeadRow[activeHeadAnim],Head_X,Head_Y,g);

        if(itemHat!=null){
            drawSprite(face_left, utl.hat.get(itemHat.getSPRITE()),0,0,0,0,g);
        }
		
/*
		if(face_left){


            cutHead = sHead.getSubimage(headAnimation * sprite_dimension, animHeadRow[activeHeadAnim] * sprite_dimension, sprite_dimension, sprite_dimension);
			AffineTransform tx1 = AffineTransform.getScaleInstance(-1, 1);
			tx1.translate(-cutHead.getWidth(null), 0);
			AffineTransformOp op1 = new AffineTransformOp(tx1,AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		    cutHead = op1.filter(cutHead, null);
			
			cutBody = sTop.getSubimage(topAnimation * sprite_dimension, animTopRow[activeTopAnim] * sprite_dimension, sprite_dimension, sprite_dimension);
			AffineTransform tx2 = AffineTransform.getScaleInstance(-1, 1);
			tx2.translate(-cutBody.getWidth(null), 0);
			AffineTransformOp op2 = new AffineTransformOp(tx2,AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
			cutBody = op2.filter(cutBody, null);
			
			cutBottom = sBottom.getSubimage(bottomAnimation * sprite_dimension, animBottomRow[activeBottomAnim] * sprite_dimension, sprite_dimension, sprite_dimension);
			AffineTransform tx3 = AffineTransform.getScaleInstance(-1, 1);
			tx3.translate(-cutBottom.getWidth(null), 0);
			AffineTransformOp op3 = new AffineTransformOp(tx3,AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
			cutBottom = op3.filter(cutBottom, null);
			
			cutWeapon = sWeapon.getSubimage(weaponAnimation * sprite_dimension, (animWeaponRow[activeWeaponAnim] + thisAnim_mod) * sprite_dimension, sprite_dimension, sprite_dimension);
			AffineTransform tx4 = AffineTransform.getScaleInstance(-1, 1);
			tx4.translate(-cutWeapon.getWidth(null), 0);
			AffineTransformOp op4 = new AffineTransformOp(tx4,AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
			cutWeapon = op4.filter(cutWeapon, null);
			
			g.drawImage(cutBottom, (int)drawX+(int)map.getx()-24-Bottom_X, (int)drawY+(int)map.gety()-40+height+Bottom_Y, null);
			g.drawImage(cutWeapon, (int)drawX+(int)map.getx()-24-Weapon_X, (int)drawY+(int)map.gety()-40+height+Weapon_Y, null);
			g.drawImage(cutBody, (int)drawX+(int)map.getx()-24-Top_X, (int)drawY+(int)map.gety()-40+height+Top_Y, null);
			g.drawImage(cutHead, (int)drawX+(int)map.getx()-24-Head_X, (int)drawY+(int)map.gety()-40+height+Head_Y, null);
            //g.drawImage(cutHat, (int)drawX+(int)map.getx()-24-Head_X, (int)drawY+(int)map.gety()-40+height+Head_Y, null);

		}
		if(face_right){
			//cutImage = image.getSubimage(animRealFrames*sprite_dimension, activeAnim*sprite_dimension, sprite_dimension, sprite_dimension);
			
			cutHead = sHead.getSubimage(headAnimation*sprite_dimension, animHeadRow[activeHeadAnim]*sprite_dimension, sprite_dimension, sprite_dimension);
			cutBody = sTop.getSubimage(topAnimation * sprite_dimension, animTopRow[activeTopAnim] * sprite_dimension, sprite_dimension, sprite_dimension);
			cutBottom = sBottom.getSubimage(bottomAnimation * sprite_dimension, animBottomRow[activeBottomAnim] * sprite_dimension, sprite_dimension, sprite_dimension);
			cutWeapon = sWeapon.getSubimage(weaponAnimation * sprite_dimension, (animWeaponRow[activeWeaponAnim] + thisAnim_mod) * sprite_dimension, sprite_dimension, sprite_dimension);

			g.drawImage(cutBottom, (int)drawX+(int)map.getx()-24+Bottom_X, (int)drawY+(int)map.gety()-40+height+Bottom_Y, null);
			g.drawImage(cutWeapon, (int)drawX+(int)map.getx()-24+Weapon_X, (int)drawY+(int)map.gety()-40+height+Weapon_Y, null);
			g.drawImage(cutBody, (int)drawX+(int)map.getx()-24+Top_X, (int)drawY+(int)map.gety()-40+height+Top_Y, null);
			g.drawImage(cutHead, (int)drawX+(int)map.getx()-24+Head_X, (int)drawY+(int)map.gety()-40+height+Head_Y, null);

		}
		
		
		
	
	*/
		
		
		
		
		super.draw(g);
	}


    public void drawSprite(boolean dir, BufferedImage sprite, int animX, int animY, int modX, int modY, Graphics2D g){


        BufferedImage cut = sprite.getSubimage(animX * sprite_dimension, animY * sprite_dimension, sprite_dimension, sprite_dimension);

        if(dir){

            AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
            tx.translate(-cut.getWidth(null), 0);
            AffineTransformOp op = new AffineTransformOp(tx,AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
            cut = op.filter(cut, null);


            g.drawImage(cut, (int)x+(int)map.getx()-24-modX, (int)y+(int)map.gety()-40+height+modY, null);

        }else{

            g.drawImage(cut, (int)x+(int)map.getx()-24+modX, (int)y+(int)map.gety()-40+height+modY, null);

        }



    }

	
	public void handleOverlay(){
		if(!overlayActive) {
			overlayActive = true;
			moveLock = true;
			overlay.showInventory(this);
		}else{
			overlayActive = false;
			moveLock = false;
			overlay.hideInventory();
		}
	}

	public void shoot(){
		JukeBox.play(sound_atk);
		//activeAnim = anim_attacking;
		//animLock = true;
		//resetFrames();
		//shot Shot = new shot(this, face_left, activeLevel);
        //Shot.setATK(ATK);
        pool.newShot(this);
	}

    public boolean getJumping(){return jumpLock;}

	public int getHead() {return head;}
	public void setHead(int head) {this.head = head;}

	public int getTop() {return top;}
	public void setTop(int top) {this.top = top;}

	public int getBottom() {return bottom;}
	public void setBottom(int bottom) {this.bottom = bottom;}

	public void setSprites(int head, int top, int bottom){
		this.head = head;
		this.top = top;
		this.bottom = bottom;
	}

	public void setHeight(int height) {this.height = height;}
	public void setWidth(int width) {this.width = width;}


	//public int getWeapon() {return weapon;}
	//public void setWeapon(int weapon) {this.weapon = weapon;}

	
	public boolean getMLeft() {return move_left;}
	public boolean getMRight() {return move_right;}
	
	public void setMLeft(boolean move_left) {this.move_left = move_left;}
	public void setMRight(boolean move_right) {this.move_right = move_right;}
	
	
	public boolean getFLeft() {return face_left;}
	public boolean getFRight() {return face_right;}
	
	public void setFLeft(boolean face_left) {this.face_left = face_left;}
	public void setFRight(boolean face_right) {this.face_right = face_right;}
	
	public void setTarget(MapObject target){this.target = target;}
	
	public ArrayList<MapObject> getTargets(){return targets;}
	public void pushTarget(MapObject theTarget){targets.add(theTarget);}

	public int getHealth() {return HP;}
	
	public boolean getMoveLock(){return moveLock;}
	
	public boolean getBlocking(){return blocking;}
	
	public boolean getWallrunning(){
		return wallrunning;
	}

	@Override
	public void jaw() {
		// TODO Auto-generated method stub
		
	}
	
	public void setName(String name){this.Name = name;}
	public void setDescription(String Description){this.Description = Description;}

	public void setInteractTarget(MapObject object) {
		interactTarget = object;
		
	}
	
	public MapObject getInteractTarget() {
		return interactTarget;
		
	}

    public inventory getInventory(){
        return Inventory;
    }


    public void gainEXP(int level){
        int plus = level*10;

        EXP = EXP + plus;

        showNumbers.addNumber((int) x, (int) y, "+"+String.valueOf(plus)+" EXP", 300, Color.GREEN, utl.Arial14);


        if(EXP>EXPNEED){

            JukeBox.play("victory");

            EXPNEED = EXPNEED + level * 100;
            LVL++;
            showNumbers.addNumber((int) x, (int) y, "LEVEL UP!", 300, Color.ORANGE, utl.Arial14);


            // STAT UPGREADS

            ATK = ATK + 2;

            maxHP = maxHP + 10 * level;

            HP = maxHP;
        }



    }

    public void setHealth(int i) {
        if(i<maxHP) {
            HP = i;
        }else{
            HP = maxHP;
        }
    }

    public void setMAXHP(int i) {
        maxHP = i;
        HP = i;
    }

    public int getLVL() {return LVL;}
    public int getExp() {return EXP;}
    public int getExpNeed() {return EXPNEED;}

    public int getATK(){return ATK;}
    public void setATK(int i) {
        ATK = i;
    }

    public int getDamage(){
        ArrayList<item> equipped = Inventory.getEquipped();

        int extraATK = 0;

        for(int i = 0; i<equipped.size();i++){
            extraATK += equipped.get(i).getATK();
        }

        int damage = ATK*10 + extraATK;

        return damage;
    }

    public int getWeight(){
        ArrayList<item> equipped = Inventory.getEquipped();

        int weight = 0;

        for(int i = 0; i<equipped.size();i++){
            weight += equipped.get(i).getWEIGHT();
        }

        return weight;
    }

    public void setLVL(int LVL) {this.LVL = LVL;}
    public void setExp(int EXP) {this.EXP = EXP;}
    public void setExpNeed(int EXPNEED) {this.EXPNEED = EXPNEED;}





}
