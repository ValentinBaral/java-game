package com.neet.Entity;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import com.neet.Audio.JukeBox;
import com.neet.GameState.running;
import com.neet.events.utl;

public class drop extends MapObject{

    private int type = 0;
    private item Item;

	public drop(int activeLevel) {
		//super(room);
		
		//hasImage = false;

        this.activeLevel = activeLevel;
        worldGenerator.getLevel(activeLevel).getList().add(this);

        width = 10;
		height = 10;
		
		noCollision = true;
        noGrav = true;

        active = false;




		
		//this.type = type;
		
		/*if(type == 0){
			activeImg = 0;
		}*/
	}
	
	public void update(){
        super.update();

        if(type == 1){

            Color color = Color.RED;

            if(Item.getRARITY() == 1){
                color = Color.WHITE;
            }
            else if(Item.getRARITY() == 2){
                color = Color.GREEN;
            }
            else if(Item.getRARITY() == 3){
                color = Color.MAGENTA;
            }
            else if(Item.getRARITY() == 4){
                color = Color.ORANGE;
            }

            if(running.counter%3 == 0) {
                gfx.addParticle((int) x + (int) (width / 2), (int) y, 4, 4, 4, 1, 60, color, 1, false);
            }
        }
	}
	
	public void draw(Graphics2D g){
        if(type == 0) {
            g.setColor(Color.RED);
            g.fillOval((int) x + (int) map.getx(), (int) y + (int) map.gety(), width, height);
        }
        if(type == 1){
            if(Item != null){
                BufferedImage sprite = null;
                if(Item.getTYPE() == 0){
                    sprite = utl.hat.get(Item.getSPRITE());
                }
                if(Item.getTYPE() == 1){
                    sprite = utl.weapon.get(Item.getSPRITE());
                }
                /*if(Item.getTYPE() == 2){
                    sprite = utl.weapon.get(Item.getSPRITE());
                }*/

                BufferedImage cutSprite = sprite.getSubimage(0, 0, sprite_dimension, sprite_dimension);
                g.drawImage(cutSprite, (int)x+(int)map.getx()-15, (int)y+(int)map.gety()-15, null);
            }
            //g.setColor(Color.BLUE);
            //g.fillRect((int)x+(int)map.getx()-25,(int)y+(int)map.gety()-25,60,60);


        }
		
		super.draw(g);
	}

    public void collect(smthliving obj){

        System.out.println("@collect");

        if(type == 0){
            obj.setHealth(obj.getHealth()+25);
        }
        if(type == 1){

            obj.getInventory().add(new item(Item));

            JukeBox.play("wosch");
            //Item = null;
        }

        active = false;
    }


    public void getAttacked(smthliving theEnemy, int damage) {

    }

    @Override
    public void jaw() {
        // TODO Auto-generated method stub

    }

    public int getType(){
        return type;
    }

    public void setType(int type){
        this.type = type;
    }

    public item getItem() {
        return Item;
    }

    public void setItem(item item) {
        Item = item;
    }
}
