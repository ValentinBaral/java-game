package com.neet.Entity;

import com.neet.Main.GamePanel;
import com.neet.world.map;

import java.awt.*;
import java.util.Random;
import java.util.Vector;

/**
 * Created by Valentin on 21/02/2015.
 */
public class static_background {

    private static Random random = new Random();

    private static int count = 0;

    private static int num = 0;

    private static Vector<Integer> x = new Vector<Integer>();
    private static Vector<Integer> y = new Vector<Integer>();
    private static Vector<Integer> width = new Vector<Integer>();
    private static Vector<Integer> height = new Vector<Integer>();
    private static Vector<Color> color = new Vector<Color>();
    private static Vector<Integer> style = new Vector<Integer>();

    private static int current = 0;

    public static void update(){

        num++;

        if(current == 1){
            if(num%2 == 0) {
                gfx.addParticle(57, 37, 3, 3, 4, 1, 150, Color.BLUE,2,true);
                gfx.addParticle(GamePanel.WIDTH - 61, 37, 3, 3, 4, 1, 150, Color.BLUE,2,true);
            }
        }

        if(current == 2){
            if(num%10 == 0) {
                gfx.addParticle(-40, 20, 10, 2, 3, 1, 500, Color.MAGENTA,2,true);
                gfx.addParticle(-40, 50, 10, 2, 3, 1, 500, Color.MAGENTA,2,true);
                gfx.addParticle(-40, 80, 10, 2, 3, 1, 500, Color.MAGENTA,2,true);
                gfx.addParticle(-40, 110, 10, 2, 3, 1, 500, Color.MAGENTA,2,true);
                gfx.addParticle(-40, 140, 10, 2, 3, 1, 500, Color.MAGENTA,2,true);
                gfx.addParticle(-40, 170, 10, 2, 3, 1, 500, Color.MAGENTA,2,true);
                gfx.addParticle(-40, 200, 10, 2, 3, 1, 500, Color.MAGENTA,2,true);
                gfx.addParticle(-40, 230, 10, 2, 3, 1, 500, Color.MAGENTA,2,true);
            }
        }

        //static_background_gfx.update();

        for(int i=0; i<count; i++){
            //updatePartikle(i);
        }

    }

    public static void draw(Graphics2D g){

        for(int i=0; i<count; i++){
            drawPartikle(i, g);
        }

        //static_background_gfx.draw(g);

        gfx.draw(g, 2);

    }

    private static void updatePartikle(int i){
        if(x.get(i)+width.get(i)+ map.getx()> GamePanel.WIDTH || y.get(i)+height.get(i)+map.gety()>GamePanel.HEIGHT){
            //removeParticel(i);
        }
    }

    private static void drawPartikle(int i, Graphics2D g){

        int newX = (int)(x.get(i));
        int newY = (int)(y.get(i));

        g.setColor(color.get(i));
        //g.fillRect(newX, newY, width.get(i), height.get(i));
        if(style.get(i)==0) {
            g.fillArc(newX, newY, width.get(i), height.get(i), 220, 90);
        }

        if(style.get(i)==1) {
            g.fillRect(newX, newY, width.get(i), height.get(i));
        }
    }

    private static void removeParticel(int i){
        x.remove(i);
        y.remove(i);
        width.remove(i);
        height.remove(i);
        color.remove(i);
        style.remove(i);
        count--;
    }

    public static void clear(){
        x.clear();
        y.clear();
        width.clear();
        height.clear();
        color.clear();
        style.clear();
        count=0;
    }

    public static void showBG(int num){
        current = num;
        gfx.clean();

        if(num == 1){
            add(0, 40, 60, 400, 1, Color.getHSBColor(0.1f,0.2f,0.2f));
            add(GamePanel.WIDTH-60, 40, 60, 400, 1, Color.getHSBColor(0.1f,0.2f,0.2f));
        }
    }

    public static void add(int _x, int _y, int _width, int _height, int _style, Color _color){

        count++;
        x.add(_x);
        y.add(_y);
        width.add(_width);
        height.add(_height);
        style.add(_style);
        color.add(_color);

    }

}
