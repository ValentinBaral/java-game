package com.neet.Entity;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import com.neet.Main.GamePanel;
import com.neet.events.utl;
import com.neet.world.map;

public class level extends Solids{
	
	Color color = Color.GRAY;

	public level(int room) {
		super(room);
	}
	
	public void draw(Graphics2D g) {

		//double drawX = (x-oldX)*GamePanel.interpolation+oldX;
		//double drawY = (y-oldY)*GamePanel.interpolation+oldY;

		double drawX = x;
		double drawY = y;

		g.setColor(color);

		if (!hasImage){
			g.fillRect((int) map.getx() + (int) drawX, (int) map.gety() + (int) drawY, width, height);
		}
		// border
		if(!noCollision) {
			g.setColor(Color.LIGHT_GRAY);

			BufferedImage cuImg;
			int sprite_dimension = 10;

			/*for(int i=0;i<(int)(width/sprite_dimension);i++){
				if(i == 0){
					cuImg = utl.Wall.getSubimage(0, 0, sprite_dimension, sprite_dimension);
					g.drawImage(cuImg, (int)drawX+(int)map.getx()+i*sprite_dimension, (int)drawY+(int)map.gety(), null);
				}
				else if(i == (int)(width/sprite_dimension)-1){
					cuImg = utl.Wall.getSubimage(10, 0, sprite_dimension, sprite_dimension);
					g.drawImage(cuImg, (int)drawX+(int)map.getx()+i*sprite_dimension, (int)drawY+(int)map.gety(), null);
				}
				else{
					cuImg = utl.Wall.getSubimage(20, 0, sprite_dimension, sprite_dimension);
					g.drawImage(cuImg, (int)drawX+(int)map.getx()+i*sprite_dimension, (int)drawY+(int)map.gety(), null);
				}
			}


			for(int i=0;i<(int)(width/sprite_dimension);i++){
				if(i == 0){
					cuImg = utl.Wall.getSubimage(0, 10, sprite_dimension, sprite_dimension);
					g.drawImage(cuImg, (int)drawX+(int)map.getx()+i*sprite_dimension, (int)drawY+(int)map.gety()+height-sprite_dimension, null);
				}
				else if(i == (int)(width/sprite_dimension)-1){
					cuImg = utl.Wall.getSubimage(10, 10, sprite_dimension, sprite_dimension);
					g.drawImage(cuImg, (int)drawX+(int)map.getx()+i*sprite_dimension, (int)drawY+(int)map.gety()+height-sprite_dimension, null);
				}
				else{
					cuImg = utl.Wall.getSubimage(20, 10, sprite_dimension, sprite_dimension);
					g.drawImage(cuImg, (int)drawX+(int)map.getx()+i*sprite_dimension, (int)drawY+(int)map.gety()+height-sprite_dimension, null);
				}
			}


			for(int i=0;i<(int)(height/sprite_dimension);i++){
				if(i == 0){
				}
				else if(i == (int)(width/sprite_dimension)-1){
				}
				else{
					cuImg = utl.Wall.getSubimage(30, 0, sprite_dimension, sprite_dimension);
					g.drawImage(cuImg, (int)drawX+(int)map.getx(), (int)drawY+(int)map.gety()+i*sprite_dimension, null);
				}
			}

			for(int i=0;i<(int)(height/sprite_dimension);i++){
				if(i == 0){
				}
				else if(i == (int)(width/sprite_dimension)-1){
				}
				else{
					cuImg = utl.Wall.getSubimage(30, 10, sprite_dimension, sprite_dimension);
					g.drawImage(cuImg, (int)drawX+(int)map.getx()+width-sprite_dimension, (int)drawY+(int)map.gety()+i*sprite_dimension, null);
				}
			}*/
		}

		
		super.draw(g);
	}
	
	public void setColor(Color color){this.color = color;}

	public void setImg(int img){activeImg = img;hasImage = true;}

}
