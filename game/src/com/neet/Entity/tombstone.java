package com.neet.Entity;

public class tombstone {
	
	public int level;
	public int ATK;
	public int DEF;
	
	public String name;
	public String quote;
	
	
	public tombstone(){
		
		int r_level = tombstones.random.nextInt(5)+1;
		
		level = r_level;
		ATK = 50+tombstones.random.nextInt(50)*r_level-tombstones.random.nextInt(50);
		DEF = 50+tombstones.random.nextInt(50)*r_level-tombstones.random.nextInt(50);
		
		name = tombstones.firstNames[tombstones.random.nextInt(tombstones.firstNames.length)]+" "+tombstones.lastNames[tombstones.random.nextInt(tombstones.lastNames.length)];
	}
}
