package com.neet.Entity;

import java.util.ArrayList;

public class collision {
	
	private static ArrayList<MapObject> objects = new ArrayList<MapObject>();
	//private static ArrayList<aLevel> activeLevel = new ArrayList<aLevel>();
	//private static ArrayList<shot> shots = new ArrayList<shot>();
	
	public collision(){
		
	}
	
	public static void check(MapObject object){
				
		boolean last_collision_left = object.getCollision_left();
		boolean last_collision_top = object.getCollision_top();
		boolean last_collision_right =  object.getCollision_right();
		boolean last_collision_bottom =  object.getCollision_bottom();
		
		boolean collision_left = false;
		boolean collision_top = false;
		boolean collision_right = false;
		boolean collision_bottom = false;
		
		double thisX = object.getx();
		double thisY = object.gety();
		
		int thisWidth = object.getwidth();
		int thisHeight = object.getheight();
		
		double thisNextX = object.getNextX();
		double thisNextY = object.getNextY();
		
		//for(int i2 = 0; i2<activeLevel.size(); i2++){
			ArrayList<MapObject> roomObjects = worldGenerator.getActiveLevel().getList();
			for(int i = 0; i<roomObjects.size(); i++){
				
				MapObject otherObject = roomObjects.get(i);


				
				if(!otherObject.getNoColl()&&!object.getNoColl()){
					if(otherObject!=object){
						if(otherObject.IAM() != otherObject.IAM_SHOT){
						if((object.IAM() != MapObject.IAM_SHOT || otherObject.IAM() != MapObject.IAM_ENEMY)&&(object.IAM() == MapObject.IAM_PLAYER && otherObject.IAM() == MapObject.IAM_NPC) == false){
		
						
							double otherX = otherObject.getx();
							double otherY = otherObject.gety();
							
							int otherWidth = otherObject.getwidth();
							int otherHeight = otherObject.getheight();
							
							boolean collision_bottom_fix = false;
							boolean collision_top_fix = false;
							boolean collision_left_fix = false;
							boolean collision_right_fix = false;
							
							
							if(((thisNextY+thisHeight >= otherY)&&(thisNextY <= otherY+otherHeight))){
								if(((thisNextX+thisWidth >= otherX)&&(thisNextX <= otherX+otherWidth))){		
									
									if(thisY+thisHeight == otherY && thisX+thisWidth == otherX){
										collision_bottom_fix = true;
										collision_right_fix = true;
									}
									
									if(thisY+thisHeight == otherY && thisX == otherX+otherWidth){
										collision_bottom_fix = true;
										collision_left_fix = true;
									}
									
									
									// not working probably just a quick fix :(((((((((
									
									if(thisY == otherY+otherHeight && thisX+thisWidth == otherX){
										collision_top_fix = true;
										collision_right_fix = true;
										System.out.println("fixRight");
									}
									
									if(thisY == otherY+otherHeight && thisX == otherX+otherWidth){
										collision_top_fix = true;
										collision_left_fix = true;
										System.out.println("fixLeft");
									}
									
									if(thisX >= otherX+otherWidth){
										if(!collision_left_fix){
											if(!last_collision_left){
												object.setNextX(otherX+otherWidth);
			
											}
											collision_left = true;
										}
									}
									
									if(thisY >= otherY+otherHeight){
										if(!collision_top_fix){
											if(!last_collision_top){
												object.setNextY(otherY+otherHeight);
				
											}
											collision_top = true;
										}
									}
									
									if(thisX+thisWidth <= otherX){
										if(!collision_right_fix){
											if(!last_collision_right){
												object.setNextX(otherX-thisWidth);
			
											}
											collision_right = true;
										}
									}
			
									if(thisY+thisHeight <= otherY){
										if(!collision_bottom_fix){
											if(!last_collision_bottom){
												object.setNextY(otherY-thisHeight);
			
											}
											collision_bottom = true;
										}
									}
									
									if(object.IAM() == MapObject.IAM_SHOT){
										
										if(otherObject.IAM() == MapObject.IAM_PLAYER){
                                            shot Shot = (shot)object;
											otherObject.getAttacked(Shot.getObj(), Shot.getObj().getDamage());
                                            object.setActive(false);
										}
										
										if(otherObject.IAM() != MapObject.IAM_ENEMY){
											//worldGenerator.getLevel(object.getLevel()).kill(object);
                                            //object.noCollision = true;
                                            //object.render =false;
                                            //object.setStatic(true);
                                            object.setActive(false);
										}
									}
									
									if(object.getLiving() == true && otherObject.getDeadly()){
										
										((smthliving)object).die();
										
									}
			
								}
							}
						}
					}
				}
			}
		
		}
		
		object.setCollisions(collision_left, collision_top, collision_right, collision_bottom);
	}
	
	public static void interactionRadius(smthliving object){
		object.setInteractTarget(null);
		
		int thisWidth = object.getwidth();
		int thisHeight = object.getheight();
		
		boolean face_left = object.getFLeft();
		boolean face_right = object.getFRight();
		
		double thisNextX = object.getNextX();
		double thisNextY = object.getNextY();

		ArrayList<MapObject> levelObjects = worldGenerator.getActiveLevel().getList();
		for(int i = 0; i<levelObjects.size(); i++) {

            if (levelObjects.get(i).getActive()) {

                levelObjects.get(i).setInRadius(false);

                if (levelObjects.get(i) != object && levelObjects.get(i).getInteraction()) {

                    int mod_y = -40;
                    int mod_x = -40;

                    int mod_height = 80;
                    int mod_width = 80;

                    double otherX = levelObjects.get(i).getx();
                    double otherY = levelObjects.get(i).gety();

                    int otherWidth = levelObjects.get(i).getwidth();
                    int otherHeight = levelObjects.get(i).getheight();

                    //if((thisY+thisHeight+mod_y+mod_height >= otherY)&&(thisY+mod_y <= otherY+otherHeight)){
                    //if((thisX+thisWidth+mod_x+mod_width >= otherX)&&(thisX+mod_x <= otherX+otherWidth)){

                    if (((thisNextY + thisHeight + mod_y + mod_height >= otherY) && (thisNextY + mod_y <= otherY + otherHeight))) {
                        if (((thisNextX + thisWidth + mod_x + mod_width >= otherX) && (thisNextX + mod_x <= otherX + otherWidth))) {

                            if (object.getInteractTarget() != null) {
                                if (Math.abs((otherX + otherWidth / 2) - (thisNextX + thisWidth / 2)) < Math.abs((object.getInteractTarget().getx() + object.getInteractTarget().getwidth() / 2) - (thisNextX + thisWidth / 2))) {
                                    object.getInteractTarget().setInRadius(false);
                                    levelObjects.get(i).setInRadius(true);
                                    object.setInteractTarget(levelObjects.get(i));
                                }
                            } else {
                                levelObjects.get(i).setInRadius(true);
                                object.setInteractTarget(levelObjects.get(i));


                            }

                            if (levelObjects.get(i).getInteractionType() == "drop") {
                                drop theDrop = (drop) levelObjects.get(i);


                                if (thisNextX > otherX + otherWidth / 2) {
                                    levelObjects.get(i).setNextX(otherX + 1);
                                }
                                if (thisNextX + thisWidth / 2 < otherX) {
                                    levelObjects.get(i).setNextX(otherX - 1);
                                }
                                if (thisNextY > otherY + otherHeight / 2) {
                                    levelObjects.get(i).setNextY(otherY + 1);
                                }
                                if (thisNextY + thisHeight / 2 < otherY) {
                                    levelObjects.get(i).setNextY(otherY - 1);
                                }

                                if (((thisNextY + thisHeight >= otherY) && (thisNextY <= otherY + otherHeight))) {
                                    if (((thisNextX + thisWidth >= otherX) && (thisNextX <= otherX + otherWidth))) {
                                        //levelObjects.remove(i);

                                        System.out.println("@collssion precollect");


                                        theDrop.collect(object);


                                        //object.setHealth(object.getHealth()+25);


                                    }
                                }


                            }

                        }
                    }
                }
            }
        }
	}
	
	public static boolean check_special(smthliving object, int mod_x, int mod_y, int mod_width, int mod_height) {
        object.getTargets().clear();
        //ArrayList<MapObject> roomObjects = worldGenerator.getRoom(object.getRoom()).getList();
		
		/*double thisX = object.getx();
		double thisY = object.gety();*/

        int thisWidth = object.getwidth();
        int thisHeight = object.getheight();

        boolean face_left = object.getFLeft();
        boolean face_right = object.getFRight();

        double thisNextX = object.getNextX();
        double thisNextY = object.getNextY();

        boolean gotTarget = false;
        //for(int i2 = 0; i2<activeLevel.size(); i2++){
        ArrayList<MapObject> roomObjects = worldGenerator.getActiveLevel().getList();
        for (int i = 0; i < roomObjects.size(); i++) {

            //MapObject otherObject = worldGenerator.getRoom(object.getRoom()).getList().get(i);

            if (roomObjects.get(i) != object) {


                double otherX = roomObjects.get(i).getx();
                double otherY = roomObjects.get(i).gety();

                int otherWidth = roomObjects.get(i).getwidth();
                int otherHeight = roomObjects.get(i).getheight();

                //if((thisY+thisHeight+mod_y+mod_height >= otherY)&&(thisY+mod_y <= otherY+otherHeight)){
                //if((thisX+thisWidth+mod_x+mod_width >= otherX)&&(thisX+mod_x <= otherX+otherWidth)){

                if (((thisNextY + thisHeight + mod_y + mod_height >= otherY) && (thisNextY + mod_y <= otherY + otherHeight))) {
                    if (((thisNextX + thisWidth + mod_x + mod_width >= otherX) && (thisNextX + mod_x <= otherX + otherWidth))) {

                        if (roomObjects.get(i).IAM == object.getPreferredTarget()) {

                            if (roomObjects.get(i).getLiving()) {

                                if (thisNextX + mod_x + (thisWidth + mod_width) * 0.5 <= otherX + otherWidth * 0.5) {
                                    if (face_right) {
                                        object.pushTarget(roomObjects.get(i));
                                        gotTarget = true;
                                    }
                                }
                                if (thisNextX + mod_x + (mod_width + thisWidth) * 0.5 >= otherX + otherWidth * 0.5) {
                                    if (face_left) {
                                        object.pushTarget(roomObjects.get(i));
                                        gotTarget = true;
                                    }
                                }

                            }
                        }

                        if (roomObjects.get(i).IAM == MapObject.IAM_DESTRUCTABLE) {


                            if (thisNextX + mod_x + (thisWidth + mod_width) * 0.5 <= otherX + otherWidth * 0.5) {
                                if (face_right) {
                                    object.pushTarget(roomObjects.get(i));
                                    gotTarget = true;
                                }
                            }
                            if (thisNextX + mod_x + (mod_width + thisWidth) * 0.5 >= otherX + otherWidth * 0.5) {
                                if (face_left) {
                                    object.pushTarget(roomObjects.get(i));
                                    gotTarget = true;
                                }
                            }

                        }

                    }
                }
            }
        }
        //}
        return gotTarget;
    }
	
	public static void push(MapObject object){
		objects.add(object);
		//checkRoom(object);
	}
	
	public static void kill(MapObject object){
		objects.remove(object);
	}
	
	public static ArrayList<MapObject> getList(){
		return objects;
	}
	
	/*public static ArrayList<aLevel> getActiveRooms(){
		return activeLevel;
	}*/
	
	public static Player findPlayer(){
		
		Player player = null;
		
		for(int i = 0; i<objects.size(); i++){
			if(objects.get(i).IAM == MapObject.IAM_PLAYER){
				player = (Player) objects.get(i);
			}
		}
		return player;
		
	}

}
